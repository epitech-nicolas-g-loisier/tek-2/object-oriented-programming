/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Clock Method
*/

#include "Clock.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>

nts::Clock::Clock(const std::string name, const Tristate value)
	: _name(name), _value(value)
{
}

nts::Tristate nts::Clock::compute(std::size_t pin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	return _value;
}

void nts::Clock::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	other.setLink(otherPin, *this, pin);
}

void nts::Clock::dump() const
{
	std::cout << _name << "=" << _value << std::endl;
}

const std::string &nts::Clock::getName() const
{
	return (_name);
}

void nts::Clock::setValue(nts::Tristate value)
{
	_value = value;
}

void nts::Clock::update()
{
	if (_value == TRUE)
		_value = FALSE;
	if (_value == FALSE)
		_value = TRUE;
}