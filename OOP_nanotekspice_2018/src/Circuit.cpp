/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Circuit
*/

#include <iostream>
#include <stdio.h>
#include <string.h>
#include "Circuit.hpp"
#include "Exception.hpp"
#include "IInput.hpp"
#include "Prompt.hpp"

nts::Circuit::Circuit(const std::string &filename)
	: _parser(FileParser(filename)), _factory(Factory())
{
	_parser.parseFile();
}

nts::Circuit::~Circuit()
{
}

void nts::Circuit::createInput()
{
	std::vector<std::string> list = _parser.getInput();

	for (std::string n : list) {
		std::pair<std::string, std::string> component = getComponentFromString(n);
		_input.push_back(_factory.createComponent(component.first, component.second));
	}

	for (size_t idx1 = 0; idx1 < _input.size(); idx1++) {
		for (size_t idx2 = idx1 + 1; idx2 < _input.size(); idx2++) {
			if (_input[idx1]->getName() == _input[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = 0; idx2 < _output.size(); idx2++) {
			if (_input[idx1]->getName() == _output[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = 0; idx2 < _component.size(); idx2++) {
			if (_input[idx1]->getName() == _component[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
	}
}

void nts::Circuit::createOutput()
{
	std::vector<std::string> list = _parser.getOutput();

	for (std::string n : list) {
		std::pair<std::string, std::string> component = getComponentFromString(n);
		_output.push_back(_factory.createComponent(component.first, component.second));
	}

	for (size_t idx1 = 0; idx1 < _output.size(); idx1++) {
		for (size_t idx2 = 0; idx2 < _input.size(); idx2++) {
			if (_output[idx1]->getName() == _input[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = idx1 + 1; idx2 < _output.size(); idx2++) {
			if (_output[idx1]->getName() == _output[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = 0; idx2 < _component.size(); idx2++) {
			if (_output[idx1]->getName() == _component[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
	}
}

void nts::Circuit::createComponent()
{
	std::vector<std::string> list = _parser.getComponent();

	for (std::string n : list) {
		std::pair<std::string, std::string> component = getComponentFromString(n);
		_component.push_back(_factory.createComponent(component.first, component.second));
	}

	for (size_t idx1 = 0; idx1 < _component.size(); idx1++) {
		for (size_t idx2 = 0; idx2 < _input.size(); idx2++) {
			if (_component[idx1]->getName() == _input[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = 0; idx2 < _output.size(); idx2++) {
			if (_component[idx1]->getName() == _output[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
		for (size_t idx2 = idx1 + 1; idx2 < _component.size(); idx2++) {
			if (_component[idx1]->getName() == _component[idx2]->getName())
				throw NanoException("Several components share the same name.");
		}
	}
}

std::pair<std::string, std::string> nts::Circuit::getComponentFromString(const std::string & line)
{
	std::pair<std::string, std::string> ret;

	char *lin = const_cast<char *>(line.c_str());
	char *tmp = strtok(lin," \t\n");;
	std::string end;

	while (tmp != NULL)  {
		if (ret.first.empty())
			ret.first = tmp;
		else if (ret.second.empty())
			ret.second = tmp;
		else {
			std::string end = tmp;
			break;
		}
		tmp = strtok(NULL," \t\n");
	}

	if (ret.first.empty() || ret.second.empty())
		throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
	else if (end.empty() || end[0] == '#')
		return ret;
	else
		throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
}

std::pair<std::string, nts::Tristate> nts::Circuit::getValueFromString(const std::string & line)
{
	std::pair<std::string, nts::Tristate> ret {"", nts::UNDEFINED};

	char *lin = const_cast<char *>(line.c_str());
	char *tmp = strtok(lin,"=");;

	while (tmp != NULL)  {
		if (ret.first.empty())
			ret.first = tmp;
		else if (ret.second == nts::UNDEFINED)
			if (strcmp(tmp, "1") == 0)
				ret.second = nts::TRUE;
			else if (strcmp(tmp, "0") == 0)
				ret.second = nts::FALSE;
			else
				throw NanoException("Error in provided command line!");
		else {
			throw NanoException("Error in provided command line!");
		}
		tmp = strtok(NULL,"=");
	}
	if (ret.first.empty() || ret.second == nts::UNDEFINED)
		throw NanoException("Error in provided command line!");
	return ret;
}

void nts::Circuit::setInputValue(char **values)
{
	for (size_t i = 0; values[i] != nullptr ; i++) {
		size_t pos = 0;
		std::pair<std::string, nts::Tristate> value = getValueFromString(values[i]);
		for (pos = 0; pos < _input.size() ; pos++) {
			if (_input[pos]->getName() == value.first)
				break;
		}
		if (pos == _input.size())
			throw NanoException("A provided input is unknown.");
		else {
			static_cast<IInput *>(_input[pos].get())->setValue(value.second);
		}
	}
}

void nts::Circuit::execute()
{
	nts::Prompt prompt;
	prompt.start(_input, _output);
}

void nts::Circuit::getPairFromLinkStr(const std::string &string, std::pair<std::string, size_t> &link)
{
	size_t separator = string.find(':');
	if (separator == string.npos)
		throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
	size_t firstPin = strtoul(&string[separator + 1], NULL, 10);
	link = {string.substr(0, separator), firstPin};
}

nts::IComponent *nts::Circuit::getComponentByName(std::string &name)
{
	for (auto&& i : _output) {
		if (i.get()->getName() == name)
			return (i.get());
	}
	for (auto&& i : _input) {
		if (i.get()->getName() == name)
			return (i.get());
	}
	for (auto&& i : _component) {
		if (i.get()->getName() == name)
			return (i.get());
	}
	throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
}

void nts::Circuit::linkComponents()
{
	std::vector<std::string> linkStr = _parser.getLink();

	for (auto&& i : linkStr) {
		size_t firstBeg = i.find_first_not_of(" \t");
		if (firstBeg == i.npos || i[firstBeg] == '#')
			continue;
		size_t firstEnd = i.substr(firstBeg).find_first_of(" \t");
		std::string first = i.substr(firstBeg, firstEnd);
		size_t secondBeg = i.substr(firstBeg + firstEnd).find_first_not_of(" \t");
		if (secondBeg == i.npos || i[firstBeg + firstEnd + secondBeg] == '#')
			throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
		std::string second = i.substr(firstBeg + firstEnd + secondBeg);
		size_t secondEnd = second.find_first_of(" \t");
		if (secondEnd != second.npos && second.substr(secondEnd).find_first_not_of(" \t") != second.npos)
			throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
		std::pair<std::string, size_t> firstPair;
		std::pair<std::string, size_t> secondPair;
		getPairFromLinkStr(first, firstPair);
		getPairFromLinkStr(second, secondPair);
		nts::IComponent *firstComponent = getComponentByName(firstPair.first);
		firstComponent->setLink(firstPair.second, *getComponentByName(secondPair.first), secondPair.second);
	}
}