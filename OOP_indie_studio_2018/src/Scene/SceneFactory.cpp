/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** SceneFactory
*/

#include <iostream>
#include <Scene/SplashScreenScene.hpp>
#include <Scene/OptionScene.hpp>
#include <Scene/PowerUpScene.hpp>
#include "SceneFactory.hpp"
#include "BeginScene.hpp"
#include "TitleScreenScene.hpp"
#include "PlayerSelectorScene.hpp"
#include "MapSelectorScene.hpp"
#include "GameScene.hpp"
#include "LoadingScene.hpp"
#include "GameEndScene.hpp"

indie::SceneFactory::SceneFactory()
{
	_ctors[indie::IScene::BEGIN] = &SceneFactory::createBegin;
	_ctors[indie::IScene::TITLESCREEN] = &SceneFactory::createTitleScreen;
	_ctors[indie::IScene::LOADING] = &SceneFactory::createLoading;
	_ctors[indie::IScene::SPLASHSCREEN] = &SceneFactory::createSplashScreen;
	_ctors[indie::IScene::POWERUP] = &SceneFactory::createPowerUpScreen;

	_ctorsOption[indie::IScene::PLAYERSELECTOR] = &SceneFactory::createPlayerSelector;
	_ctorsOption[indie::IScene::MAPSELECTOR] = &SceneFactory::createMapSelector;
	_ctorsOption[indie::IScene::GAME] = &SceneFactory::createGame;
	_ctorsOption[indie::IScene::OPTION] = &SceneFactory::createOption;
	_ctorsOption[indie::IScene::GAMEEND] = &SceneFactory::createGameEnd;
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createScene(const indie::IScene::sceneState choice, indie::GraphicalEngine &engine, indie::GameOption &gameOption)
{
	if (_ctorsOption.count(choice) != 0)
		return (this->*_ctorsOption[choice])(engine, gameOption);
	else if (_ctors.count(choice) != 0)
		return (this->*_ctors[choice])(engine);
	else
		return (this->*_ctors[indie::IScene::TITLESCREEN])(engine);
}

std::unique_ptr<indie::IScene> indie::SceneFactory::createBegin(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::BeginScene>(engine);
}

std::unique_ptr<indie::IScene> indie::SceneFactory::createTitleScreen(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::TitleScreenScene>(engine);
}

std::unique_ptr<indie::IScene> indie::SceneFactory::createLoading(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::LoadingScene>(engine);
}

std::unique_ptr<indie::IScene> indie::SceneFactory::createSplashScreen(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::SplashScreenScene>(engine);
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createPlayerSelector(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const
{
	gameOption.player = 1;
	gameOption.character = {"random", "random", "random", "random"};
	return std::make_unique<indie::PlayerSelectorScene>(engine, gameOption);
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createMapSelector(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const
{
	return std::make_unique<indie::MapSelectorScene>(engine, gameOption);
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createGame(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const
{
	return std::make_unique<indie::GameScene>(engine, gameOption);
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createOption(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const
{
	return std::make_unique<indie::OptionScene>(engine, gameOption);
}

std::unique_ptr<indie::IScene>
indie::SceneFactory::createGameEnd(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const
{
	return std::make_unique<indie::GameEndScene>(engine, gameOption);
}

std::unique_ptr<indie::IScene> indie::SceneFactory::createPowerUpScreen(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::PowerUpScene>(engine);
}
