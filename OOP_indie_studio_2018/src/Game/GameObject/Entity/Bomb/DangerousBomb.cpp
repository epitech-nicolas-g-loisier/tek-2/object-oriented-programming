/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** DangerousBomb
*/
#include "DangerousBomb.hpp"
#include "MapVisitors.hpp"

indie::DangerousBomb::DangerousBomb(unsigned char fLevel, std::pair<unsigned char, unsigned char> &pos, GraphicalEngine &graphicalEngine, indie::AudioEngine &_audioEngine)
		:	Bomb(fLevel, pos, graphicalEngine, _audioEngine)
{}

bool indie::DangerousBomb::update(indie::MapData &map)
{
	bool isAlive = std::chrono::steady_clock::now() < deathTime;
	if (isAlive)
		return isAlive;
	audioEngine.playBombSound();
	std::string hitMap("\010\4\4\4\010\4\1\1\1\4\4\1\1\1\4\4\1\1\1\4\010\4\4\4\010");
	for (int y = _pos.second - 2; y < _pos.second + 3; y++) {
		if (y < 0 || y > 8)
			continue;
		for (int x = _pos.first - 2; x < _pos.first + 3; x++) {
			if (x < 0 || x > 14 || level < hitMap[5 * abs(y - _pos.second + 2) + abs(x - _pos.first + 2)])
				continue;
			addBlastToMap(map, {x, y});
		}
	}
	auto blast = std::make_shared<indie::Blast>(_pos, engine, false);
	map.entityMap[_pos] = blast;
	map.entities.push_back(std::move(blast));
	return (isAlive);
}

bool indie::DangerousBomb::addBlastToMap(indie::MapData &map, const std::pair<unsigned char, unsigned char> &i) const
{
	auto blast = std::make_shared<indie::Blast>(i, engine, false);
	if (map.entityMap.count(i)) {
		std::weak_ptr<IGameObject> weakPtr;
		std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[i]);
		if (weakPtr.lock()->interact(blast.get()))
			return (false);
	}
	map.entityMap[i] = blast;
	map.entities.push_back(std::move(blast));
	return (true);
}