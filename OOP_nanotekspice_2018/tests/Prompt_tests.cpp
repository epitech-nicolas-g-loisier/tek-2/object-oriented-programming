/*
** EPITECH PROJECT, 2019
** nanotekspice
** File description:
** Tests for DFlipFlop and prompt
*/
#include "criterion/criterion.h"
#include "criterion/redirect.h"
#include "Circuit.hpp"
#include "Exception.hpp"
#include <iostream>

static void redirect_stdout(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(C4081, main_test, .init=redirect_stdout)
{
	nts::Circuit circuit("d_latch.nts");
	circuit.createInput();
	circuit.createOutput();
	circuit.createComponent();
	circuit.linkComponents();
	char var1[10] = "set=1\0";
	char var2[10] = "data=1\0";
	char *vars[3] = {&var1[0], &var2[0], nullptr};
	circuit.setInputValue(vars);
	std::istringstream input("display\nsimulate\nhello\nset=0\nsimulate\ndisplay\nexit");
	std::cin.rdbuf(input.rdbuf());
	circuit.execute();
}

Test(C4081, invalid_value, .init=redirect_stdout)
{
	nts::Circuit circuit("d_latch.nts");
	circuit.createInput();
	circuit.createOutput();
	circuit.createComponent();
	circuit.linkComponents();
	char var1[10] = "set=1\0";
	char var2[10] = "data=1\0";
	char *vars[3] = {&var1[0], &var2[0], nullptr};
	circuit.setInputValue(vars);
	std::istringstream input("set=ayaya\nexit");
	std::cin.rdbuf(input.rdbuf());
	try {
		circuit.execute();
		cr_assert(false, "Expected exception");
	} catch (NanoException) {
	}
}