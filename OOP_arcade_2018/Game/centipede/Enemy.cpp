/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Centipede
*/
#include <algorithm>
#include <cstdlib>
#include "Enemy.hpp"

Centipede::Enemy::Enemy(int memberLeft)
	: _memberLeft(memberLeft)
{
	if (memberLeft)
		_body.push_back(std::pair<int, int>(std::rand() % 36 + 2, 0));
	_dir = std::rand() % 2;
	start = std::chrono::steady_clock::now().time_since_epoch();
}

Centipede::Enemy::Enemy(const Centipede::Enemy &src)
{
	_memberLeft = src._memberLeft;
	_dir = src._dir;
	for (auto &&i: src._body)
		_body.push_back(i);
	start = std::chrono::steady_clock::now().time_since_epoch();
}

int Centipede::Enemy::move(std::vector<std::unique_ptr<Centipede::Enemy>> &entityList, std::vector<Centipede::Obstacle> &obstacleList, std::pair<int, int> &shot)
{
	std::chrono::nanoseconds end = std::chrono::steady_clock::now().time_since_epoch();
	if ((end - start).count() > 50000000) {
		if (_memberLeft) {
			_body.push_back(std::pair<int, int>(15, 0));
			_memberLeft--;
		}
		_body.back() = _body[0];
		if (_dir) {
			if (_body[0].first > 0 && !checkColision(entityList, obstacleList, -1)) {
				_body.back().first--;
			} else {
				_dir = false;
				_body.back().second++;
			}
		} else {
			if (_body[0].first < 39 && !checkColision(entityList, obstacleList, 1)) {
				_body.back().first++;
			} else {
				_dir = true;
				_body.back().second++;
			}
		}
		std::rotate(_body.rbegin(), _body.rbegin() + 1,_body.rend());
		start = end;
	}
	if (_body[0].second == 39)
		return (-90 - (_body.size() * 10));
	for (auto &&i = _body.begin(); i != _body.end(); i++) {
		if (*i.base() == shot) {
			shot.second = -1;
			obstacleList.push_back({*i.base(), 5});
			if (i == _body.begin()) {
				_body.erase(i);
				_dir = _dir ? false : true;
				_body[0].second++;
			} else {
				if (i + 1 < _body.end()) {
					entityList.push_back(std::make_unique<Centipede::Enemy>(0));
					for (auto j = i + 1; j != _body.end(); j++)
						entityList.back()->_body.push_back(*j.base());
					entityList.back()->_dir = _dir;
				}
				for (auto &&j = std::distance(i, _body.end()) ; j > 0; j--)
					_body.pop_back();
			}
			if (_body.size() == 0)
				return (100);
			return (10);
		}
	}
	return (0);
}

const std::vector<std::pair<int, int>> &Centipede::Enemy::getBody() const
{
	return (_body);
}

bool Centipede::Enemy::toDelete()
{
	return (_body.size() == 0 || _body[0].second == 39);
}

bool Centipede::Enemy::getDir() const
{
	return (_dir);
}

bool Centipede::Enemy::checkColision(std::vector<std::unique_ptr<Centipede::Enemy>> &entityList, std::vector<Centipede::Obstacle> &obstacleList, int xOffset)
{
	std::pair<int, int> head = _body[0];

	for (auto &&i: obstacleList) {
		if (i.pos == head)
			return (true);
	}
	head.first += xOffset;
	for (auto &&i: obstacleList) {
		if (i.pos == head)
			return (true);
	}
	head.first -= xOffset;
	for (auto i = entityList.begin(); i < entityList.end(); i++) {
		const std::vector<std::pair<int, int>> &body = i->get()->getBody();
		if (i->get() != this && std::find(body.begin(), body.end(), head) != body.end())
			return (true);
	}
	head.first += xOffset;
	for (auto i = entityList.begin(); i < entityList.end(); i++) {
		const std::vector<std::pair<int, int>> &body = i->get()->getBody();
		if (i->get() != this && std::find(body.begin(), body.end(), head) != body.end())
			return (true);
	}
	return (false);
}

int Centipede::QuickEnemy::move(std::vector<std::unique_ptr<Centipede::Enemy>> &entityList, std::vector<Centipede::Obstacle> &obstacleList, std::pair<int, int> &shot)
{
	std::chrono::nanoseconds end = std::chrono::steady_clock::now().time_since_epoch();
	if ((end - start).count() > 5000000) {
		if (_body[0].second < 31) {
			if (_dir) {
				if (_body[0].first > 0 && !checkColision(entityList, obstacleList, -1)) {
					_body[0].first--;
				} else {
					_dir = false;
					_body[0].second++;
				}
			} else {
				if (_body[0].first < 39 && !checkColision(entityList, obstacleList, 1)) {
					_body[0].first++;
				} else {
					_dir = true;
					_body[0].second++;
				}
			}
		} else
			_body[0].second++;
		start = end;
	}
	if (_body[0].second == 39)
		return (-50);
	if (_body[0] == shot) {
		obstacleList.push_back({_body[0], 5});
		_body.pop_back();
		shot.second = -1;
		return (500);
	}
	return (0);
}

bool Centipede::QuickEnemy::toDelete()
{
	return (_body.size() == 0 || _body[0].second == 39);
}