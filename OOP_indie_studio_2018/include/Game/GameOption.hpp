/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameOption
*/

#ifndef GAMEOPTION_HPP
#define GAMEOPTION_HPP

#include <array>

namespace indie {
struct GameOption
{
	int player = 1;
	int winner = 0;
	std::array<std::string, 4> character = {"mario", "random", "random", "random"};
	std::string mapChoice;
	std::array<std::array<std::string, 6>, 4> playerInput;
	std::string music = "music/Original.ogg";
};
}

/*
 * Player Input
 *
 * 0 = up
 * 1 = down
 * 2 = left
 * 3 = right
 * 4 = bomb
 * 5 = special
 *
*/

#endif //GAMEOPTION_HPP
