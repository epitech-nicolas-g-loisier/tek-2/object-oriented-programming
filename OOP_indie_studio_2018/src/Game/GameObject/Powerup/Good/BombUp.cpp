/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** BombUp
*/

#include "BombUp.hpp"
#include <iostream>

indie::BombUp::BombUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "BombUp", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::BombUp::interact(indie::Player *player) {
	if (player) {
		if (player->bombLevel < 8)
			player->bombLevel++;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}