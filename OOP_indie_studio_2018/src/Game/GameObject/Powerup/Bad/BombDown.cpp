/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** BombDown
*/
#include "BombDown.hpp"
#include <iostream>

indie::BombDown::BombDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(false, engine, "BombDown", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::BombDown::interact(indie::Player *player) {
	if (player) {
		if (player->bombLevel > 1)
			player->bombLevel--;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerDownSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}