/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpScene
*/

#ifndef POWERUPSCENE_HPP
#define POWERUPSCENE_HPP

#include "IScene.hpp"
#include "GraphicalEngine.hpp"

namespace indie
{
class PowerUpScene : public IScene
{
public:
	explicit PowerUpScene(GraphicalEngine &);
	~PowerUpScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;

private:

};
}

#endif //POWERUPSCENE_HPP
