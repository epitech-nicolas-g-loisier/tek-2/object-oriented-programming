/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** OptionScene
*/

#include <iostream>
#include "OptionScene.hpp"

indie::OptionScene::OptionScene(indie::GraphicalEngine &engine, indie::GameOption &option)
	: _engine(engine), gameOption(option)
{
	music = {"music/Original.ogg", "music/GustyGarden.ogg", "music/DireDireDocks.ogg"};
}

indie::IScene::sceneState indie::OptionScene::draw()
{
	_engine.beginScene({120, 102, 136, 255});

	for (int i = 0; i < 4; ++i) {
		drawPlayerKeys(i);
	}

	if (selected)
		_engine.drawText("Press A key to assign", "medium", {255, 255, 255, 255}, {0, 0, 1920, 50});
	else
		_engine.drawText("Press escape to quit", "medium", {255, 255, 255, 255}, {0, 0, 1920, 50});

	drawMusic();

	_engine.drawCursor();
	_engine.endScene();

	if (_engine.receiver.keyDown("escape"))
		return TITLESCREEN;
	else
		return OPTION;
}

void indie::OptionScene::drawPlayerKeys(int player)
{
	std::pair<int, int> pos = {50 + 480 * player, 150};

	_engine.drawRectangle({480 * player, 50, 480 * player + 480, 900}, color[player]);
	_engine.drawText("Player " + std::to_string(player + 1), "medium", {255, 255, 255, 255}, {480 * player, 50, 480 * (player + 1), 100});

	drawPlayerKey("Screen/Option/MoveButton.png", 3, pos, {0, 0, 100, 100}, player);
	pos.second += 125;

	drawPlayerKey("Screen/Option/MoveButton.png", 0, pos, {100, 0, 200, 100}, player);
	pos.second += 125;

	drawPlayerKey("Screen/Option/MoveButton.png", 2, pos, {200, 0, 300, 100}, player);
	pos.second += 125;

	drawPlayerKey("Screen/Option/MoveButton.png", 1, pos, {300, 0, 400, 100}, player);
	pos.second += 125;

	drawPlayerKey("Screen/Option/BombButton.png", 4, pos, {0, 0, 100, 100}, player);
	pos.second += 125;

	drawPlayerKey("Screen/Option/SpecialButton.png", 5, pos, {0, 0, 100, 100}, player);
}

void indie::OptionScene::drawPlayerKey(const std::string& icon, int choice, std::pair<int, int> pos, rect_t rect, int player)
{
	if (_engine.receiver.leftMousePressed()) {
		std::pair<int, int> mousePos = _engine.receiver.mousePos();
		if (mousePos.first > 480 * player && mousePos.first < 480 + 480 * player && mousePos.second > pos.second && mousePos.second < pos.second + 100) {
			for (int & i : selectedChoice) {
				i = -1;
			}
			selected = true;
			selectedChoice[player] = choice;
		}
	} if (selected && selectedChoice[player] == choice) {
		_engine.drawRectangle({480 * player, pos.second, 480 + 480 * player, pos.second + 100}, {100, 100, 100, 255});
		std::string newChoice = _engine.receiver.getLastKey();
		if (newChoice != "clear" && newChoice != "escape" && !newChoice.empty()) {
			selected = false;
			for (int & i : selectedChoice) {
				i = -1;
			}
			gameOption.playerInput[player][choice] = newChoice;
		}
	}

	_engine.drawSprite(icon, pos, rect);
	_engine.drawText(gameOption.playerInput[player][choice], "medium", {255, 255, 255, 255}, {125 + player * 480, pos.second, 480 + player * 480, pos.second + 100});
}

void indie::OptionScene::drawMusic()
{
	unsigned int choice = 0;

	for (auto &element: music) {
		if (element == gameOption.music)
			break;
		choice++;
	}

	_engine.drawRectangle({0, 900, 1920, 1080}, {0, 0, 0, 155});

	std::pair<int, int> pos = _engine.receiver.mousePos();

	if (pos.second > 900 && _engine.receiver.leftMousePressed()) {
		clicked = true;
	} else if (clicked && _engine.receiver.leftMouseReleased()) {
		choice++;
		if (choice >= music.size())
			choice = 0;
		clicked = false;
		gameOption.music = music[choice];
	}

	if (choice >= music.size()) {
		_engine.drawText("No music", "large", {255, 255, 255, 255}, {0, 900, 1920, 1080});
	} else {
		std::string tmp = music[choice].substr(music[choice].find_last_of('/') + 1);
		tmp[tmp.find_last_of('.')] = '\0';
		_engine.drawText("Music: " + tmp, "large", {255, 255, 255, 255}, {0, 900, 1920, 1080});
	}
}
