/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Score class
*/
#include <vector>
#include <utility>

#ifndef SCORE_ARCADE_H_
	#define SCORE_ARCADE_H_

typedef struct ScoreTable
{
	std::vector<std::pair<std::string, int>> scores;
} ScoreTable;

#endif // SCORE_ARCADE_H_