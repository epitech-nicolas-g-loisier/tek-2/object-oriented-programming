/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** FileParser
*/

#include <fstream>
#include <iostream>
#include "FileParser.hpp"
#include "Exception.hpp"

FileParser::FileParser(const std::string &filename)
	: _filename(filename)
{
}

FileParser::~FileParser()
{
}

std::vector<std::string> FileParser::getInput() const
{
	return _input;
}

std::vector<std::string> FileParser::getOutput() const
{
	return _output;
}

std::vector<std::string> FileParser::getComponent() const
{
	return _component;
}

std::vector<std::string> FileParser::getLink() const
{
	return _link;
}

void FileParser::getPart(const std::string line, int &part)
{
	if (line.substr(line.find_first_not_of(" \t")).find(".chipsets") == 0) {
		int pos = line.substr(line.find_first_not_of(" \t")).find_first_of(" \t");
		if (part == 1)
			part = -1;
		else if (pos == -1)
			part = 1;
		else {
			part = 1;
			for (size_t i = pos; i < line.size(); i++) {
				if (line[i] != ' ' && line[i] != '\t' && line[i] != '#') {
					part = -1;
					break;
				} else if (line[i] == '#') {
					break;
				}
			}
		}
	} else if (line.substr(line.find_first_not_of(" \t")).find(".links") == 0) {
		int pos = line.substr(line.find_first_not_of(" \t")).find_first_of(" \t");
		if (part == 2)
			part = -1;
		else if (pos == -1)
			part = 2;
		else {
			part = 2;
			for (size_t i = pos; i < line.size(); i++) {
				if (line[i] != ' ' && line[i] != '\t' && line[i] != '#') {
					part = -1;
					break;
				} else if (line[i] == '#') {
					break;
				}
			}
		}
	} else {
		throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
	}
}

void FileParser::getChipset(const std::string line)
{
	std::string chipset[5] = {"input", "true", "false", "clock", "output"};

	for (size_t i = 0; i < 5; i++) {
		if (line.substr(line.find_first_not_of(" \t")).find(chipset[i]) == 0) {
			if (i != 4)
				_input.push_back(line);
			else
				_output.push_back(line);
			return;
		}
	}
	_component.push_back(line);
}

void FileParser::parseFile()
{
	std::ifstream fs;

	fs.open(_filename);

	if (!fs.is_open()) {
		fs.clear();
		throw NanoException("Can't open the file");
	} else {
		std::string line;
		int part = 0;
		while (getline(fs, line)){
			if (line == "" || line[line.find_first_not_of("\t ")] == '#') {
			} else if (line[line.find_first_not_of("\t ")] == '.') {
				getPart(line, part);
			} else if (part == 1) {
				getChipset(line);
			} else if (part == 2) {
				_link.push_back(line);
			} else {
				throw NanoException("The circuit file includes one or several lexical or syntactic errors.");
			}
		}
		fs.close();
	}
}