/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** TitleScreenScene
*/

#ifndef TITLESCREENSCENE_HPP
#define TITLESCREENSCENE_HPP

#include <vector>
#include "GraphicalEngine.hpp"
#include "Button.hpp"
#include "IScene.hpp"

namespace indie
{
class TitleScreenScene : public IScene
{
public:
	explicit TitleScreenScene(indie::GraphicalEngine &engine);
	~TitleScreenScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	std::vector<Button> button;
	std::array<indie::IScene::sceneState, 4> choices{};

};
}

#endif //TITLESCREENSCENE_HPP
