/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** GraphicalEngine Class
*/

#ifndef GRAPHICALENGINE_HPP
#define GRAPHICALENGINE_HPP

#include <irrlicht.h>
#include <string>
#include <array>
#include "PowerUpNodeFactory.hpp"
#include "EventReceiver.hpp"
#include "PlayerNodeFactory.hpp"

namespace indie
{

typedef struct color_s
{
	int red = 0;
	int green = 0;
	int blue = 0;
	int alpha = 0;
} color_t;

typedef struct rect_s
{
	int x = 0;
	int y = 0;
	int xMax = 0;
	int yMax = 0;
} rect_t;

class GraphicalEngine
{
public:
        GraphicalEngine();
        ~GraphicalEngine();
        const std::string findModelPath(const std::string& filepath);
	const std::string findTexturePath(const std::string& filepath);
	const std::string findFontPath(const std::string& filepath);
        void initGame();
        void endGame();

	void setCam(const irr::core::vector3df& targetPos, const irr::core::vector3df& pos);
	void drawCursor();

	void drawBackground(const std::string &file);
	void drawTexture(const std::string &file, std::pair<int, int> pos);
	void drawSprite(const std::string &file, std::pair<int, int> pos, rect_s spritePos);
	void drawText(const std::string &text, const std::string &size, color_t color, rect_s textPos);
	void drawRectangle(rect_s rect, color_t color);

	void beginScene(color_t BackgroundColor);
	void endScene();

        EventReceiver receiver;
        irr::IrrlichtDevice *device;
	irr::video::IVideoDriver* driver;
	irr::scene::ISceneManager* scenemgr;
	irr::gui::IGUIFont* font76;
	irr::gui::IGUIFont* font24;
	irr::gui::IGUIFont* font48;

	PlayerNodeFactory playerNodeFactory;
	PowerUpNodeFactory powerUpNodeFactory;

private:
	irr::scene::ICameraSceneNode *cam;
	irr::scene::IMeshSceneNode *base{};
	std::array<irr::video::ITexture*, 2> cursor{};

};
}

#endif //GRAPHICALENGINE_HPP
