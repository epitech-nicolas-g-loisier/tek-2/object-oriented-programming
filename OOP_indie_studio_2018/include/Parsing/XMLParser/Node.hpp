/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Element is XML content reference from a given tag to its end tag
*/

#ifndef XML_NODE_HPP_
#define XML_NODE_HPP_

#include <iterator>
#include <istream>
#include <variant>
#include <vector>
#include <memory>
#include "SimpleType.hpp"
#include "Tag.hpp"

namespace XML
{



class Node
{
  public:
	struct Pos { /* istream position */
	  public:
		std::istream::pos_type tag;
		std::istream::pos_type content;
		std::istream::pos_type next;

		bool operator==(const Pos &) const;
		bool operator!=(const Pos &) const;
		bool operator>(const Pos &) const;
		bool operator<(const Pos &) const;
		bool operator>=(const Pos &) const;
		bool operator<=(const Pos &) const;
	};
	struct Info { /* content of nodes, accessed with pointer operators */
		std::string str; /* content of node tag */
		SimpleType content; /* raw content inside layer */
	};
	Node(std::shared_ptr<std::istream> is);
	Node(const Node &coplien);
	Node();
	~Node() = default;

	Node &operator=(const Node &coplien);

	/* getters */
	const Info &operator*() const;
	const Info *operator->() const;

	/* iterator */
	Node next() const;
	Node operator++(int amt) const;
	Node &operator++();
	/* range */
	Node begin() const;
	Node end() const;

	Node find(const std::string &str) const;
	std::vector<Node> findAll(const std::string &str) const;
	SimpleType get(const std::string &str) const;
	std::vector<SimpleType> getAll(const std::string &str) const;

	static Node find(const Node &b, const Node &e, const std::string &str);
	static std::vector<Node> findAll(const Node &b, const Node &e, const std::string &str);
	static SimpleType get(const Node &b, const Node &e, const std::string &str);
	static std::vector<SimpleType> getAll(const Node &b, const Node &e, const std::string &str);

	/* comparator */
	bool operator==(const std::string &str) const;
	bool operator!=(const std::string &str) const;
	bool operator==(const Tag::Type &type) const;
	bool operator!=(const Tag::Type &type) const;
	bool operator==(int layer) const;
	bool operator!=(int layer) const;
	bool operator==(const Node &node) const;
	bool operator!=(const Node &node) const;

  private:
	std::shared_ptr<std::istream> is;

	Pos pos;
	Info info;
};

}

#endif /* !XML_NODE_HPP_ */
