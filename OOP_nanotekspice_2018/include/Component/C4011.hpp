/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component 4011 Class
*/

#ifndef C4011_HPP_
	#define C4011_HPP_

#include "IComponent.hpp"
#include "Pin.hpp"
#include <map>

namespace nts {

	class C4011 : public IComponent
	{
	public:
		C4011(std::string);
		~C4011();
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;

	private:
		std::string _name;
		std::map<size_t, InputPin *> _input;
		std::map<size_t, OutputPin *> _output;
	};
}

#endif /* !C4071_HPP_ */
