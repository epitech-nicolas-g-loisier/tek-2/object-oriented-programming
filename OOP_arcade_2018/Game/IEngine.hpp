/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Definition of the interface IEngine
*/
#include <string>
#include <vector>

#ifndef IEngine_
	#define IEngine_

#include "IGraphical.hpp"

class IEngine
{
public:
	virtual ~IEngine() {};
	virtual std::pair<bool, int> computeInput(const std::vector<std::string> &) = 0;
	virtual void display(IGraphical &) = 0;
};

#endif // IEngine_