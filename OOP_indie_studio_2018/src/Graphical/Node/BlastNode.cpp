/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** BlastNode
*/

#include <iostream>
#include "GraphicalEngine.hpp"
#include "BlastNode.hpp"
#include "GraphicalException.hpp"

indie::BlastNode::BlastNode(GraphicalEngine &engine, bool blue)
{
	mesh = engine.scenemgr->getMesh(engine.findModelPath("Fire/Fire Gem.obj").data());
	if (!mesh)
		throw ModelLoadingError("BlastNode");

	node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("BlastNode");

	if (blue)
		texture = engine.driver->getTexture(engine.findModelPath("Fire/BlueBlast.png").data());
	else
		texture = engine.driver->getTexture(engine.findModelPath("Fire/Blast.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Blast" << std::endl;
	setTexture();
}

indie::BlastNode::~BlastNode()
{
	node->remove();
}

std::pair<float, float> indie::BlastNode::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::BlastNode::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::BlastNode::getRotation()
{
	return node->getRotation().Y;
}

void indie::BlastNode::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::BlastNode::swapTexture(char)
{

}

void indie::BlastNode::setTexture()
{
	if (!texture)
		return;
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->setMaterialTexture(0, texture);
}

void indie::BlastNode::update()
{
	auto scale = node->getScale();
	scale += {diff, diff, diff};

	if (scale.Y > 2.5)
		diff = -0.05;
	else if (scale.Y < 1)
		diff = 0.05;

	node->setScale(scale);
}
