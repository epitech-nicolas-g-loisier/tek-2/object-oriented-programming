/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** IInput
*/
#include "IComponent.hpp"

#ifndef IINPUT_HPP_
#define IINPUT_HPP_

namespace nts
{
	class IInput : public IComponent
	{
	public:
		virtual void setValue(nts::Tristate) = 0;
		virtual void update() = 0;
	};
}

#endif /* !IINPUT_HPP_ */