/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Tag
*/

#ifndef TAG_HPP_
#define TAG_HPP_

#include <unordered_map>
#include <string>
#include "XMLParser/SimpleType.hpp"

namespace XML
{

class Tag {
  public:
	enum Layer {None = -2, Closing = -1, Single = 0, Opening = 1};
	enum Type {UndefinedType, Comment, Meta, Basic};
	typedef std::unordered_map<std::string, SimpleType> Attributes;
	struct Info {
	  public:
		Tag::Type type;
		std::string name;
		Tag::Attributes attributes;
	};
	struct Attribute : public std::pair<std::string, SimpleType> {
	  public:
		Attribute(const std::string &str);
		~Attribute() = default;
	};

	Tag(const std::string &content);
	Tag() = default;
	~Tag() = default;

	class Parse {
	  public:
		static std::string name(const std::string &str);
		static Tag::Type type(const std::string &str);
		static Tag::Layer layer(const std::string &str);
		static bool hasAttributes(const std::string &str);
		static Tag::Attributes attributes(const std::string &str);
	};

	const Info &operator*() const;
	const Info *operator->() const;

  private:
	Info info;
};

}

#endif /* !TAG_HPP_ */
