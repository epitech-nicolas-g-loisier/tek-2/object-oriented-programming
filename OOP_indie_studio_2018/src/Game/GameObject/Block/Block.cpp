/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Block
*/
#include "Block.hpp"

indie::Block::Block(bool destroyable, std::pair<float, float> pos, GraphicalEngine &engine, indie::AudioEngine &_audioEngine)
	: isDestroyable(destroyable), powerUp(), _node(destroyable, engine), audioEngine(_audioEngine)
{
	_node.setPosition(pos);
}

indie::Block::Block(indie::PowerUpFactory &factory, bool destroyable, std::pair<float , float> pos, GraphicalEngine &engine, indie::AudioEngine &_audioEngine)
	: isDestroyable(destroyable), _node(destroyable, engine), audioEngine(_audioEngine)
{
	powerUp = factory.spawnPowerUp(engine, pos, _audioEngine);

	_node.setPosition(pos);
}

indie::Block::~Block()
{
	audioEngine.playBreakBlockSound();
}

bool indie::Block::interact(indie::Player *player)
{
	if (player)
		player->moving = false;
	return (true);
}

bool indie::Block::interact(indie::Blast *blast)
{
	if (blast) {
		if (isDestroyable) {
			blast->holdPowerUp(powerUp);
			return (false);
		} else
			blast->burnOut();
	}
	return (true);
}

bool indie::Block::interact(indie::Bomb *)
{
	return (true);
}

bool indie::Block::interact(indie::PowerUp *)
{
	return false;
}
