/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** BlockFactory
*/
#ifndef INDIE_STUDIO_BLOCKFACTORY_HPP
#define INDIE_STUDIO_BLOCKFACTORY_HPP

#include "PowerUpFactory.hpp"
#include <random>

namespace indie {
	class BlockFactory {
	public:
		BlockFactory(GraphicalEngine &, AudioEngine &);
		std::shared_ptr<indie::Block> createBlock(char c, std::pair<float, float> position);

	private:
		std::knuth_b rand;
		PowerUpFactory pFactory;
		GraphicalEngine &engine;
		AudioEngine &audioEngine;
	};
}

#endif //INDIE_STUDIO_BLOCKFACTORY_HPP
