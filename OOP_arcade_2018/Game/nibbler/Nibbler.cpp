/*
** EPITECH PROJECT, 2019
** OOP arcade
** File description:
** Nibbler Method
*/

#include <algorithm>
#include "Nibbler.hpp"

nibbler::Nibbler::Nibbler()
	: _food({5, 15}), _bonusFood({-1, -1}), _mapSize({30, 30}), _score(0)
{
	createFood();

	start = std::chrono::steady_clock::now().time_since_epoch();
	std::srand(std::time(nullptr));
}

nibbler::Nibbler::~Nibbler()
{
}

std::pair<bool, int> nibbler::Nibbler::computeInput(const std::vector<std::string> &inputs)
{
	std::string inputList[4] = {"Z", "S", "Q", "D"};
	std::chrono::nanoseconds end = std::chrono::steady_clock::now().time_since_epoch();
	int speed = 100000000;


	if (std::find(std::begin(inputs), std::end(inputs), " ") != std::end(inputs)) {
		speed = speed / 10;
	}
	createBonusFood();
	for (std::string input : inputs) {
		if (std::find(std::begin(inputList), std::end(inputList), input) != std::end(inputList)) {
			_snake->changeDir(input);
		}
	}
	if ((end - start).count() > (speed - _score * 1000000)) {
		start = end;
		if ((_snake->getBody())[0].first == _food.first && (_snake->getBody())[0].second == _food.second) {
			_snake->grow();
			_score++;
			createFood();
		} else if ((_snake->getBody())[0].first == _bonusFood.first && (_snake->getBody())[0].second == _bonusFood.second) {
			int plus = rand() % 4 + 2;
			_snake->grow(plus);
			_score += plus;
			_bonusFood = {-1, -1};
		} else {
			_snake->move();
		}
	}
	if (checkColision())
		return std::pair<bool, int>(false, _score);
	else if (_score == 896)
		return std::pair<bool, int>(false, _score);
	else
		return std::pair<bool, int>(true, _score);
}

void nibbler::Nibbler::getHeadSprite(const std::pair<float, float> &pos1, const std::pair<float, float> &pos2)
{
	if (pos1.first == pos2.first) {
		if (pos1.second > pos2.second)
			_snakeHead.spriteName = "nibbler/SnakeHeadDown.png";
		else
			_snakeHead.spriteName = "nibbler/SnakeHeadUp.png";
	} else {
		if (pos1.first > pos2.first)
			_snakeHead.spriteName = "nibbler/SnakeHeadRight.png";
		else
			_snakeHead.spriteName = "nibbler/SnakeHeadLeft.png";
	}
}

void nibbler::Nibbler::display(IGraphical &display)
{
	std::vector<std::pair<float, float>> body = _snake->getBody();

	display.drawRect(_mapRect);

	_foodRect.pos.first = _food.first + 5;
	_foodRect.pos.second = _food.second + 5;
	display.drawRect(_foodRect);
	if (_bonusFood.first != -1) {
		_bonusFoodRect.pos.first = _bonusFood.first + 5;
		_bonusFoodRect.pos.second = _bonusFood.second + 5;
		display.drawRect(_bonusFoodRect);
	}
	_snakeHead.pos.first = body[0].first + 5;
	_snakeHead.pos.second = body[0].second + 5;
	getHeadSprite(body[0], body[1]);
	display.drawRect(_snakeHead);
	for (size_t i = 1; i < body.size() - 5; i++) {
		_snakeBody.pos.first = body[i].first + 5;
		_snakeBody.pos.second = body[i].second + 5;
		display.drawRect(_snakeBody);
	}
	display.drawPair({{"Score:", _score}, {40, 5}, Arcade::Color::WHITE, false});
	display.drawPair({{"Developer's High Score:", 87}, {40, 10}, Arcade::Color::WHITE, false});
}

bool nibbler::Nibbler::checkColision() const
{
	std::vector<std::pair<float, float>> body = _snake->getBody();

	if (body[0].first < 0 || body[0].second < 0)
		return true;
	else if (body[0].first > _mapSize.first || body[0].second > _mapSize.second)
		return true;

	for (size_t i = 1; i < body.size() - 5; i++) {
		if (body[i].first == body[0].first && body[i].second == body[0].second) {
			return true;
		}
	}

	return false;
}

void nibbler::Nibbler::createFood()
{
	std::vector<std::pair<float, float>> body = _snake->getBody();

	_food.first += std::rand();
	_food.first = _food.first % _mapSize.first;
	_food.second += std::rand();
	_food.second = _food.second % _mapSize.second;

	for (auto part : body) {
		if (part.first == _food.first && part.second == _food.second) {
			createFood();
			break;
		}
	}
	if (_food.first == _bonusFood.first && _food.second == _bonusFood.second)
			createFood();
}

void nibbler::Nibbler::createBonusFood()
{
	if (std::rand() % 500 == 0) {
		_bonusFood = {-1, -1};
		return;
	} else if (_bonusFood.first != -1)
		return;
	else if (std::rand() % 1000 != 42)
		return;
	std::vector<std::pair<float, float>> body = _snake->getBody();

	_bonusFood.first += std::rand();
	_bonusFood.first = _bonusFood.first % _mapSize.first;
	_bonusFood.second += std::rand();
	_bonusFood.second = _bonusFood.second % _mapSize.second;

	for (auto part : body) {
		if (part.first == _bonusFood.first && part.second == _bonusFood.second) {
			createBonusFood();
			break;
		}
	}
	if (_food.first == _bonusFood.first && _food.second == _bonusFood.second)
			createBonusFood();
}
