/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Wrapper for the sfml lib
*/

#include "IGraphical.hpp"
#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <sstream>
#include <iomanip>
#include <map>

__attribute__ ((constructor))
void wrapper_ctor()
{
}

__attribute__ ((destructor))
void wrapper_dtor()
{
}

class ArcadeWindow : public IGraphical
{
public:
	ArcadeWindow();
	~ArcadeWindow();
	void drawLine(const Arcade::Line &) final;
	void drawRect(const Arcade::Rect &) final;
	void drawPair(const Arcade::Pair &) final;
	void update() final;
	std::vector<std::string> getInput() final;

private:
	std::unique_ptr<sf::RenderWindow> window;
	std::vector<std::pair<sf::Keyboard::Key, const char*>> bindings;
	sf::Font font;
	sf::Event event;
	std::array<sf::Color, 8> _colors = {sf::Color::Black, sf::Color::Red, sf::Color::Green,
						sf::Color::Yellow, sf::Color::Blue, sf::Color::Magenta,
						sf::Color::Cyan, sf::Color::White};
	void loadTexture(const std::string &);
	std::map<std::string, std::unique_ptr<sf::Texture>> _textures;
};

ArcadeWindow::ArcadeWindow()
{
	window = std::make_unique<sf::RenderWindow>(sf::VideoMode(1600, 800), "Arcade", sf::Style::Close);
	window->setVerticalSyncEnabled(true);
	font.loadFromFile("./ressource/arial.ttf");
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Up, "UP"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Down, "DOWN"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Left, "LEFT"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Right, "RIGHT"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Escape, "ESCAPE"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Return, "RETURN"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::BackSpace, "BACKSPACE"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Space, " "));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num0, "0"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num1, "1"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num2, "2"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num3, "3"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num4, "4"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num5, "5"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num6, "6"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num7, "7"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num8, "8"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Num9, "9"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::A, "A"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::B, "B"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::C, "C"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::D, "D"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::E, "E"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::F, "F"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::G, "G"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::H, "H"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::I, "I"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::J, "J"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::K, "K"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::L, "L"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::M, "M"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::N, "N"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::O, "O"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::P, "P"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Q, "Q"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::R, "R"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::S, "S"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::T, "T"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::U, "U"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::V, "V"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::W, "W"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::X, "X"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Y, "Y"));
	bindings.push_back(std::pair<sf::Keyboard::Key, const char*>(sf::Keyboard::Z, "Z"));
}

ArcadeWindow::~ArcadeWindow()
{
}

void ArcadeWindow::drawLine(const Arcade::Line &line)
{
	int symb = -1 + 2 * (line.dir % 2);
	float x2 = line.dir < 2 ? line.pos.first : line.pos.first + line.length * symb;
	float y2 = line.dir < 2 ? line.pos.second + line.length * symb : line.pos.second;

	sf::Vertex sfmlLine[3] = {sf::Vertex(sf::Vector2f(line.pos.first * 20, line.pos.second * 20)), sf::Vertex(sf::Vector2f(x2 * 20, y2 * 20))};
	window->draw(sfmlLine, 2, sf::Lines);
}

void ArcadeWindow::drawRect(const Arcade::Rect &rect)
{
	sf::RectangleShape sfmlRect;

	loadTexture(rect.spriteName);
	sfmlRect.setPosition(sf::Vector2f(rect.pos.first * 20, rect.pos.second * 20));
	sfmlRect.setSize(sf::Vector2f(rect.size.first * 20, rect.size.second * 20));
	auto pos =_textures.find(rect.spriteName);
	if (pos != _textures.end()) {
		sfmlRect.setTexture(_textures[rect.spriteName].get());
	} else
		sfmlRect.setFillColor(_colors[rect.defaultColor]);
	if (rect.isOutlined) {
		sfmlRect.setOutlineThickness(2);
		sfmlRect.setOutlineColor(_colors[Arcade::Color::WHITE]);
	}
	window->draw(sfmlRect);
}

void ArcadeWindow::drawPair(const Arcade::Pair &pair)
{
	sf::Text txt(pair.value.first, font);
	txt.setCharacterSize(20);
	if (pair.background) {
		sf::Vector2f size = txt.findCharacterPos(pair.value.first.size());
		drawRect({pair.pos, {size.x / 20 , 1.2}, "", pair.color, 0, false});
		if (pair.color == Arcade::Color::WHITE)
			txt.setFillColor(sf::Color::Black);
	}
	else
		txt.setFillColor(_colors[pair.color]);
	txt.setPosition(sf::Vector2f(pair.pos.first * 20, pair.pos.second * 20));
	window->draw(txt);
	if (pair.value.second >= 0) {
		std::stringstream ss;
		ss << std::setfill('0') << std::setw(6) << pair.value.second;
		txt.setString(ss.rdbuf()->str());
		txt.setPosition(sf::Vector2f(pair.pos.first * 20, pair.pos.second * 20 + 25));
		if (pair.background) {
			float size = txt.findCharacterPos(ss.rdbuf()->str().size()).x - pair.pos.first * 20;
			drawRect({{pair.pos.first, pair.pos.second + 1}, {size / 20 , 1.4}, "", pair.color, 0, false});
			if (pair.color == Arcade::Color::WHITE)
				txt.setFillColor(sf::Color::Black);
		}
		window->draw(txt);
	}
}

void ArcadeWindow::update()
{
	sf::Text txt("SFML", font);
	txt.setCharacterSize(20);
	txt.setPosition(sf::Vector2f(75 * 20, 38 * 20));
	window->draw(txt);

	window->pollEvent(event);
	window.get()->display();
	window.get()->clear();

	sf::sleep(sf::milliseconds(40));
}

std::vector<std::string> ArcadeWindow::getInput()
{
	std::vector<std::string> input;

	for (unsigned int i = 0; i < bindings.size(); i++) {
		if (sf::Keyboard::isKeyPressed(bindings[i].first))
			input.push_back(std::string(bindings[i].second));
	}
	return (input);
}

void ArcadeWindow::loadTexture(const std::string &name)
{
	if (_textures.find(name) != _textures.end())
		return;
	if (name == "")
		return;

	_textures.emplace(name, std::make_unique<sf::Texture>());
	bool res = _textures[name]->loadFromFile("ressource/textures/" + name);
	if (!res) {
		_textures.erase(name);
	}
}

extern "C" std::unique_ptr<IGraphical> getIGraphical()
{
	return (std::make_unique<ArcadeWindow>());
}