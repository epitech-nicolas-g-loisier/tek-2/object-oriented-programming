/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** True Method
*/

#include "True.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>

nts::True::True(const std::string name)
	: _name(name)
{
}

nts::Tristate nts::True::compute(std::size_t pin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	return _value;
}

void nts::True::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	other.setLink(otherPin, *this, pin);
}

void nts::True::dump() const
{
	std::cout << _name << "=" << _value << std::endl;
}

const std::string &nts::True::getName() const
{
	return (_name);
}

void nts::True::setValue(nts::Tristate)
{
	throw NanoException("Cannot set value of 'true' component");
}