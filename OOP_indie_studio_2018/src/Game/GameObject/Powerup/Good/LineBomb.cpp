/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** LineBomb
*/

#include "LineBomb.hpp"
#include <iostream>

indie::LineBomb::LineBomb(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "LineBomb", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::LineBomb::interact(indie::Player *player) {
	if (player) {
		player->hasLineBomb = true;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}