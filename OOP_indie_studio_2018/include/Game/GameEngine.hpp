/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Game engine class
*/

#include <vector>
#include <map>
#include <utility>
#include <variant>
#include <chrono>
#include "PowerUpFactory.hpp"
#include "MapVisitors.hpp"
#include "AudioEngine.hpp"
#include "MapData.hpp"
#include "MapGenerator.hpp"

#ifndef GAMEENGINE_HPP_
#define GAMEENGINE_HPP_

namespace indie {

class GameEngine {
public:
	explicit GameEngine(GraphicalEngine &, GameOption);
	~GameEngine() = default;

	void generateMap();
	int mainLoop();

private:
	std::chrono::duration<long, std::ratio<1, 1000000000>> displayTime() const;
	void spawnSuddenDeathBlock(std::chrono::duration<long, std::ratio<1, 1000000000>>);

	indie::AudioEngine audioEngine;
	indie::MapData map;
	indie::GameOption options;
	indie::GraphicalEngine &graphicalEngine;
	std::chrono::steady_clock::time_point gameTime;
	unsigned char sdIndex;
	std::vector<std::shared_ptr<indie::Block>> blocks;
};
}

#endif // GAMEENGINE_HPP_