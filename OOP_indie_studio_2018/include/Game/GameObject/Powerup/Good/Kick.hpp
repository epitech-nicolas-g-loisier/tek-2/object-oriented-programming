/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Kick
*/

#ifndef BOMBERMAN_KICK_HPP
#define BOMBERMAN_KICK_HPP

#include "PowerUp.hpp"

namespace indie {
	class Kick : virtual public PowerUp
	{
	public:
		Kick(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //BOMBERMAN_KICK_HPP
