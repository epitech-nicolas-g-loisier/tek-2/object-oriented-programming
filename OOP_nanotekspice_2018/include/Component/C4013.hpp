/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component 4013 Class
*/

#ifndef C4013_HPP_
	#define C4013_HPP_

#include "IComponent.hpp"
#include "Pin.hpp"
#include <map>
#include <vector>

namespace nts {

	class C4013 : public IComponent
	{
	public:
		C4013(std::string);
		~C4013();
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;

	private:
		std::string _name;
		std::map<size_t, InputPin *> _input;
		std::map<size_t, std::vector<size_t>> _output;
		Tristate _vpin;
		Tristate _pcl;
	};
}

#endif /* !C4071_HPP_ */