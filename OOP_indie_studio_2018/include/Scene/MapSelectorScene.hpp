/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** MapSelectorScene
*/

#ifndef MAPSELECTORSCENE_HPP
#define MAPSELECTORSCENE_HPP

#include <array>
#include <vector>
#include <string>
#include "IScene.hpp"
#include "GraphicalEngine.hpp"
#include "GameOption.hpp"

namespace indie
{
class MapSelectorScene : public IScene
{
public:
	explicit MapSelectorScene(GraphicalEngine &, indie::GameOption &);
	~MapSelectorScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	indie::GameOption &gameOption;
};
}

#endif //MAPSELECTORSCENE_HPP
