/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Ia
*/

#include <cmath>
#include "Ia.hpp"

indie::Ia::Ia(std::pair<float, float> src, GraphicalEngine &engine, const std::string &character, indie::AudioEngine &_audioEngine)
	: Player(src, engine, character, _audioEngine),_dest({0, 0}), _prevDest({0, 0}),
	_bombPos({0, 0}), _graphicalEngine(engine), _isMoving(false), _prevTime(std::chrono::seconds(0)), _currTime(std::chrono::seconds(0))
{
	name = character;
}

bool indie::Ia::update(indie::MapData &mapData)
{
	bool isAlive = hp > 0;

	if (isAlive) {
		_currTime = std::chrono::steady_clock::now();
		if (_prevTime.time_since_epoch() < std::chrono::seconds(1)) {
			_prevTime = _currTime;
		}
		_dest = chooseEnemy(mapData);
		handling(mapData);
		interactWithEntity(mapData);
	}
	if (hp <= 0) {
		for (auto it = mapData.players.begin(); it != mapData.players.end(); it++) {
			if (it->get() == this) {
				mapData.players.erase(it);
				break;
			}
		}
	}
	_prevDest = _dest;
	_prevTime = _currTime;
	return (isAlive);
}

std::pair<float,float> indie::Ia::chooseEnemy(indie::MapData &map)
{
	std::pair<float,float> diff;
	std::pair<float,float> current;

	if (map.players.size() <= 1) {
		return _dest;
	} else if (map.players.begin()->get() == this) {
		if (map.players.size() >= 2) {
			auto iterator = std::next(map.players.begin(), 1);
			diff = {iterator->get()->getPosition().first, iterator->get()->getPosition().second};
		}
	} else {
		diff = {map.players.begin()->get()->getPosition().first,
			map.players.begin()->get()->getPosition().second};
	}
	for (auto iterator = map.players.begin(); iterator != map.players.end(); iterator++) {
		if (iterator->get() == this)
			iterator++;
		if (iterator == map.players.end())
			return diff;
		current.first = iterator->get()->getPosition().first;
		current.second = iterator->get()->getPosition().second;
		if ((abs(pos.first - current.first) + abs(pos.second - current.second)) <
		(abs(pos.first - diff.first) + abs(pos.second - diff.second))) {
			diff = current;
		}
	}
	return diff;
}

void indie::Ia::isInRangeOfBlast(indie::MapData &mapData, unsigned int index)
{
	while (_indexPos.first == index) {
		if (cellIsValid(_indexPos.first + 1, _indexPos.second) &&
		isUnBlocked(mapData.entityMap, {_indexPos.first + 1, _indexPos.second})) {
			_indexPos.first += 1.0;
			_path.push_back({_indexPos, false});
			_isMoving = true;
		} else if (cellIsValid(_indexPos.first - 1, _indexPos.second) &&
			isUnBlocked(mapData.entityMap, {_indexPos.first - 1, _indexPos.second})) {
			_indexPos.first -= 1.0;
			_path.push_back({_indexPos, false});
			_isMoving = true;
		} else if (cellIsValid(_indexPos.first, _indexPos.second + 1) &&
			isUnBlocked(mapData.entityMap, {_indexPos.first, _indexPos.second + 1})) {
			_indexPos.second += 1.0;
			_path.push_back({_indexPos, false});
			_isMoving = true;
		} else if (cellIsValid(_indexPos.first, _indexPos.second - 1) &&
			isUnBlocked(mapData.entityMap, {_indexPos.first, _indexPos.second - 1})) {
			_indexPos.second -= 1.0;
			_path.push_back({_indexPos, false});
			_isMoving = true;
		} else {
			break;
		}
	}
}

void indie::Ia::hideFromRow(indie::MapData &mapData)
{
	indie::EntityTypeVisitor visitor;

	for (unsigned int i = 0; i < ROW; i++) {
		auto index = mapData.entityMap.find({i, _indexPos.second});
		if (index != mapData.entityMap.end()) {
			std::visit(visitor, mapData.entityMap.at({i, _indexPos.second}));
			if (visitor.type == EntityTypeVisitor::EntityType::BOMB) {
				isInRangeOfBlast(mapData, i);
			}
		}
	}
}

void indie::Ia::hideFromCol(indie::MapData &mapData)
{
	indie::EntityTypeVisitor visitor;

	for (auto i = 0; i < COL; i++) {
		auto index = mapData.entityMap.find({_indexPos.first, i});
		if (index != mapData.entityMap.end()) {
			std::visit(visitor, mapData.entityMap.at({_indexPos.first, i}));
			if (visitor.type == EntityTypeVisitor::EntityType::BOMB) {
				isInRangeOfBlast(mapData, i);
			}
		}
	}
}

bool indie::Ia::hide(indie::MapData &mapData)
{
	hideFromRow(mapData);
	hideFromCol(mapData);
	return _isMoving;
}

void indie::Ia::setPosition(std::pair<float, float> position)
{
	pos = position;
}

bool indie::Ia::isDestination(int row, int col)
{
	return (row == _dest.first && col == _dest.second);
}

bool indie::Ia::isUnBlocked(std::map<std::pair<unsigned char, unsigned char>, indie::EntityUnion> const &grid, std::pair<float, float> mapIndex)
{
	indie::EntityTypeVisitor visitor;
	if (grid.count(mapIndex)) {
		auto index = grid.find(mapIndex);
		if (index != grid.end()) {
			std::visit(visitor, grid.at({mapIndex.first, mapIndex.second}));
			if (visitor.type == EntityTypeVisitor::EntityType::POWERUP) {
				pickPowerUp(std::get<0>(grid.at({mapIndex.first, mapIndex.second})));
				return true;
			}
		}
	}
	return !grid.count({mapIndex.first, mapIndex.second});
}

bool indie::Ia::cellIsValid(int row, int col)
{
	return (row >= 0) && (row < ROW) && (col >= 0) && (col < COL);
}

bool indie::Ia::mapIsValid()
{
	if (!cellIsValid(pos.first, pos.second)) {
		return false;
	} else if (!cellIsValid(_dest.first, _dest.second)) {
		return false;
	} else if (isDestination(pos.first, pos.second)) {
		node->idle();
		return true;
	}
	return true;
}

void indie::Ia::launchBomb(indie::MapData &mapData)
{
	indie::EntityTypeVisitor visitor;

	auto index = mapData.entityMap.find(_bombPos);
	if (index != mapData.entityMap.end()) {
		std::visit(visitor, mapData.entityMap.at({_bombPos.first, _bombPos.second}));
		if (visitor.type != EntityTypeVisitor::EntityType::BOMB &&
		visitor.type != EntityTypeVisitor::EntityType::BLAST) {
			_bombPos = pos;
			putBomb(mapData);
		}
	} else {
		_bombPos = pos;
		putBomb(mapData);
	}
	hide(mapData);
}

void indie::Ia::movePlayerRight(indie::MapData &mapData, long timeDiff)
{
	auto position = _path.begin();
	std::pair<float, float> velocity({0,0});

	if (position != _path.end() && pos.first < position->first.first) {
		velocity.first += (0.6875 * speedLevel * timeDiff / 1000);
		node->setRotation(-90);
		move(velocity, mapData);
		if (pos.first > position->first.first) {
			pos.first = position->first.first;
			_path.erase(position);
		}
	}
}

void indie::Ia::movePlayerLeft(indie::MapData &mapData, long timeDiff)
{
	auto position = _path.begin();
	std::pair<float, float> velocity({0,0});

	if (position != _path.end() && pos.first > position->first.first) {
		velocity.first -= (0.6875 * speedLevel * timeDiff / 1000);
		node->setRotation(90);
		move(velocity, mapData);
		if (pos.first < position->first.first) {
			pos.first = position->first.first;
			_path.erase(position);
		}
	}
}

void indie::Ia::movePlayerDown(indie::MapData &mapData, long timeDiff)
{
	auto position = _path.begin();
	std::pair<float, float> velocity({0,0});

	if (position != _path.end() && pos.second > position->first.second) {
		velocity.second -= (0.6875 * speedLevel * timeDiff / 1000);
		node->setRotation(0);
		move(velocity, mapData);
		if (pos.second < position->first.second) {
			pos.second = position->first.second;
			_path.erase(position);
		}
	}
}

void indie::Ia::movePlayerUp(indie::MapData &mapData, long timeDiff)
{
	auto position = _path.begin();
	std::pair<float, float> velocity({0,0});

	if (position != _path.end() && pos.second < position->first.second) {
		velocity.second += (0.6875 * speedLevel * timeDiff / 1000);
		node->setRotation(180);
		move(velocity, mapData);
		if (pos.second > position->first.second) {
			pos.second = position->first.second;
			_path.erase(position);
		}
	}
}

bool indie::Ia::moving(indie::MapData &mapData)
{
	auto position = _path.begin();

	if (position == _path.end()) {
		_isMoving = false;
		return false;
	} else if ((_dest.first != _prevDest.first || _dest.second != _prevDest.second) ||
		(pos.first == position->first.first && pos.second == position->first.second)) {
		node->idle();
		_isMoving = false;
		_path.clear();
	} else if (_isMoving) {
		long timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(_currTime - _prevTime).count();
		movePlayerRight(mapData, timeDiff);
		movePlayerLeft(mapData, timeDiff);
		movePlayerUp(mapData, timeDiff);
		movePlayerDown(mapData, timeDiff);
		node->setPosition(pos);
		node->move();
		if (pos.first == position->first.first && pos.second == position->first.second) {
			node->idle();
			launchBomb(mapData);
			_isMoving = false;
			return false;
		}
		return true;
	}
	return false;
}

bool indie::Ia::isNearTarget(indie::MapData &mapData)
{
	if ((pos.first + 1 == _dest.first || pos.first -1 == _dest.first)
	&& pos.second == _dest.second) {
		_bombPos = pos;
		putBomb(mapData);
		return true;
	} else if ((pos.second + 1 == _dest.second || pos.second -1 == _dest.second)
		&& pos.first == _dest.first) {
		_bombPos = pos;
		putBomb(mapData);
		return true;
	}
	return false;
}

void indie::Ia::canMoveWhenDecreasing(indie::MapData &mapData)
{
	bool move = false;

	while ((cellIsValid(_indexPos.first - 1, _indexPos.second) &&
		isUnBlocked(mapData.entityMap, {_indexPos.first - 1,_indexPos.second})) ||
	(cellIsValid(_indexPos.first, _indexPos.second - 1) &&
		isUnBlocked(mapData.entityMap, {_indexPos.first,_indexPos.second - 1}))) {
		if (isNearTarget(mapData)) {
			_isMoving = true;
			return;
		} else if (cellIsValid(_indexPos.first - 1, _indexPos.second) &&
			isUnBlocked(mapData.entityMap,{_indexPos.first - 1,_indexPos.second})) {
			_indexPos.first -= 1;
			_path.push_back({_indexPos, false});
			move = true;
		} else if (cellIsValid(_indexPos.first, _indexPos.second - 1) &&
			isUnBlocked(mapData.entityMap, {_indexPos.first, _indexPos.second - 1})) {
			_indexPos.second -= 1;
			_path.push_back({_indexPos, false});
			move = true;
		} else {
			break;
		}
	}
	if(move == false) {
		_path.push_back({_indexPos, true});
	}
}

void indie::Ia::canMoveWhenIncreasing(indie::MapData &mapData)
{
	bool move = false;

	while ((cellIsValid(_indexPos.first + 1, _indexPos.second) &&
		isUnBlocked(mapData.entityMap, {_indexPos.first + 1,_indexPos.second})) ||
	(cellIsValid(_indexPos.first, _indexPos.second + 1) &&
		isUnBlocked(mapData.entityMap, {_indexPos.first,_indexPos.second - 1}))) {
		if (isNearTarget(mapData)) {
			_isMoving = true;
			return;
		} else if (cellIsValid(_indexPos.first + 1, _indexPos.second) &&
			isUnBlocked(mapData.entityMap,{_indexPos.first + 1,_indexPos.second})) {
			_indexPos.first += 1;
			_path.push_back({_indexPos, false});
			move = true;
		} else if (cellIsValid(_indexPos.first, _indexPos.second + 1) &&
			isUnBlocked(mapData.entityMap, {_indexPos.first, _indexPos.second + 1})) {
			_indexPos.second += 1;
			_path.push_back({_indexPos, false});
			move = true;
		} else {
			break;
		}
	}
	if(move == false) {
		_path.push_back({_indexPos, true});
	}
}

void indie::Ia::calculatePathToPlayer(indie::MapData &mapData)
{
	indie::EntityTypeVisitor visitor;

	if (isNearTarget(mapData)) {
		return;
	}
	auto index = mapData.entityMap.find(_bombPos);
	if (index != mapData.entityMap.end()) {
		std::visit(visitor, mapData.entityMap.at({_bombPos.first, _bombPos.second}));
		if (visitor.type == EntityTypeVisitor::EntityType::BOMB ||
		visitor.type == EntityTypeVisitor::EntityType::BLAST) {
			_isMoving = true;
			return;
		}
	} else if (isNearTarget(mapData)) {
		return;
	} else if (_indexPos.first > _dest.first || _indexPos.second > _dest.second) {
		canMoveWhenDecreasing(mapData);
	} else {
		canMoveWhenIncreasing(mapData);
	}
	_isMoving = true;
}

void indie::Ia::interactWithEntity(indie::MapData &mapData)
{
	std::pair<int, int> roundedPos = {lround(pos.first), lround(pos.second)};

	if (mapData.entityMap.count(roundedPos)) {
		std::weak_ptr<IGameObject> weakPtr;
		std::visit(ENTITY_VISITOR(weakPtr), mapData.entityMap[roundedPos]);
		if (!weakPtr.lock()->interact(this))
			mapData.entityMap.erase(roundedPos);
	}
}

bool indie::Ia::handling(indie::MapData &mapData)
{
	if (!mapIsValid()) {
		return false;
	} else if (_isMoving) {
		moving(mapData);
		return true;
	}
	_indexPos = pos;
	calculatePathToPlayer(mapData);
	return false;
}
