/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** test
*/

#include <irrlicht.h>
#include "EventReceiver.hpp"

indie::EventReceiver::EventReceiver()
{
	for (int i = 0; i <= irr::KEY_KEY_CODES_COUNT; i++)
	{
		keyState[i] = UP;
	}

	for (int i = 0; i <= 2; i++)
	{
		mouseButtonState[i] = UP;
	}

	mouse.wheel = 0.0f;
}

bool indie::EventReceiver::OnEvent(const irr::SEvent& event)
{
	bool eventProcessed = false;

	if (event.EventType == irr::EET_KEY_INPUT_EVENT)
	{
		if (!event.KeyInput.PressedDown) {
			if (keyState[event.KeyInput.Key] != UP)
				keyState[event.KeyInput.Key] = RELEASED;
		} else {
			if (keyState[event.KeyInput.Key] != DOWN)
				keyState[event.KeyInput.Key] = PRESSED;
			else
				keyState[event.KeyInput.Key] = DOWN; // Set to Down
		}
		eventProcessed = true;
	}

	if (event.EventType == irr::EET_MOUSE_INPUT_EVENT) {
		if (event.MouseInput.Event == irr::EMIE_MOUSE_MOVED) {
			mouse.pos.second = event.MouseInput.Y;
			mouse.pos.first = event.MouseInput.X;
		}

		if (event.MouseInput.Event == irr::EMIE_MOUSE_WHEEL)
			mouse.wheel += event.MouseInput.Wheel;

		if (event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN) {
			if (mouseButtonState[0] == UP || mouseButtonState[0] == RELEASED)
				mouseButtonState[0] = PRESSED;
			else
				mouseButtonState[0] = DOWN;
		}

		if (event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP && mouseButtonState[0] != UP)
			mouseButtonState[0] = RELEASED;

		if (event.MouseInput.Event == irr::EMIE_MMOUSE_PRESSED_DOWN) {
			if (mouseButtonState[1] == UP || mouseButtonState[1] == RELEASED)
				mouseButtonState[1] = PRESSED;
			else
				mouseButtonState[1] = DOWN;
		}

		if (event.MouseInput.Event == irr::EMIE_MMOUSE_LEFT_UP && mouseButtonState[1] != UP)
			mouseButtonState[1] = RELEASED;

		if (event.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN) {
			if (mouseButtonState[2] == UP || mouseButtonState[2] == RELEASED)
				mouseButtonState[2] = PRESSED;
			else
				mouseButtonState[2] = DOWN;
		}

		if (event.MouseInput.Event == irr::EMIE_RMOUSE_LEFT_UP && mouseButtonState[2] != UP)
			mouseButtonState[2] = RELEASED;

		eventProcessed = true;
	}

	return eventProcessed;
}

float indie::EventReceiver::mouseWheel()
{
	return mouse.wheel;
}

std::pair<int, int> indie::EventReceiver::mousePos()
{
	return mouse.pos;
}

bool indie::EventReceiver::leftMouseReleased()
{
	return mouseButtonState[0] == RELEASED;
}
bool indie::EventReceiver::leftMouseUp()
{
	return mouseButtonState[0] == RELEASED || mouseButtonState[0] == UP;
}

bool indie::EventReceiver::leftMousePressed()
{
	return mouseButtonState[0] == PRESSED;
}

bool indie::EventReceiver::leftMouseDown()
{
	return mouseButtonState[0] == PRESSED || mouseButtonState[0] == DOWN;
}

bool indie::EventReceiver::middleMouseReleased()
{
	return mouseButtonState[1] == RELEASED;
}
bool indie::EventReceiver::middleMouseUp()
{
	return mouseButtonState[1] == RELEASED || mouseButtonState[1] == UP;
}

bool indie::EventReceiver::middleMousePressed()
{
	return mouseButtonState[1] == PRESSED;
}

bool indie::EventReceiver::middleMouseDown()
{
	return mouseButtonState[1] == PRESSED || mouseButtonState[1] == DOWN;
}

bool indie::EventReceiver::rightMouseReleased()
{
	return mouseButtonState[2] == RELEASED;
}
bool indie::EventReceiver::rightMouseUp()
{
	return mouseButtonState[2] == RELEASED || mouseButtonState[2] == UP;
}

bool indie::EventReceiver::rightMousePressed()
{
	return mouseButtonState[2] == PRESSED;
}

bool indie::EventReceiver::rightMouseDown()
{
	return mouseButtonState[2] == PRESSED || mouseButtonState[2] == DOWN;
}//

bool indie::EventReceiver::keyPressed(const std::string &keyCode)
{
	return keyState[convertor.getKey(keyCode)] == PRESSED;
}

bool indie::EventReceiver::keyDown(const std::string &keyCode)
{
	return keyState[convertor.getKey(keyCode)] == DOWN || keyState[convertor.getKey(keyCode)] == PRESSED;
}

bool indie::EventReceiver::keyUp(const std::string &keyCode)
{
	return keyState[convertor.getKey(keyCode)] == UP || keyState[convertor.getKey(keyCode)] == RELEASED;
}

bool indie::EventReceiver::keyReleased(const std::string &keyCode)
{
	return keyState[convertor.getKey(keyCode)] == RELEASED;
}

const std::string &indie::EventReceiver::getLastKey()
{
	for (int i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
		if (keyState[i] == PRESSED) {
			return convertor.getKey(i);
		}
	}
	return convertor.getKey(0);
}
