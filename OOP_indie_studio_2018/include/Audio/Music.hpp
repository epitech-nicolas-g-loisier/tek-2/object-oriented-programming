/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Music
*/

#ifndef MUSIC_HPP
#define MUSIC_HPP

#include <SFML/Audio.hpp>

namespace indie
{
class Music
{
public:
	explicit Music(const std::string &file);
	~Music() = default;

	void switchMusic(const std::string &file);
	void stop();
	void play();
private:
	sf::Music music;
	std::string current;
};
}

#endif //MUSIC_HPP
