/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** CriterionWindow definition
*/
#include <vector>
#include "IGraphical.hpp"
#include "Assets.hpp"

#ifndef CRITERION_WINDOW_
	#define CRITERION_WINDOW_

class CriterionWindow : public IGraphical
{
	void drawLine(const Arcade::Line &);
	void drawRect(const Arcade::Rect &);
	void drawPair(const Arcade::Pair &);
	void update();
	std::vector<std::string> getInput();
};

#endif // CRITERION_WINDOW_