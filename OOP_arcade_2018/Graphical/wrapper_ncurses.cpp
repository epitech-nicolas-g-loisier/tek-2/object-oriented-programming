/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** ncurse wrapper
*/
#include "IGraphical.hpp"
#include <ncurses.h>
#include <iostream>
#include <memory>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>

__attribute__ ((constructor))
void wrapper_ctor()
{
}

__attribute__ ((destructor))
void wrapper_dtor()
{
	noraw();
	nocbreak();
	endwin();
}

class NcursesWindow : public IGraphical
{
public:
	NcursesWindow();
	~NcursesWindow();
	void drawLine(const Arcade::Line &) final;
	void drawRect(const Arcade::Rect &) final;
	void drawPair(const Arcade::Pair &) final;
	void update() final;
	std::vector<std::string> getInput() final;

private:
	WINDOW *window;
	std::vector<std::pair<int, const char*>> bindings;
	std::vector<std::string> map;
	std::vector<std::string> mapColor;
};

NcursesWindow::NcursesWindow()
	: window(initscr())
{
	timeout(0);
	noecho();
	cbreak();
	curs_set(0);
	keypad(window, true);
	for (int i = 0; i < 40; i++)
		map.push_back(std::string(80, ' '));
	for (int i = 0; i < 40; i++)
		mapColor.push_back(std::string(80, '0'));
	bindings.clear();
	bindings.push_back(std::pair<int, const char*>(KEY_UP, "UP"));
	bindings.push_back(std::pair<int, const char*>(KEY_DOWN, "DOWN"));
	bindings.push_back(std::pair<int, const char*>(KEY_LEFT, "LEFT"));
	bindings.push_back(std::pair<int, const char*>(KEY_RIGHT, "RIGHT"));
	bindings.push_back(std::pair<int, const char*>(27, "ESCAPE"));
	bindings.push_back(std::pair<int, const char*>('\n', "RETURN"));
	bindings.push_back(std::pair<int, const char*>(KEY_BACKSPACE, "BACKSPACE"));
	bindings.push_back(std::pair<int, const char*>(' ', " "));
	bindings.push_back(std::pair<int, const char*>('0', "0"));
	bindings.push_back(std::pair<int, const char*>('1', "1"));
	bindings.push_back(std::pair<int, const char*>('2', "2"));
	bindings.push_back(std::pair<int, const char*>('3', "3"));
	bindings.push_back(std::pair<int, const char*>('4', "4"));
	bindings.push_back(std::pair<int, const char*>('5', "5"));
	bindings.push_back(std::pair<int, const char*>('6', "6"));
	bindings.push_back(std::pair<int, const char*>('7', "7"));
	bindings.push_back(std::pair<int, const char*>('8', "8"));
	bindings.push_back(std::pair<int, const char*>('9', "9"));
	bindings.push_back(std::pair<int, const char*>('A', "A"));
	bindings.push_back(std::pair<int, const char*>('B', "B"));
	bindings.push_back(std::pair<int, const char*>('C', "C"));
	bindings.push_back(std::pair<int, const char*>('D', "D"));
	bindings.push_back(std::pair<int, const char*>('E', "E"));
	bindings.push_back(std::pair<int, const char*>('F', "F"));
	bindings.push_back(std::pair<int, const char*>('G', "G"));
	bindings.push_back(std::pair<int, const char*>('H', "H"));
	bindings.push_back(std::pair<int, const char*>('I', "I"));
	bindings.push_back(std::pair<int, const char*>('J', "J"));
	bindings.push_back(std::pair<int, const char*>('K', "K"));
	bindings.push_back(std::pair<int, const char*>('L', "L"));
	bindings.push_back(std::pair<int, const char*>('M', "M"));
	bindings.push_back(std::pair<int, const char*>('N', "N"));
	bindings.push_back(std::pair<int, const char*>('O', "O"));
	bindings.push_back(std::pair<int, const char*>('P', "P"));
	bindings.push_back(std::pair<int, const char*>('Q', "Q"));
	bindings.push_back(std::pair<int, const char*>('R', "R"));
	bindings.push_back(std::pair<int, const char*>('S', "S"));
	bindings.push_back(std::pair<int, const char*>('T', "T"));
	bindings.push_back(std::pair<int, const char*>('U', "U"));
	bindings.push_back(std::pair<int, const char*>('V', "V"));
	bindings.push_back(std::pair<int, const char*>('W', "W"));
	bindings.push_back(std::pair<int, const char*>('X', "X"));
	bindings.push_back(std::pair<int, const char*>('Y', "Y"));
	bindings.push_back(std::pair<int, const char*>('Z', "Z"));
	bindings.push_back(std::pair<int, const char*>('a', "A"));
	bindings.push_back(std::pair<int, const char*>('b', "B"));
	bindings.push_back(std::pair<int, const char*>('c', "C"));
	bindings.push_back(std::pair<int, const char*>('d', "D"));
	bindings.push_back(std::pair<int, const char*>('e', "E"));
	bindings.push_back(std::pair<int, const char*>('f', "F"));
	bindings.push_back(std::pair<int, const char*>('g', "G"));
	bindings.push_back(std::pair<int, const char*>('h', "H"));
	bindings.push_back(std::pair<int, const char*>('i', "I"));
	bindings.push_back(std::pair<int, const char*>('j', "J"));
	bindings.push_back(std::pair<int, const char*>('k', "K"));
	bindings.push_back(std::pair<int, const char*>('l', "L"));
	bindings.push_back(std::pair<int, const char*>('m', "M"));
	bindings.push_back(std::pair<int, const char*>('n', "N"));
	bindings.push_back(std::pair<int, const char*>('o', "O"));
	bindings.push_back(std::pair<int, const char*>('p', "P"));
	bindings.push_back(std::pair<int, const char*>('q', "Q"));
	bindings.push_back(std::pair<int, const char*>('r', "R"));
	bindings.push_back(std::pair<int, const char*>('s', "S"));
	bindings.push_back(std::pair<int, const char*>('t', "T"));
	bindings.push_back(std::pair<int, const char*>('u', "U"));
	bindings.push_back(std::pair<int, const char*>('v', "V"));
	bindings.push_back(std::pair<int, const char*>('w', "W"));
	bindings.push_back(std::pair<int, const char*>('x', "X"));
	bindings.push_back(std::pair<int, const char*>('y', "Y"));
	bindings.push_back(std::pair<int, const char*>('z', "Z"));

	start_color();
	init_pair(0, Arcade::Color::BLACK, Arcade::Color::BLACK);
	init_pair(1, Arcade::Color::RED, Arcade::Color::BLACK);
	init_pair(2, Arcade::Color::GREEN, Arcade::Color::BLACK);
	init_pair(3, Arcade::Color::YELLOW, Arcade::Color::BLACK);
	init_pair(4, Arcade::Color::BLUE, Arcade::Color::BLACK);
	init_pair(5, Arcade::Color::MAGENTA, Arcade::Color::BLACK);
	init_pair(6, Arcade::Color::CYAN, Arcade::Color::BLACK);
	init_pair(7, Arcade::Color::WHITE, Arcade::Color::BLACK);
	init_pair(8, Arcade::Color::WHITE, Arcade::Color::BLACK);
	init_pair(9, Arcade::Color::WHITE, Arcade::Color::RED);
	init_pair(10, Arcade::Color::WHITE, Arcade::Color::GREEN);
	init_pair(11, Arcade::Color::WHITE, Arcade::Color::YELLOW);
	init_pair(12, Arcade::Color::WHITE, Arcade::Color::BLUE);
	init_pair(13, Arcade::Color::WHITE, Arcade::Color::MAGENTA);
	init_pair(14, Arcade::Color::WHITE, Arcade::Color::CYAN);
	init_pair(15, Arcade::Color::BLACK, Arcade::Color::WHITE);
}

NcursesWindow::~NcursesWindow()
{
}

void NcursesWindow::drawLine(const Arcade::Line &line)
{
	int realX = line.pos.first;
	int realY = line.pos.second;
	int realSize = line.length;
	int symb = -1 + 2 * (line.dir % 2);
	if (line.dir < 2) {
		for (int i = 0; i < realSize; i++)
			map[realY + i * symb][realX] = 'L';
	} else {
		for (int i = 0; i < realSize; i++)
			map[realY][realX + i * symb] = 'L';
	}
}

void NcursesWindow::drawRect(const Arcade::Rect &rect)
{
	int realX = rect.pos.first + 0.5;
	int realY = rect.pos.second + 0.5;
	int realLength = rect.size.first + 0.5;
	int realHeight = rect.size.second + 0.5;

	if (realX < 0 || realX >= 80 || realY < 0 || realY >= 40)
		return;
	if (rect.isOutlined) {
		for (int i = 0; i <= realLength + 1; i++) {
			if (realX + i > 0 && realY > 0 && realX + i <= 80){
				mapColor[realY - 1][realX + i - 1] = Arcade::Color::WHITE;
				map[realY - 1][realX + i - 1] = '*';
			}
			if (realX + i > 0 && realY + realHeight < 40 && realX + i <= 80) {
				mapColor[realY + realHeight][realX + i - 1] = Arcade::Color::WHITE;
				map[realY + realHeight][realX + i - 1] = '*';
			}
		}
		for (int i = 0; i <= realHeight + 1; i++) {
			if (realY + i > 0 && realX > 0 && realY + i <= 40) {
				mapColor[realY + i - 1][realX - 1] = Arcade::Color::WHITE;
				map[realY + i - 1][realX - 1] = '*';
			}
			if (realY + i > 0 && realX + realLength + 1 < 80 && realY + i <= 40) {
				mapColor[realY + i - 1][realX + realLength] = Arcade::Color::WHITE;
				map[realY + i - 1][realX + realLength] = '*';
			}
		}
	}
	for (int i = 0; i < realHeight; i++) {
		for (int j = 0; j < realLength; j++)
			if (realY + i < 40 && realX + j < 80) {
				mapColor[realY + i][realX + j] = rect.defaultColor;
				map[realY + i][realX + j] = rect.termSymbol;
			}
	}
}

void NcursesWindow::drawPair(const Arcade::Pair &pair)
{
	int realX = pair.pos.first + 0.5;
	int realY = pair.pos.second + 0.5;

	int iterX = realX;

	for (auto &&i: pair.value.first) {
		mapColor[realY][iterX] = pair.background ? pair.color + 8 : pair.color;
		map[realY][iterX] = i;
		iterX++;
	}
	if (pair.value.second >= 0) {
		std::stringstream ss;
		ss << std::setfill('0') << std::setw(6) << pair.value.second;
		std::string value = ss.rdbuf()->str();
		for (auto &&i: value) {
			mapColor[realY + 1][realX] = pair.background ? pair.color + 8 : pair.color;
			map[realY + 1][realX] = i;
			realX++;
		}
	}
}

void NcursesWindow::update()
{
	while (COLS <= 80 || LINES <= 40) {
		erase();
		mvprintw(LINES / 2, COLS / 2 - 11, "Resize to a bigger size");
		refresh();
	}
	clear();
	for (int i = 0; i < 40; i++) {
		for (int j = 0; j < 80; j++) {
			attron(COLOR_PAIR(mapColor[i][j]));
			mvaddch(i, j, map[i][j]);
			attroff(COLOR_PAIR(mapColor[i][j]));
		}
		map[i] = std::string(80, ' ');
		mapColor[i] = std::string(80, Arcade::Color::BLACK);
	}
	mvaddnstr(39, 70, "NCURSES", 7);
	std::this_thread::sleep_for(std::chrono::milliseconds(40));
}

std::vector<std::string> NcursesWindow::getInput()
{
	std::vector<std::string> input;
	int keyPressed = getch();

	while (keyPressed != -1) {
		for (unsigned int i = 0; i < bindings.size(); i++) {
			if (bindings[i].first == keyPressed) {
				input.push_back(std::string(bindings[i].second));
				break;
			}
		}
		keyPressed = getch();
	}
	return (input);
}

extern "C" std::unique_ptr<IGraphical> getIGraphical()
{
	return (std::make_unique<NcursesWindow>());
}