/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameException
*/
#include <exception>
#include <string>

#ifndef INDIE_STUDIO_GAMEEXCEPTION_HPP
#define INDIE_STUDIO_GAMEEXCEPTION_HPP

namespace indie {
	class GameException : public std::exception {
	public:
		GameException(std::string message, std::string className = "Unknown");
		const char *what() const noexcept override;

	private:
		std::string _message;
		std::string _className;
	};
}

#endif //INDIE_STUDIO_GAMEEXCEPTION_HPP
