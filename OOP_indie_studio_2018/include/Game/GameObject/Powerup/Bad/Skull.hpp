/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Skull
*/

#ifndef INDIE_STUDIO_SKULL_HPP
#define INDIE_STUDIO_SKULL_HPP

#include "PowerUp.hpp"

namespace indie {
	class Skull : virtual public PowerUp {
	public:
		Skull(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //INDIE_STUDIO_SKULL_HPP
