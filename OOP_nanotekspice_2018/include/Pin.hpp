/*
** EPITECH PROJECT, 2019
** OOP Nano
** File description:
** Pin Struct
*/

#ifndef PIN_HPP_
	#define PIN_HPP_

#include "IComponent.hpp"

struct InputPin {
	InputPin(nts::IComponent &other, size_t pin)
		: pinNumber(pin), prev(other)
	{};
	size_t pinNumber = 0;
	nts::IComponent &prev;
};

struct OutputPin {
	OutputPin(size_t pin1, size_t pin2)
	{
		pinNumber1 = pin1;
		pinNumber2 = pin2;
	};
	size_t pinNumber1 = 0;
	size_t pinNumber2 = 0;
};

#endif /* !PIN_HPP_ */
