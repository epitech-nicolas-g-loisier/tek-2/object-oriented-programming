/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** SimpleType
*/

#ifndef SIMPLETYPE_HPP_
#define SIMPLETYPE_HPP_

#include <string>
#include <variant>

namespace XML
{

class SimpleType {
public:
	struct undef : std::string {};

	SimpleType(const std::string &field);
	SimpleType() = default;
	~SimpleType() = default;

	template <class T>
	T get() const;

private:
	typedef std::variant<std::string, int, float, bool, undef> Data;
	Data data;
};

}

#endif /* !SIMPLETYPE_HPP_ */
