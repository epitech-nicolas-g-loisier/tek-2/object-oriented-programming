/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** BombDown
*/

#ifndef BOMBERMAN_BOMBDOWN_HPP
#define BOMBERMAN_BOMBDOWN_HPP

#include "PowerUp.hpp"

namespace indie {
	class BombDown : virtual public PowerUp {
	public:
		BombDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //BOMBERMAN_BOMBDOWN_HPP
