/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** EventReceiver
*/

#ifndef EVENT_RECEIVER_HPP
#define EVENT_RECEIVER_HPP

#include <irrlicht.h>
#include "InputConvertor.hpp"

namespace indie
{
class EventReceiver : public irr::IEventReceiver
{
public:

	EventReceiver();
	~EventReceiver() override = default;

	float mouseWheel();
	std::pair<int, int> mousePos();

	bool leftMouseReleased();
	bool leftMouseUp();
	bool leftMousePressed();
	bool leftMouseDown();

	bool middleMouseReleased();
	bool middleMouseUp();
	bool middleMousePressed();
	bool middleMouseDown();

	bool rightMouseReleased();
	bool rightMouseUp();
	bool rightMousePressed();
	bool rightMouseDown();

	bool keyPressed(const std::string &keyCode);
	bool keyDown(const std::string &keyCode);
	bool keyUp(const std::string &keyCode);
	bool keyReleased(const std::string &keyCode);

	const std::string &getLastKey();

protected:
	enum keyStatesENUM {UP, DOWN, PRESSED, RELEASED};
	keyStatesENUM mouseButtonState[3]{};
	keyStatesENUM keyState[irr::KEY_KEY_CODES_COUNT]{};
	InputConvertor convertor;

	struct mouseData
	{
		std::pair<int, int> pos;
		float wheel;
	};
	struct mouseData mouse{};

	bool OnEvent(const irr::SEvent& event) override;
};
}

#endif //EVENT_RECEIVER_HPP
