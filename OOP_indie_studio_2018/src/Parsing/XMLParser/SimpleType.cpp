/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** SimpleType
*/

#include <exception>
#include <algorithm>
#include <string>
#include <stdexcept>
#include "XMLParser/SimpleType.hpp"

namespace XML
{

SimpleType::SimpleType(const std::string &field)
{
    if (field.empty())
        throw std::invalid_argument("Field is empty");

    if (field.find('"') != std::string::npos) {
        auto strbegin = field.find('"');
        auto strend = field.rfind('"');
        if (std::count(field.begin(), field.end(), '"') != 2)
            throw std::invalid_argument("Invalid String");
        data = std::string(field.begin() + strbegin + 1, field.begin() + strend);
    } else if (std::count(field.begin(), field.end(), '.') == 1) {
        data = float((std::atof(field.c_str())));
    } else if (field == "true" || field == "false") {
        data = bool((field == "true")? true : false);
    } else {
        try {
            data = int(std::atoi(field.c_str()));
        } catch (std::exception &e) {
            data = std::string(field);
        }
    }
}

template<>
std::string SimpleType::get<std::string>() const
{
    return std::get<std::string>(data);
}

template<>
float SimpleType::get<float>() const
{
    auto f = std::get_if<float>(&data);
    if (f)
        return *f;
    else
        return std::get<int>(data);
}

template<>
int SimpleType::get<int>() const
{
    return std::get<int>(data);
}

template<>
bool SimpleType::get<bool>() const
{
    return std::get<bool>(data);
}

}
