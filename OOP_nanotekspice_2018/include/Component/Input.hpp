/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** input
*/

#ifndef INPUT_HPP_
	#define INPUT_HPP_

#include "IInput.hpp"
#include "Pin.hpp"

namespace nts {

	class Input : public IInput
	{
	public:
		Input(const std::string name, const Tristate value = nts::UNDEFINED);
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;
		void setValue(nts::Tristate);
		void update() {};

	private:
		std::string _name;
		Tristate _value;
	};
}

#endif /* !INPUT_HPP_ */
