/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** IPlayerNode
*/

#ifndef IPLAYERNODE_HPP
#define IPLAYERNODE_HPP

#include "INode.hpp"

namespace indie
{
class IPlayerNode : virtual public indie::INode
{
public:
	virtual ~IPlayerNode() = default;
	virtual void move() = 0;
	virtual void idle() = 0;
	virtual bool dance() = 0;
	virtual void setPositionY(float) = 0;

};
}

#endif //IPLAYERNODE_HPP
