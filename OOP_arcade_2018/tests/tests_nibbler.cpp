/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Tests for nibbler
*/
#include <criterion/criterion.h>
#include <unistd.h>
#include "CriterionWindow.hpp"
#include "Nibbler.hpp"

Test(Nibbler, computeInput)
{
	nibbler::Nibbler game;
	std::vector<std::string> input;
	input.push_back("Z");
	cr_assert(game.computeInput(input).first, "Game ended early, UP");
	sleep(1);
	input.clear();
	input.push_back("Q");
	cr_assert(game.computeInput(input).first, "Game ended early, LEFT");
	sleep(1);
	input.clear();
	input.push_back("S");
	cr_assert(game.computeInput(input).first, "Game ended early, DOWN");
	sleep(1);
	input.clear();
	input.push_back("D");
	cr_assert(game.computeInput(input).first, "Game ended early, RIGHT");
	sleep(1);
	input.clear();
	input.push_back(" ");
	cr_assert(game.computeInput(input).first, "Game ended early, Speed up");
	CriterionWindow win;
	game.display(win);
}