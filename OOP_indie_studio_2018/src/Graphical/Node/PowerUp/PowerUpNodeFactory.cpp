/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpNodeFactory
*/

#include <iostream>
#include "GraphicalException.hpp"
#include "PowerUpNodeFactory.hpp"
#include "GraphicalEngine.hpp"

indie::PowerUpNodeFactory::PowerUpNodeFactory()
{
	_ctors["BombUp"] = &PowerUpNodeFactory::createBombUpNode;
	_ctors["BombDown"] = &PowerUpNodeFactory::createBombDownNode;
	_ctors["DangerousBomb"] = &PowerUpNodeFactory::createDangerousBombNode;
	_ctors["LineBomb"] = &PowerUpNodeFactory::createlineBombNode;
	_ctors["PierceBomb"] = &PowerUpNodeFactory::createPierceBombNode;
	_ctors["FireUp"] = &PowerUpNodeFactory::createFireUpNode;
	_ctors["FireDown"] = &PowerUpNodeFactory::createFireDownNode;
	_ctors["FireMax"] = &PowerUpNodeFactory::createFireMaxNode;
	_ctors["Heart"] = &PowerUpNodeFactory::createHeartNode;
	_ctors["Kick"] = &PowerUpNodeFactory::createKickNode;
	_ctors["Punch"] = &PowerUpNodeFactory::createPunchNode;
	_ctors["SpeedUp"] = &PowerUpNodeFactory::createSpeedUpNode;
	_ctors["SpeedDown"] = &PowerUpNodeFactory::createSpeedDownNode;
	_ctors["Skull"] = &PowerUpNodeFactory::createSkullNode;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createNode(indie::GraphicalEngine &engine, const std::string &type)
{
	if (_ctors.count(type) != 0) {
		return (this->*_ctors[type])(engine);
	}
	return nullptr;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createBombUpNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Mushroom/Mushroom.3ds").data());
	if (!mesh)
		throw ModelLoadingError("BatNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("BatNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Mushroom/BombUp.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Blast" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createBombDownNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Mushroom/Mushroom.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createBombDownNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createBombDownNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Mushroom/BombDown.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for BombDown" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createDangerousBombNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Pow/Pow.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createDangerousBombNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createDangerousBombNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Pow/0_model0.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Pow" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createPierceBombNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Bill/Bill.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createPierceBombNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createPierceBombNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Bill/Env2.png").data());
	irr::video::ITexture *texture1 = engine.driver->getTexture(engine.findModelPath("PowerUp/Bill/killer_eye_around.png").data());
	irr::video::ITexture *texture2 =engine.driver->getTexture(engine.findModelPath("PowerUp/Bill/MagunamKillerBody.png").data());
	irr::video::ITexture *texture3 =engine.driver->getTexture(engine.findModelPath("PowerUp/Bill/MagunamKillerHa.png").data());
	if (!texture || !texture1 || !texture2 || !texture3)
		std::cerr << "Cannot find texture for Bill" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->getMaterial(0).setTexture(0, texture3);
		node->getMaterial(1).setTexture(0, texture);
		node->getMaterial(2).setTexture(0, texture1);
		node->getMaterial(3).setTexture(0, texture2);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createlineBombNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Scope/Scope.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createlineBombNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createlineBombNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Scope/model11.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Scope" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createFireUpNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Flower/Flower.3ds").data());
	if (!mesh)
		throw ModelLoadingError("PowerUpNodeFactory");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("PowerUpNodeFactory");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Flower/FireUp.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for FireUp" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createFireDownNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Flower/Flower.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createFireDownNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createFireDownNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Flower/FireDown.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Blast" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createFireMaxNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Flower/Flower.3ds").data());
	if (!mesh)
		throw ModelLoadingError("BatNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("BatNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Flower/FireMax.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Blast" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createHeartNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Mushroom/Mushroom.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createHeartNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createHeartNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Mushroom/Heart.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Heart" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createKickNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Hammer/Hammer.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createKickNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createKickNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Hammer/0_model0.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Hammer" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createPunchNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Bat/Bat.3ds").data());
	if (!mesh)
		throw ModelLoadingError("BatNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("BatNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Bat/0_model0.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Blast" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createSpeedUpNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Fruit/Fruit.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createSpeedUpNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createSpeedUpNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Fruit/FruitGood.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for FruitGood" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createSpeedDownNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Fruit/Fruit.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createSpeedDownNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createSpeedDownNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Fruit/FruitBad.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for FruitBad" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}

irr::scene::IMeshSceneNode *indie::PowerUpNodeFactory::createSkullNode(indie::GraphicalEngine &engine) const
{
	irr::scene::IMesh *mesh = engine.scenemgr->getMesh(engine.findModelPath("PowerUp/Skull/Skull.3ds").data());
	if (!mesh)
		throw ModelLoadingError("createSkullNode");

	irr::scene::IMeshSceneNode *node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("createSkullNode");

	irr::video::ITexture *texture = engine.driver->getTexture(engine.findModelPath("PowerUp/Skull/HumanSkull_sm.png").data());
	if (!texture)
		std::cerr << "Cannot find texture for Skull" << std::endl;
	else {
		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setMaterialTexture(0, texture);
	}

	return node;
}
