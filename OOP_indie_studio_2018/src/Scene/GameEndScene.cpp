/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameEndScene
*/

#include "GameEndScene.hpp"

indie::GameEndScene::GameEndScene(indie::GraphicalEngine &engine, indie::GameOption &option)
	:	_engine(engine), gameOption(option), sound(gameOption.winner > gameOption.player ? "effect/Defeat.ogg" : "effect/Victory.ogg")
{
	engine.endGame();
	if (option.winner != -1) {
		winner = engine.playerNodeFactory.createPlayerNode(option.character[option.winner - 1], engine);
		winner->setRotation(45);
		winner->dance();
		engine.setCam({0, 3, 5}, {0, 0, 0});
	} else
		winner = nullptr;
	sound.play();
}

indie::IScene::sceneState indie::GameEndScene::draw()
{
	_engine.beginScene({});
	if (_engine.receiver.keyDown("escape"))
		return indie::IScene::BEGIN;

	if (gameOption.winner > gameOption.player)
		drawDefeat();
	else if (gameOption.winner == -1)
		drawEquality();
	else
		drawVictory();

	_engine.endScene();
	return indie::IScene::GAMEEND;
}

void indie::GameEndScene::drawVictory()
{
	_engine.drawRectangle({0, 0, 1920, 1080}, {0, 0, 255, 255});
	_engine.drawText("player" + std::to_string(gameOption.player) + " Win", "large", {255, 255, 255, 255}, {0, 0, 1920, 1080});
	_engine.drawText("Press Escape to return to menu", "medium", {255, 255, 255, 255}, {0, 1000, 1920, 1080});
	_engine.scenemgr->drawAll();
}

void indie::GameEndScene::drawDefeat()
{
	_engine.drawRectangle({0, 0, 1920, 1080}, {255, 0, 0, 255});
	_engine.drawTexture("Screen/Game/BowserHead.png", {510, 90});
	_engine.drawText("You Lose", "large", {255, 255, 255, 255}, {0, 0, 1920, 1080});
	_engine.drawText("Press Escape to return to menu", "medium", {255, 255, 255, 255}, {0, 1000, 1920, 1080});
	_engine.scenemgr->drawAll();
}

void indie::GameEndScene::drawEquality()
{
	_engine.drawRectangle({0, 0, 1920, 1080}, {0, 255, 0, 255});
	_engine.drawText("Draw", "large", {255, 255, 255, 255}, {0, 0, 1920, 1080});
	_engine.drawText("Press Escape to return to menu", "medium", {255, 255, 255, 255}, {0, 1000, 1920, 1080});
}
