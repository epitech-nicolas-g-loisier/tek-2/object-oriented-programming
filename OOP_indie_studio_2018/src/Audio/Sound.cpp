/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Sound
*/

#include <iostream>
#include "AssetPath.hpp"
#include "Sound.hpp"

indie::Sound::Sound(const std::string &file)
{
	buffer.loadFromFile(SoundPath + "/" + file);
	sound.setBuffer(buffer);
}

void indie::Sound::play()
{
	sound.play();
}
