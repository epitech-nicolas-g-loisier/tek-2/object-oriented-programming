/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** InputConvertor
*/

#ifndef INPUTCONVERTOR_HPP
#define INPUTCONVERTOR_HPP

#include <map>
#include <string>
#include "irrlicht.h"

class InputConvertor
{
public:
	InputConvertor();
	~InputConvertor() = default;

	irr::EKEY_CODE getKey(const std::string&);
	const std::string &getKey(int);

private:
	std::map<std::string, irr::EKEY_CODE> keys;
	std::map<int, std::string> keysFromIrr;

};

#endif //INPUTCONVERTOR_HPP
