/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Bomb
*/

#include <iostream>
#include "BombNode.hpp"
#include "GraphicalException.hpp"

indie::BombNode::BombNode(GraphicalEngine &engine)
{
	mesh = engine.scenemgr->getMesh(engine.findModelPath("Bomb/model.dae").data());
	if (!mesh)
		throw ModelLoadingError("BombNode");

	texture[0] = engine.driver->getTexture(engine.findModelPath("Bomb/modeMM00.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("Bomb/modeSS02.png").data());
	if (!texture[0] || !texture[1])
		std::cerr << "Cannot find texture for Bomb" << std::endl;

	node = engine.scenemgr->addMeshSceneNode(mesh);
	if (!node)
		throw NodeCreationError("BombNode");

	node->setScale({0.013, 0.013, 0.013});
	node->setPosition({1, -1, 1});
	setTexture();

}

indie::BombNode::~BombNode()
{
	node->remove();
}

void indie::BombNode::setTexture()
{
	if (!texture[state])
		return;
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->getMaterial(0).setTexture(0, texture[state]);
}

std::pair<float, float> indie::BombNode::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::BombNode::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::BombNode::getRotation()
{
	return node->getRotation().Y;
}

void indie::BombNode::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::BombNode::swapTexture(char choice)
{
	state = choice;
	setTexture();
}
