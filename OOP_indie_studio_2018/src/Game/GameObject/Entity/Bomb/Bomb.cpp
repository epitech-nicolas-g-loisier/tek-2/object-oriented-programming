/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Bomb
*/

#include "Bomb.hpp"
#include "MapVisitors.hpp"
#include <iostream>
#include <cmath>

indie::Bomb::Bomb(unsigned char fireLevel, std::pair<unsigned char, unsigned char>&pos, GraphicalEngine &_engine, indie::AudioEngine &_audioEngine)
	: level(fireLevel), _pos(pos), deathTime(std::chrono::steady_clock::now() + std::chrono::seconds(3)), node(_engine), engine(_engine), audioEngine(_audioEngine)
{
	node.setPosition(pos);
}

bool indie::Bomb::update(indie::MapData &map)
{
	bool isAlive = std::chrono::steady_clock::now() < deathTime;
	if (isAlive)
		return isAlive;
	audioEngine.playBombSound();
	for (std::pair<unsigned char, unsigned char>i = {_pos.first - 1, _pos.second}; i.first >= _pos.first - level && i.first < 15; i.first--)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first + 1, _pos.second}; i.first <= _pos.first + level && i.first < 15 ; i.first++)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first, _pos.second - 1}; i.second >= _pos.second - level && i.second < 9; i.second--)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first, _pos.second + 1}; i.second <= _pos.second + level && i.second < 9; i.second++)
		if (!addBlastToMap(map, i))
			break;
	auto blast = std::make_shared<indie::Blast>(_pos, engine, false);
	map.entityMap[_pos] = blast;
	map.entities.push_back(std::move(blast));
	return (isAlive);
}

bool indie::Bomb::addBlastToMap(indie::MapData &map, const std::pair<unsigned char, unsigned char> &i) const
{
	auto blast = std::make_shared<indie::Blast>(i, engine, false);
	bool continueStatus = true;
	if (map.entityMap.count(i)) {
		std::weak_ptr<IGameObject> weakPtr;
		std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[i]);
		if (weakPtr.lock()->interact(blast.get()))
			return (false);
		EntityTypeVisitor typeVisitor;
		std::visit(typeVisitor, map.entityMap[i]);
		continueStatus = typeVisitor.type != typeVisitor.BLOCK;
	}
	map.entityMap[i] = blast;
	map.entities.push_back(std::move(blast));
	return (continueStatus);
}

void indie::Bomb::detonate()
{
	deathTime -= std::chrono::seconds(5);
}

bool indie::Bomb::interact(indie::Player *player)
{
	if (player)
		player->moving = false;
	return (true);
}

bool indie::Bomb::interact(indie::Bomb *)
{
	return (true);
}

bool indie::Bomb::interact(indie::Blast *blast)
{
	if (blast)
		deathTime -= std::chrono::seconds(5);
	return (true);
}

unsigned char indie::Bomb::getTimer() const
{
	long timer = (deathTime - std::chrono::steady_clock::now()).count();
	return (timer > 255 ? 255 : timer);
}

bool indie::Bomb::interact(indie::PowerUp *)
{
	return false;
}
