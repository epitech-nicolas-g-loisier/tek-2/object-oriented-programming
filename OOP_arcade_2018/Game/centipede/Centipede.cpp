/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Centipede game
*/
#include <ctime>
#include <algorithm>
#include "Centipede.hpp"

Centipede::Game::Game()
	: _enemies(), _player(15, 35), _shot(-1, -1), _score(0), _enemiesLeft(20)
{
	std::srand(std::time(nullptr));
	for (int i = std::rand() % 20 + 10; i > 0; i--)
		_obstacles.push_back({{std::rand() % 35 + 1, std::rand() % 29 + 1}, 5});
	start = std::chrono::steady_clock::now().time_since_epoch();
}

std::pair<bool, int> Centipede::Game::computeInput(const std::vector<std::string> &input)
{
	std::chrono::nanoseconds end = std::chrono::steady_clock::now().time_since_epoch();
	if ((end - start).count() > 20000000) {
		start = end;
		for (auto &&i: input) {
			if (!i.compare("Q") && _player.first > 0 && checkPlayerColision({-1, 0}))
				_player.first--;
			if (!i.compare("D") && _player.first < 39 && checkPlayerColision({1, 0}))
				_player.first++;
			if (!i.compare("Z") && _player.second > 30 && checkPlayerColision({0, -1}))
				_player.second--;
			if (!i.compare("S") && _player.second < 38 && checkPlayerColision({0, 1}))
				_player.second++;
			if (!i.compare("A") && _shot.second == -1)
				_shot = std::pair<int, int>(_player.first, _player.second);
		}
	}
	if (_enemies.size() == 0) {
		if (_enemiesLeft <= 0) {
			int score = _score;
			_enemiesLeft = 20;
			_shot.second = -1;
			_score = 0;
			_player = {15, 35};
			std::srand(std::time(nullptr));
			return std::pair<bool, int>(false, score);
		}
		_enemies.push_back(std::make_unique<Centipede::Enemy>(4 + std::rand() % 3));
		_enemiesLeft--;
	}
	if (_enemiesLeft > 0 && std::rand() % 2500 == 2499) {
		_enemies.push_back(std::make_unique<Centipede::QuickEnemy>());
		_enemiesLeft--;
	}
	if (_shot.second >= 0)
		_shot.second--;
	_enemies.reserve(_enemies.size() * 2);
	for (auto &&i = _enemies.begin(); i < _enemies.end(); i++) {
		_score += i->get()->move(_enemies, _obstacles, _shot);
		auto body = i->get()->getBody();
		if (i->get()->toDelete()) {
			_enemies.erase(i);
			i--;
		} else if (std::find(body.begin(), body.end(), _player) != body.end()) {
			_enemiesLeft = 0;
			_enemies.clear();
			return std::pair<bool, int>(false, _score);
		}
	}
	for (auto &&i = _obstacles.begin(); i < _obstacles.end(); i++) {
		if (i->pos == _shot) {
			_shot.second = -1;
			i->hp--;
			if (i->hp == 0) {
				_obstacles.erase(i);
				_score++;
			}
			break;
		}
	}
	return std::pair<bool, int>(true, _score);
}

void Centipede::Game::display(IGraphical &display)
{
	Arcade::Rect rect;

	rect.defaultColor = Arcade::Color::BLACK;
	rect.pos = {0, 0};
	rect.size = {40, 39};
	rect.isOutlined = true;
	rect.termSymbol = ' ';
	display.drawRect(rect);
	rect.isOutlined = false;
	rect.size = {1, 1};
	rect.spriteName = "centipede/CentipedeObstacle1.png";
	for (auto &&i: _obstacles) {
		rect.pos = {i.pos.first, i.pos.second};
		rect.termSymbol = 'O' - i.hp;
		rect.spriteName[27] = ('0' + i.hp);
		rect.defaultColor = Arcade::Color(8 - i.hp);
		display.drawRect(rect);
	}
	rect.defaultColor = Arcade::Color::RED;
	rect.termSymbol = 'C';
	rect.spriteName = "";
	for (auto &&i: _enemies) {
		bool first = true;
		for (auto &&j: i->getBody()) {
			if (first) {
				rect.spriteName = "centipede/CentipedeHead0.png";
				rect.spriteName[23] = '1' - i->getDir();
				first = false;
			} else
				rect.spriteName = "centipede/CentipedeBody.png";
			rect.pos = {j.first, j.second};
			display.drawRect(rect);
		}
	}
	rect.spriteName = "centipede/CentipedeShot.png";
	if (_shot.second >= 0) {
		rect.defaultColor = Arcade::Color::YELLOW;
		rect.termSymbol = 'X';
		rect.pos = {_shot.first, _shot.second};
		display.drawRect(rect);
	}
	rect.defaultColor = Arcade::Color::GREEN;
	rect.spriteName = "centipede/CentipedePlayer.png";
	rect.termSymbol = 'P';
	rect.pos = {_player.first, _player.second};
	display.drawRect(rect);
	if (_score < 0)
		_score = 0;
	display.drawPair({{"Score", _score}, {2, 0}, Arcade::Color::WHITE, false});
}

bool Centipede::Game::checkPlayerColision(std::pair<int, int> offset)
{
	auto player = _player;
	player.first += offset.first;
	player.second += offset.second;
	for (auto &&i: _obstacles) {
		if (i.pos == player)
			return (false);
	}
	return (true);
}