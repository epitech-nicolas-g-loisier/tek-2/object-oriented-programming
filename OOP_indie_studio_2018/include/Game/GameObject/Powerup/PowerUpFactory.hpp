/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** PowerUp factory class
*/
#include <memory>
#include <vector>
#include <random>

#ifndef POWERUPFACTORY_
#define POWERUPFACTORY_

#include "PowerUp.hpp"
#include "GraphicalEngine.hpp"

namespace indie {
	class PowerUpFactory {
	public:
		PowerUpFactory();

	std::shared_ptr<indie::PowerUp> spawnPowerUp(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);

private:
	std::shared_ptr<indie::PowerUp> spawnFireUp(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnBombUp(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnSpeedUp(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnFireMax(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnFireDown(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnBombDown(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnSpeedDown(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnLineBomb(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnKick(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnPunch(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnHeart(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnSkull(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnDangerous(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;
	std::shared_ptr<indie::PowerUp> spawnPierce(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const;

	std::vector<std::shared_ptr<indie::PowerUp>(indie::PowerUpFactory::*)(indie::GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const> ctors;
	std::mt19937 gen;
};
}

#endif // POWERUPFACTORY_