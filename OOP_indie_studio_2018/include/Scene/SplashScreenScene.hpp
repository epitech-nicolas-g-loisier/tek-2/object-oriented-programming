/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** SplashScreenScene
*/

#ifndef SPLASHSCREENSCENE_HPP
#define SPLASHSCREENSCENE_HPP

#include <vector>
#include "GraphicalEngine.hpp"
#include "Button.hpp"
#include "IScene.hpp"

namespace indie
{
class SplashScreenScene : public IScene
{
public:
	explicit SplashScreenScene(indie::GraphicalEngine &engine);
	~SplashScreenScene() override = default;
	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	std::vector<std::string> splash;
	int choice = -1;
	int color = 255;
	int diff = 1;
};
}

#endif //SPLASHSCREENSCENE_HPP
