/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** BlockNode
*/

#ifndef BLOCKNODE_HPP
#define BLOCKNODE_HPP

#include <irrlicht.h>
#include <array>
#include "GraphicalEngine.hpp"
#include "INode.hpp"

namespace indie
{
class BlockNode : public indie::INode
{
public:
	BlockNode(bool, GraphicalEngine &);
	~BlockNode() override;
	std::pair<float, float> getPosition() override;
	void setPosition(const std::pair<float, float> &) override;
	float getRotation() override;
	void setRotation(float) override;
	void swapTexture(char) override;

private:
	void setTexture() override;

	irr::scene::IMeshSceneNode *node ;
	std::array<irr::video::ITexture *, 2> texture{};
	bool _destroyable;
};
}

#endif //BLOCKNODE_HPP
