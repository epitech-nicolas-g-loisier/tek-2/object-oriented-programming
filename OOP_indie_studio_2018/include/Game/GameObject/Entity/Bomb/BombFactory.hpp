/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** BombFactory
*/
#include <memory>
#include <map>

#ifndef INDIE_STUDIO_BOMBFACTORY_HPP
#define INDIE_STUDIO_BOMBFACTORY_HPP

#include "AudioEngine.hpp"

namespace indie {

	enum BombType : char;
	class Bomb;
	class GraphicalEngine;

	class BombFactory {
	public:
		explicit BombFactory(GraphicalEngine &, indie::AudioEngine &);
		std::shared_ptr<indie::Bomb> spawnBomb(indie::BombType, unsigned char fireLevel, std::pair<float, float> pos);

	private:
		std::shared_ptr<indie::Bomb> spawnNormalBomb(unsigned char, std::pair<unsigned char, unsigned char>) const;
		std::shared_ptr<indie::Bomb> spawnDangerousBomb(unsigned char, std::pair<unsigned char, unsigned char>) const;
		std::shared_ptr<indie::Bomb> spawnPierceBomb(unsigned char, std::pair<unsigned char, unsigned char>) const;

		std::map<indie::BombType, std::shared_ptr<indie::Bomb>(indie::BombFactory::*)(unsigned char, std::pair<unsigned char, unsigned char>) const> ctors;
		GraphicalEngine &graphicalEngine;
		indie::AudioEngine &audioEngine;
	};
}

#include "Bomb.hpp"
#include "GraphicalEngine.hpp"

#endif //INDIE_STUDIO_BOMBFACTORY_HPP
