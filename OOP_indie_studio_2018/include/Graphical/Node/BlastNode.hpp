/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** BlastMesh
*/

#ifndef BLASTMESH_HPP
#define BLASTMESH_HPP

#include <irrlicht.h>
#include <utility>
#include <array>
#include "INode.hpp"
#include "GraphicalEngine.hpp"

namespace indie
{
class BlastNode : public indie::INode
{
public:
	explicit BlastNode(GraphicalEngine &, bool);
	~BlastNode() override;
	std::pair<float, float> getPosition() override;
	void setPosition(const std::pair<float, float> &) override;
	float getRotation() override;
	void setRotation(float) override;
	void swapTexture(char) override;
	void update();

private:
	void setTexture() override;

	irr::scene::IMesh *mesh;
	irr::scene::IMeshSceneNode *node;
	irr::video::ITexture *texture;
	float diff = 0.05;

};
}

#endif //BLASTMESH_HPP
