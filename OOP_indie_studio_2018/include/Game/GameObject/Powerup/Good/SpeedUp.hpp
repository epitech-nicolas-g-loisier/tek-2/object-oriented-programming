/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** SpeedUp
*/

#ifndef SPEEDUP_HPP_
#define SPEEDUP_HPP_

#include "PowerUp.hpp"

namespace indie {
	class SpeedUp : virtual public PowerUp {
	public:
		SpeedUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif // SPEEDUP_HPP_