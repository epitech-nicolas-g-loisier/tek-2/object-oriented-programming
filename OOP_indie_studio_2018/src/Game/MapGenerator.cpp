/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** MapGenerator
*/

#include <ctime>
#include "MapGenerator.hpp"

MapGenerator::MapGenerator() : _width(17), _height(11)
{
	_map = new char *[_height];
	char blockChar = '*';

	for (std::size_t row = 0; row < _height; row += 1) {
		_map[row] = new char[_width + 1];
		blockChar = (row > 0 && row < _height - 1) ? ' ' : '*';
		_map[row][0] = '*';
		for (std::size_t column = 1; column < _width - 1; column += 1) {
			_map[row][column] = blockChar;
		}
		_map[row][_width - 1] = '*';
		_map[row][_width] = '\0';
	}
	std::srand(time(NULL));
}

MapGenerator::~MapGenerator()
{
	for (std::size_t index = 0; index < _height; index += 1) {
		delete [] _map[index];
	}
	delete [] _map;
}

void MapGenerator::placeHardBlocks()
{
	std::size_t column = 1;

	for (std::size_t row = 2; row < _height - 2; column += 1) {
		if (!isMapCorner(row, column) && _map[row][column] == ' ' &&
		(rand() % 100) <= 10) {
			placeLine(row, column);
		}
		if (column > _width - 4) {
			row += 1;
			column = 1;
		}
	}
	fillEmptySpace();
}

void MapGenerator::placeLine(std::size_t row, std::size_t column)
{
	std::size_t length = rand() % 4 + 1;
	std::size_t direction = rand() % 4;
	int rowIndex = 0;
	int columnIndex = 0;

	if (direction < 2) {
		rowIndex = !direction ? 1 : - 1;
	} else {
		columnIndex = (direction == 2) ? 1 : -1;
	}
	for (std::size_t count = 0; count < length; count += 1) {
		if (isMapBorder(row, column) || isMapCorner(row, column)) {
			return;
		}
		_map[row][column] = '#';
		row += rowIndex;
		column += columnIndex;
	}
}

void MapGenerator::fillEmptySpace()
{
	std::size_t size = 0;
	std::pair<int, int> squarePosition;
	std::size_t column = 2;
	std::size_t row = 2;

	while (column < _width - 2 && row < _height - 2) {
		while (findSquareSize(size + 1, column, row)) {
			size += 1;
			squarePosition = {column, row};
		}
		column += 1;
		if (column > _width - 3) {
			row += 1;
			column = 2;
		}
	}
	if (size < 4) {
		return;
	}
	column = squarePosition.first;
	row = squarePosition.second;
	while (row < squarePosition.second + size) {
		_map[row][column] = '#';
		column += 2;
		if (column >= squarePosition.first + size) {
			column = squarePosition.first;
			row += 2;
		}
	}
}

bool MapGenerator::findSquareSize(std::size_t size, std::size_t column, std::size_t row) const
{
	std::size_t columnIndex = 0;
	std::size_t rowIndex = 0;

	if (column + size > _width - 2 || row + size > _height - 2) {
		return false;
	}
	while (rowIndex < size) {
		if (_map[row + rowIndex][column + columnIndex] != ' ') {
			return false;
		}
		columnIndex += 1;
		if (columnIndex == size) {
			rowIndex += 1;
			columnIndex = 0;
		}
	}
	return true;
}

void MapGenerator::placeHardBlocksPattern()
{
       int space = 0;

       space = std::rand() % 2 + 1;
       for (std::size_t row = 2; row < _height - 1; row += 1 + space) {
               for (std::size_t column = 2; column < _width - 1; column += 1 + space) {
                       _map[row][column] = '#';
               }
       }
}

void MapGenerator::placeSoftBlocks()
{
	std::size_t column = 1;

	for (std::size_t row = 1; row < _height - 1; column += 1) {
		if (!isMapCorner(row, column) && _map[row][column] == ' ' &&
		(rand() % 100) < 75) {
			_map[row][column] = 'x';
		}
		if (column > _width - 2) {
			row += 1;
			column = 0;
		}
	}
}

void MapGenerator::printMap() const
{
	for (std::size_t index = 0; index < _height; index += 1) {

	}
}

bool MapGenerator::isMapBorder(std::size_t row, std::size_t column) const
{
	return (column < 2 || column > _width - 3 || row < 2 || row > _height - 3);
}

bool MapGenerator::isMapCorner(std::size_t row, std::size_t column) const
{
	return (isMapBorder(row, column) && !(column > 2 && column < _width - 3)
	&& !(row > 2 && row < _height - 3));
}

char * const* MapGenerator::getMap() const
{
	return _map;
}

size_t MapGenerator::getHeight() const
{
	return _height;
}

size_t MapGenerator::getWidth() const
{
	return _width;
}