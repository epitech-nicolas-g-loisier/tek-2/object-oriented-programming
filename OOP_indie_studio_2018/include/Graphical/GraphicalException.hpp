/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** GraphicalException
*/

#ifndef GRAPHICALEXCEPTION_HPP
#define GRAPHICALEXCEPTION_HPP

#include <string>
#include <exception>

namespace indie
{
class GraphicalException : public std::exception
{
public:
	explicit GraphicalException(const std::string& element, const std::string& className);
	const char *what() const noexcept override;

protected:
	std::string _message;
};

class TextureLoadingError : public GraphicalException
{
public:
	explicit TextureLoadingError(const std::string& className, const std::string &texture);
};

class ModelLoadingError : public GraphicalException
{
public:
	explicit ModelLoadingError(const std::string& className);
};

class FontLoadingError : public GraphicalException
{
public:
	explicit FontLoadingError(const std::string& className, const std::string& fontType);
};

class SoundLoadingError : public GraphicalException
{
public:
	explicit SoundLoadingError(const std::string& className);
};

class NodeCreationError : public GraphicalException
{
public:
	explicit NodeCreationError(const std::string& className);
};

}
#endif //GRAPHICALEXCEPTION_HPP
