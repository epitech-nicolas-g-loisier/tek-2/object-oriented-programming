/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** GraphicalEngine method
*/

#include <iostream>
#include "AssetPath.hpp"
#include "GraphicalException.hpp"
#include "GraphicalEngine.hpp"

indie::GraphicalEngine::GraphicalEngine()
try {

	device = createDevice(irr::video::EDT_OPENGL, irr::core::dimension2d<irr::u32>(1920,1080), 32, true, false, false, &receiver);
	if (!device)
		throw GraphicalException("Device", "GraphicalEngine");

	driver = device->getVideoDriver();
	if (!driver)
		throw GraphicalException("Driver", "GraphicalEngine");

	scenemgr = device->getSceneManager();
	if (!scenemgr)
		throw GraphicalException("Scene Manager", "GraphicalEngine");

	cam = scenemgr->addCameraSceneNode();
	if (!cam)
		throw GraphicalException("Camera", "GraphicalEngine");

	font76 = device->getGUIEnvironment()->getFont(findFontPath("Font_76px/myfont.xml").data());
	if (!font76)
		throw FontLoadingError("GraphicalEngine", "Mario 76px");

	font24 = device->getGUIEnvironment()->getFont(findFontPath("Font_24px/myfont.xml").data());
	if (!font24)
		throw FontLoadingError("GraphicalEngine", "Mario 24px");

	font48 = device->getGUIEnvironment()->getFont(findFontPath("Font_48px/myfont.xml").data());
	if (!font48)
		throw FontLoadingError("GraphicalEngine", "Mario 48px");

	device->setWindowCaption(L"Bomberman");
	cursor[0] = driver->getTexture(findTexturePath("Cursor/Cursor.png").data());
	cursor[1] = driver->getTexture(findTexturePath("Cursor/CursorClicked.png").data());
	if (!cursor[0] || !cursor[1])
		throw TextureLoadingError("GraphicalEngine", "Cursor");
	device->getCursorControl()->setVisible(false);
} catch (const indie::GraphicalException &graphicalException) {
	std::cerr << "Graphical exception: " << graphicalException.what() << std::endl;
	std::exit(84);
}

indie::GraphicalEngine::~GraphicalEngine()
{
	cam->remove();
	scenemgr->clear();
	driver->removeAllTextures();
	driver->removeAllHardwareBuffers();
	driver->removeAllOcclusionQueries();
	device->drop();
}

void indie::GraphicalEngine::setCam(const irr::core::vector3df& targetPos, const irr::core::vector3df& pos)
{
	cam->setTarget(targetPos);
	cam->bindTargetAndRotation(true);
	cam->setPosition(pos);
}

void indie::GraphicalEngine::initGame()
{
	irr::scene::IMesh *basekMesh = scenemgr->getMesh(findModelPath("Base/base.3ds").data());
	base = scenemgr->addMeshSceneNode(basekMesh);
	base->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	base->setMaterialTexture(0, driver->getTexture(findModelPath("Base/grass.png").data()));
	base->setScale({17, 1, 11});
	base->setPosition({15, -2, 9});

	setCam(base->getPosition(), {15, 18, 0});
}

void indie::GraphicalEngine::endGame()
{
	base->setVisible(false);
	base->remove();
}

void indie::GraphicalEngine::drawCursor()
{
	auto pos = device->getCursorControl()->getPosition();
	pos.X -= 20;
	int idx = 0;

	if (receiver.leftMouseDown())
		idx++;
	driver->draw2DImage(cursor[idx], pos, {0, 0, 64, 64}, nullptr, irr::video::SColor(255, 255, 255, 255), true);
}

const std::string indie::GraphicalEngine::findModelPath(const std::string& filepath) {
	return ModelPath + "/" + filepath;
}

const std::string indie::GraphicalEngine::findTexturePath(const std::string &filepath)
{
	return TexturePath + "/" + filepath;
}

const std::string indie::GraphicalEngine::findFontPath(const std::string &filepath)
{
	return FontPath + "/" + filepath;
}

void indie::GraphicalEngine::drawBackground(const std::string &file)
{
	irr::video::ITexture *background = driver->getTexture(findTexturePath(file).data());

	if (!background)
		std::cerr << "Cannot find texture: " << file << std::endl;
	else
		driver->draw2DImage(background, {0, 0});
}

void indie::GraphicalEngine::drawTexture(const std::string &file, std::pair<int, int> pos)
{
	irr::video::ITexture *texture = driver->getTexture(findTexturePath(file).data());

	if (!texture)
		std::cerr << "Cannot find texture: " << file << std::endl;
	else
		driver->draw2DImage(texture, {pos.first, pos.second},
				    {0, 0, (int)texture->getSize().Width, (int)texture->getSize().Height},
				    nullptr, irr::video::SColor(255, 255, 255, 255), true);
}

void indie::GraphicalEngine::drawSprite(const std::string &file, std::pair<int, int> pos, rect_s spritePos)
{
	irr::video::ITexture *texture = driver->getTexture(findTexturePath(file).data());

	if (!texture)
		std::cerr << "Cannot find texture: " << file << std::endl;
	else
		driver->draw2DImage(texture, {pos.first, pos.second},
				    {spritePos.x, spritePos.y, spritePos.xMax, spritePos.yMax},
				    nullptr, irr::video::SColor(255, 255, 255, 255), true);
}

void indie::GraphicalEngine::drawText(const std::string &text, const std::string &size, indie::color_t color, rect_s textPos)
{
	if (size == "small") {
		font24->draw(text.data(), {textPos.x, textPos.y, textPos.xMax, textPos.yMax},
			     irr::video::SColor(color.alpha, color.red, color.green, color.blue),  true, true);
	} else if (size == "medium") {
		font48->draw(text.data(), {textPos.x, textPos.y, textPos.xMax, textPos.yMax},
			     irr::video::SColor(color.alpha, color.red, color.green, color.blue),  true, true);
	} else if (size == "large") {
		font76->draw(text.data(), {textPos.x, textPos.y, textPos.xMax, textPos.yMax},
			     irr::video::SColor(color.alpha, color.red, color.green, color.blue),  true, true);
	}
}

void indie::GraphicalEngine::drawRectangle(rect_s rect, indie::color_t color)
{
	driver->draw2DRectangle(irr::video::SColor(color.alpha, color.red, color.green, color.blue),
				{rect.x, rect.y, rect.xMax, rect.yMax});
}

void indie::GraphicalEngine::beginScene(indie::color_t BackgroundColor)
{
	driver->beginScene(true, true, irr::video::SColor(BackgroundColor.alpha, BackgroundColor.red,
							  BackgroundColor.green, BackgroundColor.blue));
}

void indie::GraphicalEngine::endScene()
{
	driver->endScene();
}
