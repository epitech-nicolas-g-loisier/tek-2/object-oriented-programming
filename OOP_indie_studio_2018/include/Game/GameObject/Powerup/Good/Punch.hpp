/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Punch
*/

#ifndef BOMBERMAN_PUNCH_HPP
#define BOMBERMAN_PUNCH_HPP

#include "PowerUp.hpp"

namespace indie {
	class Punch : virtual public PowerUp {
	public:
		Punch(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		bool interact(indie::Player *) override;
	};
}

#endif //BOMBERMAN_PUNCH_HPP
