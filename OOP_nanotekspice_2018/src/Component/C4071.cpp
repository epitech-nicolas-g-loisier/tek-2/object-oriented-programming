/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component 4071 Method
*/

#include "C4071.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>
#include <array>
#include <algorithm>

nts::C4071::C4071(const std::string name)
	: _name(name)
{
	_output.emplace(3, new OutputPin(1, 2));
	_output.emplace(4, new OutputPin(5, 6));
	_output.emplace(10, new OutputPin(8, 9));
	_output.emplace(11, new OutputPin(12, 13));
}

nts::C4071::~C4071()
{
	for(auto o : _output)
	{
		delete o.second;
	}
	for(auto o : _input)
	{
		delete o.second;
	}

}

nts::Tristate nts::C4071::compute(std::size_t pin)
{
	if (_input.count(pin) != 0)
		return _input[pin]->prev.compute(_input[pin]->pinNumber);
	else if (_output.count(pin) != 0) {
		Tristate val1 = compute(_output[pin]->pinNumber1);
		Tristate val2 = compute(_output[pin]->pinNumber2);
		if (val1 == TRUE || val2 == TRUE)
			return TRUE;
		else if (val1 == UNDEFINED || val2 == UNDEFINED)
			return UNDEFINED;
		else
			return FALSE;
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4071::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	std::array<size_t, 8> input = {1, 2, 5, 6, 8, 9, 12, 13};
	std::array<size_t, 4> output = {3, 4, 10, 11};

	if (std::find(input.begin(), input.end(), pin) != input.end()) {
		if (_input.count(pin) == 0) {
			_input.emplace(pin, new InputPin(other, otherPin));
		} else {
			throw NanoException("Pin already linked.");
		}
	} else if (std::find(output.begin(), output.end(), pin) != output.end()) {
		other.setLink(otherPin, *this, pin);
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4071::dump() const
{
	std::cout << _name << std::endl;
}

const std::string &nts::C4071::getName() const
{
	return (_name);
}