/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Sound
*/

#ifndef SOUND_HPP
#define SOUND_HPP

#include <SFML/Audio.hpp>

namespace indie
{
class Sound
{
public:
	Sound(const std::string &file);
	~Sound() = default;

	void play();
private:
	sf::SoundBuffer buffer;
	sf::Sound sound;

};
}

#endif //SOUND_HPP
