/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** BombType
*/

#ifndef INDIE_STUDIO_BOMBTYPE_HPP
#define INDIE_STUDIO_BOMBTYPE_HPP

namespace indie {
	enum BombType : char {
		NORMAL = 0,
		PIERCE = 1,
		DANGEROUS = 2
	};
}

#endif //INDIE_STUDIO_BOMBTYPE_HPP
