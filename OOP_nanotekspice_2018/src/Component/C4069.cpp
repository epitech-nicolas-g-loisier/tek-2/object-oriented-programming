/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component 4069 Method
*/

#include "C4069.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>
#include <array>
#include <algorithm>

nts::C4069::C4069(const std::string name)
	: _name(name)
{
	_output.emplace(2, new OutputPin(1, 0));
	_output.emplace(4, new OutputPin(3, 0));
	_output.emplace(6, new OutputPin(5, 0));
	_output.emplace(8, new OutputPin(9, 0));
	_output.emplace(10, new OutputPin(11, 0));
	_output.emplace(12, new OutputPin(13, 0));
}

nts::C4069::~C4069()
{
	for(auto o : _output)
	{
		delete o.second;
	}
	for(auto o : _input)
	{
		delete o.second;
	}

}

nts::Tristate nts::C4069::compute(std::size_t pin)
{
	if (_input.count(pin) != 0)
		return _input[pin]->prev.compute(_input[pin]->pinNumber);
	else if (_output.count(pin) != 0) {
		Tristate val1 = compute(_output[pin]->pinNumber1);
		if (val1 == UNDEFINED)
			return UNDEFINED;
		if (val1 == TRUE)
			return FALSE;
		else
			return TRUE;
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4069::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	std::array<size_t, 6> input = {1, 3, 5, 9, 11, 13};
	std::array<size_t, 6> output = {2, 4, 6, 8, 10, 12};

	if (std::find(input.begin(), input.end(), pin) != input.end()) {
		if (_input.count(pin) == 0) {
			_input.emplace(pin, new InputPin(other, otherPin));
		} else {
			throw NanoException("Pin already linked.");
		}
	} else if (std::find(output.begin(), output.end(), pin) != output.end()) {
		other.setLink(otherPin, *this, pin);
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4069::dump() const
{
	std::cout << _name << std::endl;
}

const std::string &nts::C4069::getName() const
{
	return (_name);
}