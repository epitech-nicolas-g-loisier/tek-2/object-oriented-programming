/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** PowerUpFactory implementation
*/
#include <chrono>
#include "PowerUpFactory.hpp"
#include "FireUp.hpp"
#include "BombUp.hpp"
#include "SpeedUp.hpp"
#include "LineBomb.hpp"
#include "Kick.hpp"
#include "FireMax.hpp"
#include "Punch.hpp"
#include "FireDown.hpp"
#include "BombDown.hpp"
#include "SpeedDown.hpp"
#include "Heart.hpp"
#include "Skull.hpp"
#include "PierceBombPowerup.hpp"
#include "DangerousBombPowerup.hpp"

indie::PowerUpFactory::PowerUpFactory()
	: gen(std::random_device()())
{
	// 21.5%
	ctors.push_back(&indie::PowerUpFactory::spawnFireUp);
	ctors.push_back(&indie::PowerUpFactory::spawnBombUp);
	ctors.push_back(&indie::PowerUpFactory::spawnSpeedUp);
	// 6%
	ctors.push_back(&indie::PowerUpFactory::spawnFireDown);
	ctors.push_back(&indie::PowerUpFactory::spawnBombDown);
	ctors.push_back(&indie::PowerUpFactory::spawnSpeedDown);
	ctors.push_back(&indie::PowerUpFactory::spawnSkull);
	// 2%
	ctors.push_back(&indie::PowerUpFactory::spawnPierce);
	ctors.push_back(&indie::PowerUpFactory::spawnDangerous);
	// 1.5%
	ctors.push_back(&indie::PowerUpFactory::spawnLineBomb);
	ctors.push_back(&indie::PowerUpFactory::spawnKick);
	ctors.push_back(&indie::PowerUpFactory::spawnPunch);
	ctors.push_back(&indie::PowerUpFactory::spawnFireMax);
	ctors.push_back(&indie::PowerUpFactory::spawnHeart);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnPowerUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
{
	int randV = std::uniform_int_distribution<int>(0, 999)(gen);
	int index = 0;
	if (randV < 645)
		index = randV / 215;
	else if (randV < 885)
		index = (randV - 645) / 60 + 3;
	else if (randV < 925)
		index = (randV - 885) / 20 + 7;
	else
		index = (randV - 925) / 15 + 9;
	auto powerup = (this->*ctors[index])(engine, pos, _audioEngine);
	powerup->holdSmartPointer(powerup);
	return (powerup);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnFireUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::FireUp>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnBombUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::BombUp>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnSpeedUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::SpeedUp>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnFireMax(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::FireMax>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnFireDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::FireDown>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnBombDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::BombDown>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnSpeedDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::SpeedDown>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnLineBomb(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::LineBomb>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnKick(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::Kick>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnPunch(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::Punch>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnSkull(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::Skull>(engine, pos, _audioEngine);
}

std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnHeart(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::Heart>(engine, pos, _audioEngine);
}
std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnPierce(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::PierceBombPowerup>(engine, pos, _audioEngine);
}
std::shared_ptr<indie::PowerUp> indie::PowerUpFactory::spawnDangerous(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine) const {
	return std::make_shared<indie::DangerousBombPowerup>(engine, pos, _audioEngine);
}