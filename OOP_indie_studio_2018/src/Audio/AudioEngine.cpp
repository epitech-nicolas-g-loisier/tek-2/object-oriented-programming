/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** AudioEngine
*/

#include <Audio/AudioEngine.hpp>
#include "AudioEngine.hpp"

indie::AudioEngine::AudioEngine()
	: bombSound("effect/Bomb.ogg"), breakBlockSound("effect/BreakBlock.ogg"), powerUpSound("effect/PowerUp.ogg"),
	powerDownSound("effect/PowerDown.ogg"), lifeUpSound("effect/LifeUp.ogg")
{
}

void indie::AudioEngine::playBombSound()
{
	bombSound.play();
}

void indie::AudioEngine::playBreakBlockSound()
{
	breakBlockSound.play();
}

void indie::AudioEngine::playPowerUpSound()
{
	powerUpSound.play();
}

void indie::AudioEngine::playPowerDownSound()
{
	powerDownSound.play();
}

void indie::AudioEngine::playLifeUpSound()
{
	lifeUpSound.play();
}
