/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Assets
*/
#include <utility>
#include <string>

#ifndef ARCADE_ASSET_
	#define ARCADE_ASSET_

namespace Arcade
{

	enum Color {
		BLACK = 0,
		RED = 1,
		GREEN = 2,
		YELLOW = 3,
		BLUE = 4,
		MAGENTA = 5,
		CYAN = 6,
		WHITE = 7,
	};

	struct Rect
	{
		std::pair<float, float> pos;
		std::pair<float, float> size;
		std::string spriteName;
		Arcade::Color defaultColor;
		char termSymbol;
		bool isOutlined;
	};

	struct Line
	{
		enum Direction
		{
			UP = 0,
			DOWN,
			LEFT,
			RIGHT
		};

		std::pair<float, float> pos;
		float length;
		Arcade::Line::Direction dir;
		Arcade::Color color;
	};

	struct Pair
	{
		std::pair<std::string, float> value;
		std::pair<float, float> pos;
		Arcade::Color color;
		bool background;
	};
}

#endif // ARCADE_ASSET_