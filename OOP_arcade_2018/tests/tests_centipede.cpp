/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Tests for the game Centipede
*/
#include <criterion/criterion.h>
#include <unistd.h>
#include "Centipede.hpp"
#include "CriterionWindow.hpp"

Test(Centipede, InputComputing)
{
	Centipede::Game game;
	std::vector<std::string> input;
	input.push_back("Z");
	input.push_back("Q");
	cr_assert(game.computeInput(input).first, "Game ended early, UP/LEFT");
	sleep(1);
	for (int i = 0; i < 80; i++)
		cr_assert(game.computeInput(input).first, "Game ended early, UP/LEFT");
	input.clear();
	input.push_back("S");
	input.push_back("D");
	cr_assert(game.computeInput(input).first, "Game ended early, DOWN/RIGHT");
	sleep(1);
	for (int i = 0; i < 80; i++)
		cr_assert(game.computeInput(input).first, "Game ended early, DOWN/RIGHT");
	sleep(1);
	input.clear();
	input.push_back("A");
	cr_assert(game.computeInput(input).first, "Game ended early, Shooting");
	CriterionWindow win;
	game.display(win);
}