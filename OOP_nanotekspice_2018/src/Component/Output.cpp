/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Output Method
*/
#include "Output.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>
#include <unordered_map>

nts::Output::Output(const std::string name)
	: _name(name), _value(UNDEFINED)
{
}

nts::Output::~Output()
{
	delete _pin;
}

nts::Tristate nts::Output::compute(std::size_t pin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	if (_pin == nullptr)
		throw NanoException(std::string("Input '' isn't linked to anything").insert(7, _name));
	_value = _pin->prev.compute(_pin->pinNumber);
	return _value;
}

void nts::Output::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	else if (_pin != nullptr)
		throw NanoException("Input already linked.");
	else {
		_pin = new InputPin(other, otherPin);
	}
}

void nts::Output::dump() const
{
	std::cout << _name << "=" << _value << std::endl;
}

const std::string &nts::Output::getName() const
{
	return (_name);
}

const std::string nts::Output::getValue() const
{
	std::unordered_map<nts::Tristate, std::string> values;

	values[nts::UNDEFINED] = "U";
	values[nts::FALSE] = "0";
	values[nts::TRUE] = "1";
	return (values[_value]);
}