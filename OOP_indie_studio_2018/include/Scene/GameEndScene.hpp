/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameEndScene
*/

#ifndef GAMEENDSCENE_HPP
#define GAMEENDSCENE_HPP

#include "IScene.hpp"
#include "GraphicalEngine.hpp"
#include "GameOption.hpp"
#include "Sound.hpp"

namespace indie {
class GameEndScene : public IScene
{
public:
	explicit GameEndScene(indie::GraphicalEngine &engine, indie::GameOption &option);
	~GameEndScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	indie::GameOption &gameOption;
	indie::Sound sound;

	void drawVictory();
	void drawDefeat();
	void drawEquality();

	std::unique_ptr<indie::IPlayerNode> winner;
};
}

#endif //GAMEENDSCENE_HPP
