/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** C4081
*/

#ifndef C4081_HPP_
	#define C4081_HPP_

#include "IComponent.hpp"
#include "Pin.hpp"
#include <map>

namespace nts {

	class C4081 : public IComponent
	{
	public:
		C4081(const std::string name);
		~C4081();
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;

	private:
		std::string _name;
		std::map<size_t, InputPin *> _input;
		std::map<size_t, OutputPin *> _output;
	};
}

#endif /* !C4081_HPP_ */