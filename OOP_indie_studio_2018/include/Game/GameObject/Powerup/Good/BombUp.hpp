/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** BombUp powerup class
*/

#ifndef BOMBUP_HPP_
#define BOMBUP_HPP_

#include "PowerUp.hpp"

namespace indie {
class BombUp : virtual public PowerUp {
public:
	BombUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
	virtual bool interact(indie::Player *);
};
}

#endif // BOMBUP_HPP_