/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** DangerousBomb
*/

#ifndef INDIE_STUDIO_DANGEROUSBOMB_HPP
#define INDIE_STUDIO_DANGEROUSBOMB_HPP

#include "Bomb.hpp"
#include "GraphicalEngine.hpp"

namespace indie {

	class DangerousBomb : public Bomb {
	public:
		DangerousBomb(unsigned char, std::pair<unsigned char, unsigned char>&, GraphicalEngine &, indie::AudioEngine &);
		bool update(indie::MapData &) override;

	protected:
		bool addBlastToMap(indie::MapData &, const std::pair<unsigned char, unsigned char>&) const override;
	};
}

#endif //INDIE_STUDIO_DANGEROUSBOMB_HPP
