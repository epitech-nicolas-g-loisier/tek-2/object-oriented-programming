/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PlayerSelectorScene
*/

#include <iostream>
#include <random>
#include "PlayerSelectorScene.hpp"

indie::PlayerSelectorScene::PlayerSelectorScene(indie::GraphicalEngine &engine, indie::GameOption &_gameOption)
	: _engine(engine), keyTime(std::chrono::steady_clock::now()), gameOption(_gameOption)
{

	character.emplace_back("Screen/PersoSelect/CharacterIcon/Random.png");
	character.emplace_back("Screen/PersoSelect/CharacterIcon/Mario.png");
	character.emplace_back("Screen/PersoSelect/CharacterIcon/Luigi.png");
	character.emplace_back("Screen/PersoSelect/CharacterIcon/Bowser.png");
	character.emplace_back("Screen/PersoSelect/CharacterIcon/Peach.png");

	selectedCharacter["random"] = "Screen/PersoSelect/Selected/Random.png";
	selectedCharacter["mario"] = "Screen/PersoSelect/Selected/Mario.png";
	selectedCharacter["luigi"] = "Screen/PersoSelect/Selected/Luigi.png";
	selectedCharacter["bowser"] = "Screen/PersoSelect/Selected/Bowser.png";
	selectedCharacter["peach"] = "Screen/PersoSelect/Selected/Peach.png";

	characterName = {"random", "mario", "luigi", "bowser", "peach"};
	nbrCharacter = character.size() - 1;
}

indie::IScene::sceneState indie::PlayerSelectorScene::draw()
{
	_engine.beginScene({120, 102, 136, 255});

	_engine.drawBackground("Screen/PersoSelect/Background.png");
	_engine.drawTexture("Screen/PersoSelect/PersoSelected.png", {160, 100});
	_engine.drawTexture("Screen/PersoSelect/PersoChoice.png", {160, 600});
	drawSelected();

	std::pair<int, int> pos = {223, 625};

	getInput();

	for (int y = 0; y < 3; ++y) {
		pos.first = 223;
		for (int x = 0; x < 12; ++x) {
			if (y * 12 + x < nbrCharacter)
				_engine.drawTexture(character[y * 12 + x + 1], {pos.first, pos.second});
			else
				_engine.drawTexture(character[0], {pos.first, pos.second});
			for (int i = 0; i < gameOption.player; ++i) {
				if (cursorPos[i].first == x && cursorPos[i].second == y) {
					_engine.drawRectangle({pos.first - 10, pos.second - 10,
							       pos.first + 110, pos.second + 110}, color[i]);
				}
			}
			pos.first += 125;
		}
		pos.second += 125;
	}

	if (gameOption.player < 4)
		_engine.drawText("Press SPACE to Add a player", "medium", {255, 255, 255, 255}, {0, 0, 1920, 80});
	if (gameOption.player > 1)
		_engine.drawText("Press RETURN to Remove a player", "medium", {255, 255, 255, 255}, {0, 1000, 1920, 1080});
	_engine.drawText("Press Bomb Key to select/unselect", "medium", {200, 200, 200, 255}, {0, 20, 1920, 1080});
	_engine.endScene();

	if (_engine.receiver.keyPressed("escape")) {
		return indie::IScene::BEGIN;
	}
	for (int i = 0; i < gameOption.player; ++i) {
		if (!lock[i])
			return indie::IScene::PLAYERSELECTOR;
	}
	for (int i = 0; i < 4; ++i) {
		int tmp = cursorPos[i].second * 12 + cursorPos[i].first + 1;
		if (lock[i])
			gameOption.character[i] = tmp <= nbrCharacter ? characterName[tmp] : findRandomCharacter();
		else
			gameOption.character[i] = findRandomCharacter();
	}
	return indie::IScene::MAPSELECTOR;
}

void indie::PlayerSelectorScene::getInput()
{
	if (keyTime + std::chrono::milliseconds(100) <= std::chrono::steady_clock::now()) {
		for (int i = 0; i < gameOption.player; ++i) {
			if (!lock[i]) {
				if (_engine.receiver.keyPressed(gameOption.playerInput[i][0])) {
					if (cursorPos[i].second > 0)
						cursorPos[i].second -= 1;
					else
						cursorPos[i].second = 2;
				}
				if (_engine.receiver.keyPressed(gameOption.playerInput[i][1])) {
					if (cursorPos[i].second < 2)
						cursorPos[i].second += 1;
					else
						cursorPos[i].second = 0;
				}
				if (_engine.receiver.keyPressed(gameOption.playerInput[i][2])) {
					if (cursorPos[i].first > 0)
						cursorPos[i].first -= 1;
					else
						cursorPos[i].first = 11;
				}
				if (_engine.receiver.keyPressed(gameOption.playerInput[i][3])) {
					if (cursorPos[i].first < 11)
						cursorPos[i].first += 1;
					else
						cursorPos[i].first = 0;
				}
			}

			if (_engine.receiver.keyPressed(gameOption.playerInput[i][4])) {
				lock[i] = !lock[i];
				if (lock[i])
					color[i].alpha = 120;
				else
					color[i].alpha = 80;
			}
		}
		if (_engine.receiver.keyPressed("space") && gameOption.player < 4)
			gameOption.player++;
		if (_engine.receiver.keyPressed("return") && gameOption.player > 1) {
			gameOption.player--;
			gameOption.character[gameOption.player] = "random";
		}
		keyTime = std::chrono::steady_clock::now();
	}
}

void indie::PlayerSelectorScene::drawSelected()
{
	for (int j = 0; j < gameOption.player; ++j) {
		int tmp = cursorPos[j].second * 12 + cursorPos[j].first + 1;
		gameOption.character[j] = characterName[tmp <= nbrCharacter ? tmp : 0];
	}

	rect_t textPos = {210, 430, 510, 480};
	std::pair<int, int> pos = {160, 120};
	for (int i = 0; i < 4; ++i) {
		_engine.drawTexture(selectedCharacter[gameOption.character[i]], pos);
		_engine.drawText(gameOption.character[i], "medium", {255, 255, 255, 255}, textPos);
		pos.first += 400;
		textPos.x += 400;
		textPos.xMax += 400;
	}
}

const std::string &indie::PlayerSelectorScene::findRandomCharacter()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	int rand = std::uniform_int_distribution<int>(1, nbrCharacter)(gen);

	for (int i = 0; i < 4; ++i) {
		if (characterName[rand] == gameOption.character[i]) {
			rand++;
			i = -1;
		}
		if (rand > nbrCharacter)
			rand = 1;
	}
	return characterName[rand];
}
