/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** tests for 4069 component
*/

#include <criterion/criterion.h>
#include "C4069.hpp"
#include "Input.hpp"
#include "Output.hpp"
#include "Exception.hpp"

Test(C4069, test_invert_component_true)
{
	nts::C4069 invert("invert");
	nts::Input i1("i1", nts::TRUE);
	nts::Output out("out");

	invert.setLink(1, i1, 1);
	invert.setLink(2, out, 1);

	cr_assert_eq(nts::FALSE, out.compute());
}

Test(C4069, test_invert_component_false)
{
	nts::C4069 invert("invert");
	nts::Input i1("i1", nts::FALSE);
	nts::Output out("out");

	invert.setLink(1, i1, 1);
	invert.setLink(2, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4069, test_invert_component_undefined)
{
	nts::C4069 invert("invert");
	nts::Input i1("i1", nts::UNDEFINED);
	nts::Output out("out");

	invert.setLink(1, i1, 1);
	invert.setLink(2, out, 1);

	cr_assert_eq(nts::UNDEFINED, out.compute());
}

Test(C4069, create_component_bad_pin)
{
	nts::C4069 invert("invert");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		invert.setLink(0, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}

Test(C4069, create_component_relink_pin)
{
	nts::C4069 invert("invert");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		invert.setLink(1, i1, 1);
		invert.setLink(1, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Pin already linked.", error.what()) == 0);
	}
}

Test(C4069, compute_component_bad_pin)
{
	nts::C4069 invert("invert");

	try
	{
		invert.compute(0);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}