/*
** EPITECH PROJECT, 2019
** OOP arcade
** File description:
** Snake Class
*/

#ifndef SNAKE_HPP_
#define SNAKE_HPP_

#include <vector>
#include <utility>
#include <string>

namespace nibbler {

enum Direction {
	NORTH,
	EAST,
	SOUTH,
	WEST,
};

class Snake {
	public:
		Snake();
		~Snake();
		void changeDir(const std::string &input);
		void move();
		void grow(int = 1);
		std::vector<std::pair<float, float>> getBody() const;

	private:
		std::vector<std::pair<float, float>> _body;
		Direction _dir;
};

}
#endif /* !SNAKE_HPP_ */
