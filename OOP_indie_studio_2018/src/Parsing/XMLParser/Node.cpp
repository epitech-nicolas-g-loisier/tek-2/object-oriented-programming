/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Node
*/

#include <array>
#include <algorithm>
#include <iostream>
#include "XMLParser/Node.hpp"

namespace XML
{

Node::Node(std::shared_ptr<std::istream> is)
: is(is), pos{-1, -1, 1}, info{std::string(), SimpleType()}
{
	if (!is)
		throw std::invalid_argument("Cannot access istream");
	this->pos.tag = is->tellg();
	if (is->eof())
		throw std::out_of_range("End of file");
	{
		std::array<char, 1024> buf;
		if (!is->getline(buf.data(), buf.size(), '>') || is->fail())
			throw std::runtime_error("Stream fail, tag doesn not closes");

		this->info.str = std::string(buf.begin(), buf.begin() + is->gcount());
		this->pos.content = is->tellg();

		is->getline(buf.data(), buf.size(), '<');
		if (is->fail())
			throw std::runtime_error("Stream lecture failed");
		if (is->eof()) {
			this->pos.next = -1;
		} else {
			this->pos.next = is->tellg();
		}

		try {
			this->info.content = SimpleType(std::string(buf.begin(), buf.begin() + is->gcount()));
		} catch(std::exception &e) {
		}
	}
}

Node::Node(const Node &coplien)
: is(coplien.is), pos(coplien.pos), info(coplien.info)
{
}

Node::Node()
: is(nullptr), pos{-1, -1, -1}, info{"", SimpleType()}
{}

Node &Node::operator=(const Node &node)
{
	this->is = node.is;
	this->pos = node.pos;
	this->info = node.info;
	return *this;
}

const Node::Info &Node::operator*() const
{
	return this->info;
}

const Node::Info *Node::operator->() const
{
	return &this->info;
}

Node Node::next() const
{
	if (*this == end())
		return Node();
	if (pos.next < 0)
		return Node();
	is->seekg(pos.next);
	return Node(is);
}

Node &Node::operator++()
{
	int layer = Tag::Parse::layer(info.str);

	while (layer > 0 && *this != end()) {
		*this = next();
		layer += Tag::Parse::layer(info.str);
	}
	if (layer < 0) {
		*this = Node();
		return *this;
	}

	*this = next();
	return *this;
}

Node Node::operator++(int amt) const
{
	Node ret(*this);

	for (; amt > 0; --amt) {
		if (ret == end())
			break;
		++ret;
	}
	return ret;
}

Node Node::begin() const
{
	if (*this != Tag::Opening) {
		return end();
	}
	auto n = next();
	if (n == Node::end()) {
		return end();
	}
	if (n == Tag::Closing) {
		return end();
	}
	return n;
}

Node Node::end() const
{
	Node ret;
	return ret;
}

bool Node::operator==(const std::string &str) const
{
	if (Tag::Parse::name(info.str) == str)
		return true;
	else
		return false;
}

bool Node::operator!=(const std::string &str) const
{
	if (Tag::Parse::name(info.str) != str)
		return true;
	else
		return false;
}

bool Node::operator==(const Tag::Type &t) const
{
	if (Tag::Parse::type(info.str) == t)
		return true;
	else
		return false;
}

bool Node::operator!=(const Tag::Type &t) const
{
	if (Tag::Parse::type(info.str) != t)
		return true;
	else
		return false;
}

bool Node::operator==(int layer) const
{
	if (Tag::Parse::layer(info.str) == layer)
		return true;
	else
		return false;
}

bool Node::operator!=(int layer) const
{
	if (Tag::Parse::layer(info.str) != layer)
		return true;
	else
		return false;
}

bool Node::operator==(const Node &node) const
{
	if (is != node.is)
		return false;
	if (!is)
		return true;
	return pos == node.pos;
}

bool Node::operator!=(const Node &node) const
{
	if (is == node.is && pos == node.pos)
		return false;
	else
		return true;
}

Node Node::find(const std::string &str) const
{
	return Node::find(begin(), end(), str);
}

std::vector<Node> Node::findAll(const std::string &str) const
{
	std::vector<Node> ret;

	return findAll(begin(), end(), str);
}

SimpleType Node::get(const std::string &str) const
{
	try {
		Tag t(info.str);

		auto &attr = t->attributes;
		SimpleType found = attr.at(str);
		return found;
	} catch(std::exception &e) {
	}
	return Node::get(begin(), end(), str);
}

std::vector<SimpleType> Node::getAll(const std::string &str) const
{
	try {
		Tag t(info.str);

		auto &attr = t->attributes;
		SimpleType found = attr.at(str);
		return std::vector<SimpleType>{found};
	} catch(std::exception &e) {
	}
	return Node::getAll(begin(), end(), str);
}

Node Node::find(const Node &b, const Node &e, const std::string &str)
{
	for (auto it = b; it != e; ++it) {
		if (Tag::Parse::type(it->str) == Tag::Basic && it == str)
			return it;
	}
	return e;
}

std::vector<Node> Node::findAll(const Node &b, const Node &e, const std::string &str)
{
	std::vector<Node> ret;
	auto it = b;
	while (it != e) {
		it = Node::find(it, e, str);
		if (it != e)
			ret.push_back(it);
		++it;
	}
	return ret;
}

SimpleType Node::get(const Node &b, const Node &e, const std::string &str)
{
	auto found = Node::find(b, e, str);
	if (found == e) {
		return SimpleType();
	}
	if (found == Tag::Opening) {
		return found->content;
	}
	else
		throw std::out_of_range("Could not get attribute");
}

std::vector<SimpleType> Node::getAll(const Node &b, const Node &e, const std::string &str)
{
	std::vector<SimpleType> ret;
	auto found = findAll(b, e, str);
	for (auto f : found)
		ret.push_back(f->content);
	return ret;
}

/* Node::Pos comparators */
	bool Node::Pos::operator==(const Node::Pos &comp) const
	{
		return tag == comp.tag;
	}

	bool Node::Pos::operator!=(const Node::Pos &comp) const
	{
		return tag != comp.tag;
	}

	bool Node::Pos::operator>(const Node::Pos &comp) const
	{
		return tag > comp.tag;
	}

	bool Node::Pos::operator<(const Node::Pos &comp) const
	{
		return tag < comp.tag;
	}

	bool Node::Pos::operator>=(const Node::Pos &comp) const
	{
		return tag >= comp.tag;
	}

	bool Node::Pos::operator<=(const Node::Pos &comp) const
	{
		return tag <= comp.tag;
	}

}
