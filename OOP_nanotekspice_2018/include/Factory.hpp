/*
** EPITECH PROJECT, 2019
** OOP_nanotekspice
** File description:
** Factory
*/
#include "IComponent.hpp"
#include <memory>
#include <unordered_map>

#ifndef FACTORY_NTS_
	#define FACTORY_NTS_

namespace nts {
	class Factory
	{
	public:
		Factory();
		std::unique_ptr<nts::IComponent> createComponent(const std::string &, const std::string &);

	private:
		std::unique_ptr<nts::IComponent> createInput(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> createOutput(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> createTrue(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> createFalse(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> createClock(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4001(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4011(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4013(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4030(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4069(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4071(const std::string &) const noexcept;
		std::unique_ptr<nts::IComponent> create4081(const std::string &) const noexcept;

		std::unordered_map<std::string, std::unique_ptr<nts::IComponent>(nts::Factory::*)(const std::string &) const noexcept> _ctors;
	};
}

#endif // FACTORY_NTS_