/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Centipede engine
*/
#include <utility>

#ifndef CENTIPEDE_
#define CENTIPEDE_

#include "IEngine.hpp"
#include "Enemy.hpp"

namespace Centipede
{
	class Game : public IEngine
	{
	public:
		Game();
		~Game(){};
		std::pair<bool, int> computeInput(const std::vector<std::string> &);
		void display(IGraphical &);
	private:
		std::vector<std::unique_ptr<Centipede::Enemy>> _enemies;
		std::vector<Centipede::Obstacle> _obstacles;
		std::pair<int, int> _player;
		std::pair<int, int> _shot;
		int _score;
		int _enemiesLeft;
		std::chrono::nanoseconds start;

		bool checkPlayerColision(std::pair<int, int>);
	};
}

#endif // CENTIPEDE_