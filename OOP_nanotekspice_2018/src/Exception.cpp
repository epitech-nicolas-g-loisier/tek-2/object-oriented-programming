/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** Exception method for nano
*/

#include "Exception.hpp"

NanoException::NanoException(const std::string &message)
	: _message(message)
{
}

const char *NanoException::what() const noexcept
{
	return _message.data();
}