/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Core
*/

#include "Core.hpp"
#include <dlfcn.h>
#include <iostream>
#include <string>
#include <regex>
#include <sys/types.h>
#include <chrono>
#include <thread>
#include <dirent.h>
#include <fstream>
#include <algorithm>

Core::Core()
	: _engine(nullptr), _display(nullptr), _engineName(""), _displayName(""), _displayHandler(nullptr), _engineHandler(nullptr), _username("")
{
}

Core::~Core()
{
	_display.reset(nullptr);
	_engine.reset(nullptr);
	if (_displayHandler)
		dlclose(_displayHandler);
	if (_engineHandler)
		dlclose(_engineHandler);
}

std::vector<std::string> Core::getLibsFromDir(char const *dirname)
{
	std::vector<std::string> libnames;
	DIR *libdir = opendir(dirname);
	struct dirent *ent = NULL;

	if (!libdir)
		return libnames;
	ent = readdir(libdir);
	std::regex libnameRegex("^lib_arcade_.+\\.so$");
	while (ent) {
		if (std::regex_match(ent->d_name, libnameRegex))
			libnames.push_back(ent->d_name);
		ent = readdir(libdir);
	}
	closedir(libdir);
	return (libnames);
}

void Core::checkLibsContent(std::vector<std::string> liblist, std::vector<std::string> gamelist)
{
	if (liblist.size() == 0) {
		std::cerr << "No graphical library found in ./lib" << std::endl;
		exit(84);
	}
	if (gamelist.size() < 2) {
		std::cerr << "Not enough games found found in ./games" << std::endl;
		exit(84);
	}
}

void *Core::getHandlerFromLibpath(std::string path) noexcept
{
	void *handler = dlopen(path.c_str(), RTLD_NOW);
	char *error = dlerror();

	if (error) {
		std::cerr << error << std::endl;
		return (nullptr);
	}
	return (handler);
}

void Core::swapGame(std::string name)
{
	void *handler = getHandlerFromLibpath(std::string("./games/").append(name));

	if (!handler) {
		std::cerr << dlerror() << std::endl;
		exit(84);
	}
	void *symbol = dlsym(handler, "getIEngine");
	char *error = dlerror();
	if (error) {
		std::cerr << error << std::endl;
		exit(84);
	}
	_engine = reinterpret_cast<std::unique_ptr<IEngine>(*)(void)>(symbol)();
	if (_engineHandler)
		dlclose(_engineHandler);
	_engineHandler = handler;
	_engineName = name;
}

void Core::swapGraphLib(std::string name)
{
	void *handler = getHandlerFromLibpath(std::string("./lib/").append(name));

	if (!handler)
		exit(84);
	void *symbol = dlsym(handler, "getIGraphical");
	char *error = dlerror();
	if (error) {
		std::cerr << error << std::endl;
		exit(84);
	}
	_display = reinterpret_cast<std::unique_ptr<IGraphical>(*)(void)>(symbol)();
	if (_displayHandler)
		dlclose(_displayHandler);
	_displayHandler = handler;
	_displayName = name;
}

std::string Core::handleMenuInput(std::vector<std::string> rawInput, inputT &gameStatus, std::pair<int, int> listsSize)
{
	std::string usernameInput("");
	gameStatus.action = false;

	for (auto &&i: rawInput) {
		if (!i.compare("ESCAPE"))
			gameStatus.status = false;
		else if (!i.compare("DOWN") && gameStatus.columnIndex < 2) {
			if (gameStatus.columnIndex == 0)
				gameStatus.libIndex = gameStatus.libIndex == listsSize.first - 1 ? 0 : gameStatus.libIndex + 1;
			else
				gameStatus.gameIndex = gameStatus.gameIndex == listsSize.second - 1 ? 0 : gameStatus.gameIndex + 1;
		}
		else if (!i.compare("UP") && gameStatus.columnIndex < 2) {
			if (gameStatus.columnIndex == 0)
				gameStatus.libIndex = gameStatus.libIndex == 0 ? listsSize.first - 1 : gameStatus.libIndex - 1;
			else
				gameStatus.gameIndex = gameStatus.gameIndex == 0 ? listsSize.second - 1 : gameStatus.gameIndex - 1;
		}
		else if (!i.compare("LEFT")) {
			gameStatus.columnIndex = gameStatus.columnIndex == 0 ? 2 : gameStatus.columnIndex - 1;
		}
		else if (!i.compare("RIGHT")) {
			gameStatus.columnIndex = gameStatus.columnIndex == 2 ? 0 : gameStatus.columnIndex + 1;
		}
		else if (!i.compare("RETURN"))
			gameStatus.action = true;
		else if (!i.compare("BACKSPACE"))
			usernameInput.insert(0, "\b");
		else if (i.size() == 1)
			usernameInput.append(i);
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(20));
	return (usernameInput);
}

void Core::updateUsername(std::string usernameInput, int columnIndex)
{
	if (columnIndex != 2)
		return;
	while (usernameInput[0] == '\b') {
		if (_username.size())
			_username.pop_back();
		usernameInput.erase(0);
	}
	_username.append(usernameInput);
	if (_username.size() > 10)
		_username.resize(10);
}

std::string Core::getLibName(const std::string &longName) const
{
	std::string name = longName.substr(longName.find_last_of("_") + 1);
	return name.substr(0, name.find_last_of("."));
}

void Core::openMenu()
{
	std::vector<std::string> liblist = getLibsFromDir("./lib");
	std::vector<std::string> gamelist = getLibsFromDir("./games");
	checkLibsContent(liblist, gamelist);
	std::pair<int, int> listsSize = {liblist.size(), gamelist.size()};
	inputT gameStatus = {false, true, false, 0, 0, 0};
	getScoreTables(gamelist);
	gameStatus.libIndex = std::distance(liblist.begin(), std::find(liblist.begin(), liblist.end(), _displayName));
	if (gameStatus.libIndex >= (int)liblist.size())
		gameStatus.libIndex = 0;

	while (gameStatus.status) {
		std::string usernameInput = handleMenuInput(_display->getInput(), gameStatus, listsSize);
		updateUsername(usernameInput, gameStatus.columnIndex);
		if (gameStatus.action) {
			if (gameStatus.columnIndex == 0 && liblist[gameStatus.libIndex] != _displayName)
				swapGraphLib(liblist[gameStatus.libIndex]);
			else if (gameStatus.columnIndex == 1) {
				int score = startGame(gameStatus, liblist, gamelist);
				saveScoreTable(gamelist[gameStatus.gameIndex], score, gameStatus.gameIndex);
				getScoreTables(gamelist);
			}
		}
		_display->drawRect({{0, 0}, {17, 39}, "", Arcade::Color::BLACK, ' ', true});
		_display->drawPair({{"Graphics", -1}, {1, 0}, Arcade::Color::WHITE, false});
		for (unsigned int i = 0; i < liblist.size(); i++)
			_display->drawPair({{getLibName(liblist[i]), -1}, {2, 1.75 + i}, Arcade::Color::RED, (gameStatus.columnIndex == 0 && gameStatus.libIndex == (int)i)});
		_display->drawRect({{17, 0}, {17, 39}, "", Arcade::Color::BLACK, ' ', true});
		_display->drawPair({{"Games", -1}, {18, 0}, Arcade::Color::WHITE, false});
		for (unsigned int i = 0; i < gamelist.size(); i++)
			_display->drawPair({{getLibName(gamelist[i]), -1}, {19, 1.75 + i}, Arcade::Color::BLUE, (gameStatus.columnIndex == 1 && gameStatus.gameIndex == (int)i)});
		_display->drawRect({{34, 0}, {17, 4}, "", Arcade::Color::BLACK, ' ', true});
		_display->drawPair({{"Username:", -1}, {35, 0}, Arcade::Color::WHITE, false});
		if (gameStatus.columnIndex == 2)
			_display->drawRect({{35, 1.75}, {1, 1}, "", Arcade::Color::WHITE, '>', false});
		_display->drawPair({{_username, -1}, {37, 1.75}, Arcade::Color::WHITE, false});
		_display->drawRect({{34, 4}, {17, 35}, "", Arcade::Color::BLACK, ' ', true});
		_display->drawPair({{"Scores:", -1}, {35, 4}, Arcade::Color::WHITE, false});
		for (unsigned int i = 0; i < _scoreTables[gameStatus.gameIndex].scores.size() && i < 10; i++)
			_display->drawPair({{_scoreTables[gameStatus.gameIndex].scores[i].first, _scoreTables[gameStatus.gameIndex].scores[i].second}, {36, 5.75 + (i * 3)}, Arcade::Color::WHITE, false});
		_display->update();
	}
}

int Core::handleGameInput(std::vector<std::string> rawInput, inputT &gameStatus, std::pair<int, int> listsSize)
{
	bool restartGame = false;
	std::pair<int, int> oldIndex = {gameStatus.libIndex, gameStatus.gameIndex};
	int status = 0;

	for (auto &&i: rawInput) {
		if (!i.compare("ESCAPE")) {
			gameStatus.status = false;
			gameStatus.action = false;
		}
		else if (!i.compare("DOWN"))
			gameStatus.libIndex = gameStatus.libIndex == listsSize.first - 1 ? 0 : gameStatus.libIndex + 1;
		else if (!i.compare("UP"))
			gameStatus.libIndex = gameStatus.libIndex == 0 ? listsSize.first - 1 : gameStatus.libIndex - 1;
		else if (!i.compare("LEFT"))
			gameStatus.gameIndex = gameStatus.gameIndex == listsSize.second - 1 ? 0 : gameStatus.gameIndex + 1;
		else if (!i.compare("RIGHT"))
			gameStatus.gameIndex = gameStatus.gameIndex == 0 ? listsSize.second - 1 : gameStatus.gameIndex - 1;
		else if (!i.compare("BACKSPACE"))
			gameStatus.action = false;
		else if (!i.compare("R"))
			restartGame = true;
		else if (!i.compare("P")) {
			gameStatus.pause = !gameStatus.pause;
			std::this_thread::sleep_for(std::chrono::milliseconds(40));
		}
	}
	if (gameStatus.libIndex != oldIndex.first)
		status += 1;
	if (gameStatus.gameIndex != oldIndex.second || restartGame)
		status += 2;
	return (status);
}

void Core::reloadGameHandler(int reloadStatus, inputT &gameStatus, std::vector<std::string> gamelist)
{
	if (reloadStatus < 2)
		return;
	if (_engineName == gamelist[gameStatus.gameIndex]) {
		void *symbol = dlsym(_engineHandler, "getIEngine");
		char *error = dlerror();
		if (error) {
			std::cerr << error << std::endl;
			exit(84);
		}
		_engine = reinterpret_cast<std::unique_ptr<IEngine>(*)(void)>(symbol)();
	} else
		swapGame(gamelist[gameStatus.gameIndex]);
}

void Core::reloadLibHandler(int reloadStatus, inputT &gameStatus, std::vector<std::string> liblist)
{
	if (reloadStatus % 2 == 0)
		return;
	swapGraphLib(liblist[gameStatus.libIndex]);
}

int Core::startGame(inputT &gameStatus, std::vector<std::string> liblist, std::vector<std::string> gamelist)
{
	std::pair<int, int> listsSize = {liblist.size(), gamelist.size()};
	swapGame(gamelist[gameStatus.gameIndex]);
	std::pair<bool, int> score(true, 0);

	while (gameStatus.action) {
		auto &&input = _display->getInput();
		int reloadStatus = handleGameInput(input, gameStatus, listsSize);
		reloadLibHandler(reloadStatus, gameStatus, liblist);
		reloadGameHandler(reloadStatus, gameStatus, gamelist);
		if (!gameStatus.pause)
			score = _engine->computeInput(input);
		if (!score.first)
			return (score.second);
		_engine->display(*_display.get());
		if (gameStatus.pause)
			printPause();
		_display->update();
	}
	return (score.second);
}

void Core::getScoreTables(std::vector<std::string> gamelist)
{
	_scoreTables.clear();
	std::regex libnameRegex("^([0-9a-zA-Z]){1,10}:([ \t])*([0-9]){1,6}$");
	for (std::string game : gamelist) {
		ScoreTable scores;
		std::ifstream file("games/" + getLibName(game) + ".scores");
		std::string str;
		while (std::getline(file, str)) {
			std::pair<std::string, int> score;
			score.first = str.substr(0, str.find_first_of(":"));
			std::string tmp = str.substr(str.find_first_of(":"));
			tmp = tmp.substr(tmp.find_first_of("1234567890"));
			if (std::regex_match(str, libnameRegex)) {
				score.second = stoi(tmp);
				scores.scores.push_back(score);
			}
		}
		sort(scores.scores.begin(), scores.scores.end(),
			[](const std::pair<std::string, int> & a, const std::pair<std::string, int> & b) -> bool
				{
				return a.second > b.second;
				});
		_scoreTables.push_back(scores);
	}
}

void Core::saveScoreTable(const std::string &gameName, int score, int gameIdx)
{
	if (score < 0)
		return;
	std::ofstream outfile;

	if (_username != "")
		_scoreTables[gameIdx].scores.push_back(std::pair<std::string, int>(_username, score));
	else
		_scoreTables[gameIdx].scores.push_back(std::pair<std::string, int>("Player", score));

  	outfile.open("games/" + getLibName(gameName) + ".scores");

	for (auto score : _scoreTables[gameIdx].scores) {
  		outfile << score.first + ": " + std::to_string(score.second) << std::endl;
	}
}

void Core::printPause()
{
	Arcade::Rect pause = {{20, 15}, {40, 9}, "", Arcade::Color::MAGENTA, '*', false};

	_display->drawRect(pause);
	_display->drawPair({{"Game Pause", -1}, {35, 18}, Arcade::Color::WHITE, false});
	_display->drawPair({{"Press \'P\' to resume Game", -1}, {28, 20}, Arcade::Color::WHITE, false});
}