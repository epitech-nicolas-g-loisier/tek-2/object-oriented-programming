/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PlayerNodeFactory
*/

#ifndef PLAYERNODEFACTORY_HPP
#define PLAYERNODEFACTORY_HPP

#include <unordered_map>
#include <memory>
#include "IPlayerNode.hpp"

namespace indie
{
class GraphicalEngine;

class PlayerNodeFactory
{
public:
	PlayerNodeFactory();
	std::unique_ptr<indie::IPlayerNode> createPlayerNode(const std::string &, indie::GraphicalEngine &engine);

private:
	std::unique_ptr<indie::IPlayerNode> createMario(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IPlayerNode> createLuigi(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IPlayerNode> createPeach(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IPlayerNode> createBowser(indie::GraphicalEngine &engine) const ;

	std::unordered_map<std::string, std::unique_ptr<IPlayerNode>(PlayerNodeFactory::*)(indie::GraphicalEngine &engine) const > _ctors;

};
}

#endif //PLAYERNODEFACTORY_HPP
