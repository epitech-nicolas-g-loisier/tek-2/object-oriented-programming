/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Kick
*/

#include "Kick.hpp"
#include <iostream>

indie::Kick::Kick(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "Kick", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::Kick::interact(indie::Player *player) {
	if (player) {
		player->hasKick = true;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}