/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** AudioEngine
*/

#ifndef AUDIOENGINE_HPP
#define AUDIOENGINE_HPP

#include "Sound.hpp"

namespace indie {
class AudioEngine
{
public:
	AudioEngine();
	~AudioEngine() = default;

	void playBombSound();
	void playBreakBlockSound();
	void playPowerUpSound();
	void playPowerDownSound();
	void playLifeUpSound();

private:
	indie::Sound bombSound;
	indie::Sound breakBlockSound;
	indie::Sound powerUpSound;
	indie::Sound powerDownSound;
	indie::Sound lifeUpSound;

};
}

#endif //AUDIOENGINE_HPP
