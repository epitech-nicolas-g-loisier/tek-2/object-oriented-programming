/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Bomb
*/
#ifndef BOMBERMAN_BOMB_HPP
#define BOMBERMAN_BOMB_HPP

#include "IEntity.hpp"
#include "BombNode.hpp"
#include <utility>
#include <chrono>
#include "AudioEngine.hpp"

namespace indie {

	struct MapData;

	class Bomb : public IEntity {
	public:
		Bomb(unsigned char fireLevel, std::pair<unsigned char, unsigned char> &pos, GraphicalEngine &engine, indie::AudioEngine &_audioEngine);
		virtual bool update(indie::MapData &) override;
		bool interact(indie::Player*) override final;
		bool interact(indie::Bomb*) override final;
		bool interact(indie::Blast*) override final;
		bool interact(indie::PowerUp*) override final;
		void detonate();
		unsigned char getTimer() const;

	protected:
		virtual bool addBlastToMap(indie::MapData &, const std::pair<unsigned char, unsigned char>&) const;

		const unsigned char level;
		std::pair<unsigned char, unsigned char> _pos;
		std::chrono::steady_clock::time_point deathTime;

		BombNode node;
		GraphicalEngine &engine;
		indie::AudioEngine &audioEngine;

	};
}

#endif //BOMBERMAN_BOMB_HPP
