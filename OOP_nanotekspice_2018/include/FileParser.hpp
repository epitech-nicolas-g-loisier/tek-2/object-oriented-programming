/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** FileParser class for nano
*/

#ifndef FILEPARSER_HPP_
	#define FILEPARSER_HPP_

#include <string>
#include <vector>

class FileParser {
	public:
		FileParser(const std::string &filename);
		~FileParser();
		void parseFile();
		std::vector<std::string> getInput() const;
		std::vector<std::string> getOutput() const;
		std::vector<std::string> getComponent() const;
		std::vector<std::string> getLink() const;

	private:
		const std::string _filename;
		std::vector<std::string> _input;
		std::vector<std::string> _output;
		std::vector<std::string> _component;
		std::vector<std::string> _link;

		void getPart(const std::string line, int &part);
		void getChipset(const std::string line);
};

#endif /* !FILEPARSER_HPP_ */
