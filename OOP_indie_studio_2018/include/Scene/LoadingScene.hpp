/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** LoadingScene
*/

#ifndef LOADINGSCENE_HPP
#define LOADINGSCENE_HPP

#include "GraphicalEngine.hpp"
#include "IScene.hpp"

namespace indie
{
	class LoadingScene : public IScene
	{
	public:
		explicit LoadingScene(GraphicalEngine &);
		~LoadingScene() override = default;

		indie::IScene::sceneState draw() override;

	private:
		indie::GraphicalEngine &_engine;
	};
}
#endif //LOADINGSCENE_HPP
