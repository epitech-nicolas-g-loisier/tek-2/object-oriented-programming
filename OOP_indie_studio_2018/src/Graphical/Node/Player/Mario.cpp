/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Mario
*/

#include <iostream>
#include "Mario.hpp"
#include "GraphicalException.hpp"

indie::Mario::Mario(GraphicalEngine &engine)
{
	moveMesh = engine.scenemgr->getMesh(engine.findModelPath("Mario/walking.x").data());
	idleMesh = engine.scenemgr->getMesh(engine.findModelPath("Mario/hidle.x").data());
	danceMesh = engine.scenemgr->getMesh(engine.findModelPath("Mario/dance.x").data());
	if (!moveMesh || !danceMesh || !idleMesh)
		throw ModelLoadingError("Mario");

	texture[0] = engine.driver->getTexture(engine.findModelPath("Mario/body.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("Mario/pupil.png").data());
	texture[2] = engine.driver->getTexture(engine.findModelPath("Mario/face.png").data());
	texture[3] = engine.driver->getTexture(engine.findModelPath("Mario/glove.png").data());
	texture[4] = engine.driver->getTexture(engine.findModelPath("Mario/hair.png").data());
	texture[5] = engine.driver->getTexture(engine.findModelPath("Mario/hat.png").data());
	for (int i = 0; i < 6; ++i) {
		if (!texture[i])
			std::cerr << "Cannot find texture number: " << i << " for Mario" << std::endl;
	}

	node = engine.scenemgr->addAnimatedMeshSceneNode(idleMesh);
	if (!node)
		throw NodeCreationError("Mario");

	node->setRotation({0, -90, 0});
	node->setPosition({(float)(3), -0.5, (float)(3)});
	setTexture();
}

indie::Mario::~Mario()
{
	node->remove();
}

void indie::Mario::setTexture()
{
	for (int i = 0; i < 6; ++i) {
		if (!texture[i])
			return;
	}
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->getMaterial(0).setTexture(0, texture[0]);
	node->getMaterial(1).setTexture(0, texture[1]);
	node->getMaterial(2).setTexture(0, texture[2]);
	node->getMaterial(3).setTexture(0, texture[3]);
	node->getMaterial(4).setTexture(0, texture[3]);
	node->getMaterial(5).setTexture(0, texture[4]);
	node->getMaterial(6).setTexture(0, texture[5]);
}

void indie::Mario::idle()
{
	if (state == "idle")
		return;

	state = "idle";
	node->setMesh(idleMesh);
	node->setCurrentFrame(std::rand() % 100 + 20);
	setTexture();
}

void indie::Mario::move()
{
	if (state == "move")
		return;

	state = "move";
	node->setMesh(moveMesh);
	setTexture();
}

bool indie::Mario::dance()
{
	if (danceTime == 0) {
		node->setMesh(danceMesh);
		setTexture();
	}
	danceTime++;
	if (danceTime > 150) {
		danceTime = 0;
		return false;
	}
	return true;
}

std::pair<float, float> indie::Mario::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::Mario::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::Mario::getRotation()
{
	return node->getRotation().Y;
}

void indie::Mario::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::Mario::swapTexture(char)
{

}

void indie::Mario::setPositionY(float y)
{
	node->setPosition({node->getPosition().X, y, node->getPosition().X});
}
