/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** SpeedUp
*/

#include "SpeedUp.hpp"
#include <iostream>

indie::SpeedUp::SpeedUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "SpeedUp", _audioEngine)
{

	node.setPosition(pos);
}

bool indie::SpeedUp::interact(indie::Player *player) {
	if (player) {
		if (player->speedLevel < 8)
			player->speedLevel++;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}
