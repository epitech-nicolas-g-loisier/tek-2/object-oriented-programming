/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Player
*/

#include <irrlicht.h>
#include <array>
#include "GraphicalEngine.hpp"
#include "IPlayerNode.hpp"

#ifndef LUIGI_HPP
#define LUIGI_HPP

namespace indie
{
class Luigi : public IPlayerNode
{

public:
	explicit Luigi(GraphicalEngine&);
	~Luigi() override;
	void move() override;
	void idle() override;
	bool dance() override;

	int danceTime = 0;

	std::pair<float, float> getPosition() override;
	void setPosition(const std::pair<float, float> &) override;
	void setPositionY(float) override;
	float getRotation() override;
	void setRotation(float) override;
	void swapTexture(char) override;

private:
	void setTexture() override;

	irr::scene::IAnimatedMesh *idleMesh{};
	irr::scene::IAnimatedMesh *moveMesh{};
	irr::scene::IAnimatedMesh *danceMesh{};
	irr::scene::IAnimatedMeshSceneNode *node{};
	std::array<irr::video::ITexture *, 7> texture{};
	std::string state = "idle";
};
}

#endif //LUIGI_H
