/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** DangerousBombPowerup
*/

#include "DangerousBombPowerup.hpp"

indie::DangerousBombPowerup::DangerousBombPowerup(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "DangerousBomb", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::DangerousBombPowerup::interact(indie::Player *player)
{
	if (player) {
		player->bombType = DANGEROUS;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}