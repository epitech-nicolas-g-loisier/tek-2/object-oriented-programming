/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** FireUp
*/

#include "FireUp.hpp"
#include <iostream>

indie::FireUp::FireUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "FireUp", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::FireUp::interact(indie::Player *player) {
	if (player) {
		if (player->fireLevel < 8)
			player->fireLevel++;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}