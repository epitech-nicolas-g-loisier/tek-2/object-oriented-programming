/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** IScene
*/

#ifndef ISCENE_HPP
#define ISCENE_HPP

#include <string>

namespace indie
{
class IScene
{
public:
	enum sceneState {
		SPLASHSCREEN,
		BEGIN,
		TITLESCREEN,
		PLAYERSELECTOR,
		MAPSELECTOR,
		LOADING,
		POWERUP,
		GAME,
		GAMEEND,
		LOADSAVE,
		OPTION,
		EXIT,
	};

	virtual ~IScene() = default;
	virtual sceneState draw() = 0;

};
}

#endif //ISCENE_HPP
