/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** BlockFactory
*/
#include "BlockFactory.hpp"
#include "GameException.hpp"

indie::BlockFactory::BlockFactory(GraphicalEngine &graphicalEngine, AudioEngine &_audioEngine)
	:	rand(std::knuth_b(std::random_device()())), engine(graphicalEngine), audioEngine(_audioEngine)
{}

std::shared_ptr<indie::Block> indie::BlockFactory::createBlock(char c, std::pair<float, float> position)
{
	if (c != '#' && c != 'x')
		throw indie::GameException("Unknown type of block");
	int hasPowerUp = std::uniform_int_distribution<int>(1, 3)(rand) == 1;
	if (hasPowerUp && c == 'x')
		return (std::move(std::make_shared<Block>(pFactory, true, position, engine, audioEngine)));
	return (std::move(std::make_shared<Block>(c == 'x', position, engine, audioEngine)));
}