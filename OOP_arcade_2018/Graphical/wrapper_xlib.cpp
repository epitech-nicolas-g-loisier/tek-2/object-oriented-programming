/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Wrapper for the sfml lib
*/

#include "IGraphical.hpp"
#include <iostream>
#include <memory>
#include <sstream>
#include <iomanip>
#include <X11/Xlib.h>
#include <chrono>
#include <thread>

__attribute__ ((constructor))
void wrapper_ctor()
{
}

__attribute__ ((destructor))
void wrapper_dtor()
{
}

class XlibWindow : public IGraphical
{
public:
	XlibWindow();
	~XlibWindow();
	void drawLine(const Arcade::Line &) final;
	void drawRect(const Arcade::Rect &) final;
	void drawPair(const Arcade::Pair &) final;
	void update() final;
	std::vector<std::string> getInput() final;

private:
	Display *display;
	Window window;
	GC gc;
	XEvent event;
	Font font;

	std::vector<std::pair<unsigned int, const char*>> bindings;
	std::vector<std::string> input;
	std::array<XColor, 8> _colors;

	void setColor();
};

XlibWindow::XlibWindow()
{
	display = XOpenDisplay(NULL);
	setColor();
	if (display == NULL) {
		throw "XlibWindow::XlibWindow: Cannot create Display!";
	}
	int s = DefaultScreen(display);
	window = XCreateSimpleWindow(display, RootWindow(display, s), 0, 0, 1600, 800, 0, 0, 0);

	Atom delWindow = XInternAtom(display, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(display, window, &delWindow, 1);
	XSelectInput(display, window, ExposureMask | KeyPressMask);
	XMapWindow(display, window);

	gc = XCreateGC(display, window, 0, 0);

	XNextEvent(display, &event);

	font = XLoadFont(display, "-adobe-helvetica-*-r-normal-*-20-*-*-*-*-*-*-*");

	bindings.push_back(std::pair<unsigned int, const char*>(111, "UP"));
	bindings.push_back(std::pair<unsigned int, const char*>(116, "DOWN"));
	bindings.push_back(std::pair<unsigned int, const char*>(113, "LEFT"));
	bindings.push_back(std::pair<unsigned int, const char*>(114, "RIGHT"));
	bindings.push_back(std::pair<unsigned int, const char*>(9, "ESCAPE"));
	bindings.push_back(std::pair<unsigned int, const char*>(36, "RETURN"));
	bindings.push_back(std::pair<unsigned int, const char*>(22, "BACKSPACE"));
	bindings.push_back(std::pair<unsigned int, const char*>(65, " "));
	bindings.push_back(std::pair<unsigned int, const char*>(19, "0"));
	bindings.push_back(std::pair<unsigned int, const char*>(10, "1"));
	bindings.push_back(std::pair<unsigned int, const char*>(11, "2"));
	bindings.push_back(std::pair<unsigned int, const char*>(12, "3"));
	bindings.push_back(std::pair<unsigned int, const char*>(13, "4"));
	bindings.push_back(std::pair<unsigned int, const char*>(14, "5"));
	bindings.push_back(std::pair<unsigned int, const char*>(15, "6"));
	bindings.push_back(std::pair<unsigned int, const char*>(16, "7"));
	bindings.push_back(std::pair<unsigned int, const char*>(17, "8"));
	bindings.push_back(std::pair<unsigned int, const char*>(18, "9"));
	bindings.push_back(std::pair<unsigned int, const char*>(24, "A"));
	bindings.push_back(std::pair<unsigned int, const char*>(56, "B"));
	bindings.push_back(std::pair<unsigned int, const char*>(54, "C"));
	bindings.push_back(std::pair<unsigned int, const char*>(40, "D"));
	bindings.push_back(std::pair<unsigned int, const char*>(26, "E"));
	bindings.push_back(std::pair<unsigned int, const char*>(41, "F"));
	bindings.push_back(std::pair<unsigned int, const char*>(42, "G"));
	bindings.push_back(std::pair<unsigned int, const char*>(43, "H"));
	bindings.push_back(std::pair<unsigned int, const char*>(31, "I"));
	bindings.push_back(std::pair<unsigned int, const char*>(44, "J"));
	bindings.push_back(std::pair<unsigned int, const char*>(45, "K"));
	bindings.push_back(std::pair<unsigned int, const char*>(46, "L"));
	bindings.push_back(std::pair<unsigned int, const char*>(47, "M"));
	bindings.push_back(std::pair<unsigned int, const char*>(57, "N"));
	bindings.push_back(std::pair<unsigned int, const char*>(32, "O"));
	bindings.push_back(std::pair<unsigned int, const char*>(33, "P"));
	bindings.push_back(std::pair<unsigned int, const char*>(38, "Q"));
	bindings.push_back(std::pair<unsigned int, const char*>(27, "R"));
	bindings.push_back(std::pair<unsigned int, const char*>(39, "S"));
	bindings.push_back(std::pair<unsigned int, const char*>(28, "T"));
	bindings.push_back(std::pair<unsigned int, const char*>(30, "U"));
	bindings.push_back(std::pair<unsigned int, const char*>(55, "V"));
	bindings.push_back(std::pair<unsigned int, const char*>(52, "W"));
	bindings.push_back(std::pair<unsigned int, const char*>(53, "X"));
	bindings.push_back(std::pair<unsigned int, const char*>(29, "Y"));
	bindings.push_back(std::pair<unsigned int, const char*>(25, "Z"));
}

void XlibWindow::setColor()
{
	Colormap colormap;

	colormap = DefaultColormap(display, 0);

	XParseColor(display, colormap, "#000000", &_colors[Arcade::BLACK]);
	XAllocColor(display, colormap, &_colors[Arcade::BLACK]);

	XParseColor(display, colormap, "#FF0000", &_colors[Arcade::RED]);
	XAllocColor(display, colormap, &_colors[Arcade::RED]);

	XParseColor(display, colormap, "#00FF00", &_colors[Arcade::GREEN]);
	XAllocColor(display, colormap, &_colors[Arcade::GREEN]);

	XParseColor(display, colormap, "#FFFF00", &_colors[Arcade::YELLOW]);
	XAllocColor(display, colormap, &_colors[Arcade::YELLOW]);

	XParseColor(display, colormap, "#0000FF", &_colors[Arcade::BLUE]);
	XAllocColor(display, colormap, &_colors[Arcade::BLUE]);

	XParseColor(display, colormap, "#FF00FF", &_colors[Arcade::MAGENTA]);
	XAllocColor(display, colormap, &_colors[Arcade::MAGENTA]);

	XParseColor(display, colormap, "#00FFFF", &_colors[Arcade::CYAN]);
	XAllocColor(display, colormap, &_colors[Arcade::CYAN]);

	XParseColor(display, colormap, "#FFFFFF", &_colors[Arcade::WHITE]);
	XAllocColor(display, colormap, &_colors[Arcade::WHITE]);
}

XlibWindow::~XlibWindow()
{
	XDestroyWindow(display, window);
	XCloseDisplay(display);
}

void XlibWindow::drawLine(const Arcade::Line &line)
{
	int symb = -1 + 2 * (line.dir % 2);
	float x2 = line.dir < 2 ? line.pos.first : line.pos.first + line.length * symb;
	float y2 = line.dir < 2 ? line.pos.second + line.length * symb : line.pos.second;

	XSetForeground(display, gc, _colors[Arcade::WHITE].pixel);
	XDrawLine(display, window, gc, line.pos.first * 20, line.pos.second * 20, x2 * 20, y2 * 20);
}

void XlibWindow::drawRect(const Arcade::Rect &rect)
{
	if (rect.isOutlined) {
		XSetForeground(display, gc, _colors[Arcade::Color::WHITE].pixel);
		XDrawRectangle(display, window, gc, rect.pos.first * 20 - 2, rect.pos.second * 20 - 2, rect.size.first * 20 + 2, rect.size.second * 20 + 2);
	}
	XSetForeground(display, gc, _colors[rect.defaultColor].pixel);
	XFillRectangle(display, window, gc, rect.pos.first * 20, rect.pos.second * 20, rect.size.first * 20, rect.size.second * 20);
}

void XlibWindow::drawPair(const Arcade::Pair &pair)
{
	XTextItem text;
	text.font = font;
	text.delta = 0;

	if (pair.value.second >= 0) {
		std::stringstream ss;
		ss << std::setfill('0') << std::setw(6) << pair.value.second;
		std::string str = ss.rdbuf()->str();
		text.chars = const_cast <char*>(str.c_str());
		text.nchars = str.size();
		if (pair.background && pair.color != Arcade::Color::WHITE) {
			drawRect({{pair.pos.first, pair.pos.second + 1}, {text.nchars * 0.65, 1.4}, "", pair.color, 0, false});
			XSetForeground(display, gc, _colors[Arcade::Color::WHITE].pixel);
		} else if (pair.background) {
			drawRect({{pair.pos.first, pair.pos.second + 1}, {text.nchars * 0.65, 1.4}, "", pair.color, 0, false});
			XSetForeground(display, gc, _colors[Arcade::Color::BLACK].pixel);
		} else {
			XSetForeground(display, gc, _colors[pair.color].pixel);
		}
		XDrawText(display, window, gc, pair.pos.first * 20, pair.pos.second * 20 + 45, &text, 1);
	}

	text.chars = const_cast <char*>(pair.value.first.c_str());
	text.nchars = pair.value.first.size();
	if (pair.background && pair.color != Arcade::Color::WHITE) {
		drawRect({pair.pos, {text.nchars * 0.65, 1.2}, "", pair.color, 0, false});
		XSetForeground(display, gc, _colors[Arcade::Color::WHITE].pixel);
	} else if (pair.background) {
		drawRect({pair.pos, {text.nchars * 0.65, 1.2}, "", pair.color, 0, false});
		XSetForeground(display, gc, _colors[Arcade::Color::BLACK].pixel);
	} else {
		XSetForeground(display, gc, _colors[pair.color].pixel);
	}
	XDrawText(display, window, gc, pair.pos.first * 20, pair.pos.second * 20 + 20, &text, 1);
}

void XlibWindow::update()
{
	XTextItem text;
	text.font = font;
	text.delta = 0;
	text.chars = const_cast <char*>("XLib");
	text.nchars = 4;

	input.clear();
	XDrawText(display, window, gc, 75 * 20, 38 * 20, &text, 1);
	while(XPending(display)) {
		XNextEvent(display, &event);
		for (std::pair<unsigned int, const char*> binding : bindings) {
			if (binding.first == event.xkey.keycode) {
				input.push_back(binding.second);
			}
		}
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(40));
	XClearArea(display, window, 0, 0, 1600, 800, 0);
}

std::vector<std::string> XlibWindow::getInput()
{
	return input;
}

extern "C" std::unique_ptr<IGraphical> getIGraphical()
{
	return (std::make_unique<XlibWindow>());
}