/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Bomb
*/

#ifndef BOMBNODE_HPP
#define BOMBNODE_HPP

#include <irrlicht.h>
#include <array>
#include <chrono>
#include "GraphicalEngine.hpp"
#include "INode.hpp"

namespace indie
{
class BombNode : public indie::INode
{

public:
	explicit BombNode(GraphicalEngine &engine);
	~BombNode() override;
	std::pair<float, float> getPosition() override;
	void setPosition(const std::pair<float, float> &) override;
	float getRotation() override;
	void setRotation(float) override;
	void swapTexture(char) override;

private:
	void setTexture() override;

	irr::scene::IMesh *mesh;
	irr::scene::IMeshSceneNode *node;
	std::array<irr::video::ITexture *, 2> texture{};
	char state = 0;

};
}

#endif //BOMBNODE_HPP
