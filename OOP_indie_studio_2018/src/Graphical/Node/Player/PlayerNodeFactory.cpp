/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PlayerNodeFactory
*/

#include "Bowser.hpp"
#include "Peach.hpp"
#include "Luigi.hpp"
#include "Mario.hpp"
#include "PlayerNodeFactory.hpp"
#include "GraphicalException.hpp"

indie::PlayerNodeFactory::PlayerNodeFactory()
{
	_ctors["mario"] = &PlayerNodeFactory::createMario;
	_ctors["luigi"] = &PlayerNodeFactory::createLuigi;
	_ctors["peach"] = &PlayerNodeFactory::createPeach;
	_ctors["bowser"] = &PlayerNodeFactory::createBowser;
}

std::unique_ptr<indie::IPlayerNode> indie::PlayerNodeFactory::createPlayerNode(const std::string &choice, indie::GraphicalEngine &engine)
{
	if (_ctors.count(choice) != 0)
		return (this->*_ctors[choice])(engine);
	throw GraphicalException("Unknown Player Node", "PlayerNodeFactory");
}

std::unique_ptr<indie::IPlayerNode> indie::PlayerNodeFactory::createMario(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::Mario>(engine);
}

std::unique_ptr<indie::IPlayerNode> indie::PlayerNodeFactory::createLuigi(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::Luigi>(engine);
}

std::unique_ptr<indie::IPlayerNode> indie::PlayerNodeFactory::createPeach(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::Peach>(engine);
}

std::unique_ptr<indie::IPlayerNode>
indie::PlayerNodeFactory::createBowser(indie::GraphicalEngine &engine) const
{
	return std::make_unique<indie::Bowser>(engine);
}
