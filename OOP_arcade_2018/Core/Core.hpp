/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Core class of the arcade
*/

#include "IEngine.hpp"
#include "Score.hpp"
#include <memory>
#include <vector>

#ifndef CORE_H_
	#define CORE_H_

typedef struct inputS
{
	bool pause;
	bool status;
	bool action;
	int gameIndex;
	int libIndex;
	int columnIndex;
} inputT;

class Core
{
public:
	Core();
	~Core();
	void openMenu();
	void swapGraphLib(std::string);

private:
	std::unique_ptr<IEngine> _engine;
	std::unique_ptr<IGraphical> _display;
	std::string _engineName;
	std::string _displayName;
	void *_displayHandler;
	void *_engineHandler;
	std::vector<ScoreTable> _scoreTables;
	std::string _username;

	void updateUsername(std::string, int);
	std::string handleMenuInput(std::vector<std::string>, inputT &, std::pair<int, int>);
	std::vector<std::string> getLibsFromDir(char const *dirname);
	void checkLibsContent(std::vector<std::string>, std::vector<std::string>);
	void *getHandlerFromLibpath(std::string) noexcept;
	std::string getLibName(const std::string &) const;
	void reloadGameHandler(int reloadStatus, inputT &gameStatus, std::vector<std::string> gamelist);
	void reloadLibHandler(int reloadStatus, inputT &gameStatus, std::vector<std::string> liblist);
	int handleGameInput(std::vector<std::string> rawInput, inputT &gameStatus, std::pair<int, int> listsSize);
	void swapGame(std::string);
	int startGame(inputT &gamestatus, std::vector<std::string> liblist, std::vector<std::string> gamelist);
	void getScoreTables(std::vector<std::string>);
	void saveScoreTable(const std::string &, int, int);
	void printPause();
};

#endif // CORE_H_