/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Parser
*/

#include <array>
#include <sstream>
#include <iostream>
#include <algorithm>
#include "XMLParser/Tag.hpp"
#include "XMLParser.hpp"

namespace XML
{

XMLParser::XMLParser(std::shared_ptr<std::istream> is)
: is(is)
{
	if (!is || is->eof())
		throw std::invalid_argument("Cannot open fine");
	{ /* Skip 1st content */
		std::array<char, 1024> buf;
		is->getline(&buf[0], buf.size(), '<');
	}
	_begin = Node(is);
	auto l = Tag::Parse::layer(_begin->str);
	if (l == -1)
		throw std::invalid_argument("File structure error, closing bracket");
}

Node XMLParser::begin() const
{
	return this->_begin;
}

Node XMLParser::end() const
{
	return Node();
}


Node XMLParser::find(const std::string &str) const
{
	return Node::find(_begin, end(), str);
}

std::vector<Node> XMLParser::findAll(const std::string &str) const
{
	return Node::findAll(_begin, end(), str);
}

SimpleType XMLParser::get(const std::string &str) const
{
	return Node::get(_begin, end(), str);
}

std::vector<SimpleType> XMLParser::getAll(const std::string &str) const
{
	return Node::getAll(_begin, end(), str);
}

}
