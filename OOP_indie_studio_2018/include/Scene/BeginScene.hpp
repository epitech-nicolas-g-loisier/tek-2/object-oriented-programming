/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** BeginScene
*/

#ifndef BEGINSCENE_HPP
#define BEGINSCENE_HPP

#include "IScene.hpp"

namespace indie
{
class BeginScene : public IScene
{
public:
	explicit BeginScene(indie::GraphicalEngine &engine);
	~BeginScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	std::string title;
	std::string background;

};
}

#endif //BEGINSCENE_HPP
