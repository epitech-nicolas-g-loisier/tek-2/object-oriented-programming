/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** OptionScene
*/

#ifndef OPTIONSCENE_HPP
#define OPTIONSCENE_HPP

#include "IScene.hpp"
#include "GraphicalEngine.hpp"
#include "GameOption.hpp"

namespace indie
{
class OptionScene : public IScene
{
public:
	OptionScene(indie::GraphicalEngine &engine, indie::GameOption &option);
	~OptionScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	indie::GameOption &gameOption;

	void drawPlayerKeys(int);
	void drawPlayerKey(const std::string& icon, int choice, std::pair<int, int> pos, rect_t rect, int);
	bool selected = false;
	int selectedChoice[4] = {-1, -1, -1, -1};
	color_t color[4] = {{0, 0, 255, 255}, {255, 0, 0, 255}, {0, 255, 0, 255}, {255, 255, 0, 255}};

	void drawMusic();
	std::array<std::string, 3> music;
	bool clicked = false;
};
}

#endif //OPTIONSCENE_HPP
