/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** MapSelectorScene
*/

#include <iostream>
#include "MapSelectorScene.hpp"

indie::MapSelectorScene::MapSelectorScene(indie::GraphicalEngine &engine, indie::GameOption &_gameOption)
	: _engine(engine), gameOption(_gameOption)
{
	engine.device->getCursorControl()->setPosition(1440, 600);
}

indie::IScene::sceneState indie::MapSelectorScene::draw()
{
	float mousePosX = _engine.receiver.mousePos().first;

	if (mousePosX < 960) {
		gameOption.mapChoice = "grid";
	} else {
		gameOption.mapChoice = "procedural";
	}

	_engine.beginScene({120, 102, 136, 255});
	_engine.drawRectangle({gameOption.mapChoice == "grid" ? 0 : 960, 0, gameOption.mapChoice == "grid" ? 960 : 1920, 1080},
		{0, 150, 255, 200});
	_engine.drawText("Grid Map", "medium", {255, 255, 255, 255}, {0, 0, 960, 1080});
	_engine.drawText("Procedural Map", "medium", {255, 255, 255, 255}, {960, 0, 1920, 1080});

	if (_engine.receiver.leftMousePressed()) {
		return indie::IScene::LOADING;
	}

	_engine.drawCursor();
	_engine.endScene();
	return indie::IScene::MAPSELECTOR;
}
