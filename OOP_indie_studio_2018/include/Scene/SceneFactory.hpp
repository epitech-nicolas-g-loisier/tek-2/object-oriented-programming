/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** SceneFactory
*/

#ifndef SCENEFACTORY_HPP
#define SCENEFACTORY_HPP

#include <memory>
#include "GraphicalEngine.hpp"
#include "IScene.hpp"
#include "GameOption.hpp"

namespace indie
{
class SceneFactory
{
public:
	SceneFactory();
	std::unique_ptr<indie::IScene> createScene(indie::IScene::sceneState, indie::GraphicalEngine &engine, indie::GameOption&);

private:
	std::unique_ptr<indie::IScene> createBegin(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IScene> createTitleScreen(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IScene> createLoading(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IScene> createSplashScreen(indie::GraphicalEngine &engine) const ;
	std::unique_ptr<indie::IScene> createPowerUpScreen(indie::GraphicalEngine &engine) const ;

	std::unique_ptr<indie::IScene> createPlayerSelector(indie::GraphicalEngine &engine, indie::GameOption&) const ;
	std::unique_ptr<indie::IScene> createMapSelector(indie::GraphicalEngine &engine, indie::GameOption&) const ;
	std::unique_ptr<indie::IScene> createGame(indie::GraphicalEngine &engine, indie::GameOption&) const ;
	std::unique_ptr<indie::IScene> createOption(indie::GraphicalEngine &engine, indie::GameOption&) const ;
	std::unique_ptr<indie::IScene> createGameEnd(indie::GraphicalEngine &engine, indie::GameOption&) const ;

	std::unordered_map<indie::IScene::sceneState, std::unique_ptr<IScene>(SceneFactory::*)(indie::GraphicalEngine &engine) const > _ctors;
	std::unordered_map<indie::IScene::sceneState, std::unique_ptr<IScene>(SceneFactory::*)(indie::GraphicalEngine &engine, indie::GameOption &gameOption) const > _ctorsOption;

};
}

#endif //SCENEFACTORY_HPP
