/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** MapVisitors
*/
#ifndef BOMBERMAN_MAPVISITORS_HPP
#define BOMBERMAN_MAPVISITORS_HPP

#include "IGameObject.hpp"
#include "Block.hpp"

template<typename T> struct is_shared_ptr : std::false_type {};
template<typename T> struct is_shared_ptr<std::shared_ptr<T>&> : std::true_type {};

#define ENTITY_VISITOR(WPTR) [&WPTR](auto &&arg) mutable {\
	if (!is_shared_ptr<decltype(arg)>::value || !std::is_same_v<std::weak_ptr<indie::IGameObject>, decltype(WPTR)>)\
		throw std::exception();\
	using T = std::remove_pointer_t<std::decay_t<decltype(arg.get())>>;\
	if constexpr (!std::is_base_of_v<indie::IGameObject, T>)\
		throw std::exception();\
	WPTR = arg;}

namespace indie {
	struct EntityTypeVisitor {

		enum EntityType {
			BLOCK = 0,
			BOMB = 1,
			BLAST = 2,
			POWERUP = 3,
			MAX_ENTITYTYPE = 4
		};

		EntityTypeVisitor() : type(EntityType::BLOCK), misc(0) {}
		void operator()(const std::shared_ptr<indie::PowerUp> &ptr);
		void operator()(const std::shared_ptr<indie::Blast> &ptr);
		void operator()(const std::shared_ptr<indie::Bomb> &ptr);
		void operator()(const std::shared_ptr<indie::Block> &ptr);

		EntityType type;
		unsigned char misc;
	};
}

#endif //BOMBERMAN_MAPVISITORS_HPP
