/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Parser contains all actual nodes in a stack
** To secure operations, the filename can be given
*/

#ifndef XMLPARSER_HPP_
#define XMLPARSER_HPP_

#include <fstream>
#include <memory>
#include <stack>
#include "Node.hpp"

namespace XML
{

class Parser
{
  public:
	Parser(std::shared_ptr<std::istream> &is);

	~Parser() = default;

	std::unique_ptr<Node> find(const std::string &name);

  private:
	std::weak_ptr<std::istream> is;
	std::unique_ptr<Node> begin;
};

}

#endif /* !XMLPARSER_HPP_ */
