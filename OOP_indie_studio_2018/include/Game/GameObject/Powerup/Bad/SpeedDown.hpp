/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** SpeedDown
*/

#ifndef BOMBERMAN_SPEEDDOWN_HPP
#define BOMBERMAN_SPEEDDOWN_HPP

#include "PowerUp.hpp"

namespace indie {
	class SpeedDown : virtual public PowerUp {
	public:
		SpeedDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //BOMBERMAN_SPEEDDOWN_HPP
