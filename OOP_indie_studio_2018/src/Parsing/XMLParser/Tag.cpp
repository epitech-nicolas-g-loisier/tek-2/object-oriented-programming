/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Tag
*/

#include <algorithm>
#include <exception>
#include <vector>
#include <array>
#include <iostream>
#include "XMLParser/Tag.hpp"

namespace XML
{

Tag::Tag(const std::string &str)
{
	info.type = Parse::type(str);
	if (info.type == Comment)
		return;
	info.name = Parse::name(str);
	if (info.name.empty())
		throw std::invalid_argument("Syntax Error, tag has no name");
	auto nameEnd = std::find_if(str.begin(), str.end(), isspace);
	if (nameEnd == str.end())
		return;
	auto argIt = std::find_if(nameEnd, str.end(), isalnum);
	while (argIt != str.end()) {
		auto argEnd = std::find_if(argIt, str.end(), isspace);
		if (!info.attributes.insert(Attribute(std::string(argIt, argEnd))).second) {
			throw std::invalid_argument("Attribute is already declared");
		}
		argIt = std::find_if(argEnd, str.end(), isalnum);
	}
}

std::string Tag::Parse::name(const std::string &str)
{
	auto nameBegin = std::find_if(str.begin(), str.end(), isalnum);
	auto nameEnd = std::find_if_not(nameBegin, str.end(), isalnum);
	if (nameBegin == str.end())
		throw std::invalid_argument("Bad token name");
	return std::string(nameBegin, nameEnd);
}

Tag::Type Tag::Parse::type(const std::string &str)
{
	if (str.empty())
		return Type::UndefinedType;
	{ /* Comment */
		if (str.size() > 6) {
			if (!str.compare(0, 3, "!--") && !str.compare(str.size() - 3, 2, "--"))
			return Type::Comment;
		}
	}{ /* Meta */
		if (str.size() >= 2) {
			auto b = str.begin();
			auto e = str.rbegin();
			if (!*e)
				++e;
			if (*b == '?') {
				if (*e == '?')
					return Type::Meta;
				else
					return Type::UndefinedType;
			}
		}
	}{ /* Basic */
		return Basic;
	}
}

Tag::Layer Tag::Parse::layer(const std::string &str)
{
	if (Parse::type(str) != Basic) {
		return Single;
	}
	auto b = str.begin();
	auto e = str.rbegin();

	if (!*e)
		e++;
	if (*e == '/') {
		if (*b == '/')
			throw std::invalid_argument("Too many slashes");
		else
			return Single;
	} else if (*b == '/') {
		return Closing;
	} else {
		return Opening;
	}
}

bool Tag::Parse::hasAttributes(const std::string &str)
{
	auto space = std::find_if(str.begin(), str.end(), isspace);
	if (space == str.end())
		return false;
	auto nextarg = std::find_if(space, str.end(), isalnum);
	if (nextarg == str.end())
		return false;
	else
		return true;
}

const Tag::Info &Tag::operator*() const
{
	return info;
}

const Tag::Info *Tag::operator->() const
{
	return &info;
}

Tag::Attribute::Attribute(const std::string &str)
{
	auto equal = std::find(str.begin(), str.end(), '=');
	auto space = std::find_if(str.begin(), str.end(), isspace);
	if (equal == str.end() || space < equal)
		throw std::invalid_argument("Syntax Error");
	this->first = std::string(str.begin(), equal);
	this->second = SimpleType(std::string(equal + 1, std::find_if(equal, str.end(), isspace)));
}

}
