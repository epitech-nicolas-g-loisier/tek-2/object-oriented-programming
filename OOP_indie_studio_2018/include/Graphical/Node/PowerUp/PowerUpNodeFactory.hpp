/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpNodeFactory
*/

#include <irrlicht.h>
#include <unordered_map>

#ifndef POWERUPNODEFACTORY_HPP
#define POWERUPNODEFACTORY_HPP

namespace indie
{

class GraphicalEngine;

class PowerUpNodeFactory
{
public:
	PowerUpNodeFactory();
	~PowerUpNodeFactory() = default;

	irr::scene::IMeshSceneNode *createNode(indie::GraphicalEngine&, const std::string &);

private:
	irr::scene::IMeshSceneNode *createBombUpNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createBombDownNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createDangerousBombNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createlineBombNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createPierceBombNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createFireUpNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createFireDownNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createFireMaxNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createHeartNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createKickNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createPunchNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createSpeedUpNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createSpeedDownNode(indie::GraphicalEngine &engine) const;
	irr::scene::IMeshSceneNode *createSkullNode(indie::GraphicalEngine &engine) const;

	std::unordered_map<std::string, irr::scene::IMeshSceneNode *(PowerUpNodeFactory::*)(indie::GraphicalEngine &engine) const> _ctors;

};
}


#endif //POWERUPNODEFACTORY_HPP
