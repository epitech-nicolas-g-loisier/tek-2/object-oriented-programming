/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Peach
*/

#include <iostream>
#include "Peach.hpp"
#include "GraphicalException.hpp"

indie::Peach::Peach(GraphicalEngine &engine)
{
	moveMesh = engine.scenemgr->getMesh(engine.findModelPath("Peach/Walking.x").data());
	idleMesh = engine.scenemgr->getMesh(engine.findModelPath("Peach/Idle.x").data());
	danceMesh = engine.scenemgr->getMesh(engine.findModelPath("Peach/Dance.x").data());
	if (!moveMesh || !danceMesh || !idleMesh)
		throw ModelLoadingError("Peach");

	texture[0] = engine.driver->getTexture(engine.findModelPath("Peach/peachsuit_alb.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("Peach/peacheye_alb.1.png").data());
	for (int i = 0; i < 2; ++i) {
		if (!texture[i])
			std::cerr << "Cannot find texture number: " << i << " for Peach" << std::endl;
	}

	node = engine.scenemgr->addAnimatedMeshSceneNode(idleMesh);
	if (!node)
		throw NodeCreationError("Peach");

	node->setRotation({0, -90, 0});
	node->setPosition({(float)(3), -0.5, (float)(3)});
	setTexture();
}

indie::Peach::~Peach()
{
	node->remove();
}

void indie::Peach::setTexture()
{
	for (int i = 0; i < 2; ++i) {
		if (!texture[i])
			return;
	}
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->getMaterial(0).setTexture(0, texture[0]);
	node->getMaterial(1).setTexture(0, texture[1]);
}

void indie::Peach::idle()
{
	if (state == "idle")
		return;

	state = "idle";
	node->setMesh(idleMesh);
	node->setCurrentFrame(std::rand() % 100 + 20);
	setTexture();
}

void indie::Peach::move()
{
	if (state == "move")
		return;

	state = "move";
	node->setMesh(moveMesh);
	setTexture();
}

bool indie::Peach::dance()
{
	if (danceTime == 0) {
		node->setMesh(danceMesh);
		setTexture();
	}
	danceTime++;
	if (danceTime > 150) {
		danceTime = 0;
		return false;
	}
	return true;
}

std::pair<float, float> indie::Peach::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::Peach::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::Peach::getRotation()
{
	return node->getRotation().Y;
}

void indie::Peach::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::Peach::swapTexture(char)
{

}

void indie::Peach::setPositionY(float y)
{
	node->setPosition({node->getPosition().X, y, node->getPosition().X});
}
