/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** MapVisitors
*/

#include "MapVisitors.hpp"

void indie::EntityTypeVisitor::operator()(const std::shared_ptr<indie::PowerUp> &ptr) {
	type = EntityType::POWERUP;
	misc = ptr->isBonus;
}

void indie::EntityTypeVisitor::operator()(const std::shared_ptr<indie::Blast> &ptr) {
	type = EntityType::BLAST;
	misc = ptr->getTimeToLive();
}

void indie::EntityTypeVisitor::operator()(const std::shared_ptr<indie::Bomb> &ptr) {
	type = EntityType::BOMB;
	misc = ptr->getTimer();
}

void indie::EntityTypeVisitor::operator()(const std::shared_ptr<indie::Block> &ptr) {
	type = EntityType::BLOCK;
	misc = ptr->isDestroyable;
}