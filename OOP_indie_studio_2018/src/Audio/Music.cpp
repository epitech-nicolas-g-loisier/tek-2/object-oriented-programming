/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Music
*/

#include "AssetPath.hpp"
#include "Music.hpp"

indie::Music::Music(const std::string &file)
	: current(file)
{
	music.openFromFile(SoundPath + "/" + file);
	music.play();
	music.setVolume(50);
}

void indie::Music::switchMusic(const std::string &file)
{
	if (file == current)
		return;
	else if (file == "stop") {
		music.stop();
		return;
	}
	current = file;
	music.openFromFile(SoundPath + "/" + file);
	music.play();
	music.setVolume(20);
}

void indie::Music::stop()
{
	music.stop();
}

void indie::Music::play()
{
	music.play();
}
