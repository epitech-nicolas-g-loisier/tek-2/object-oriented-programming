/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameScene
*/

#ifndef GAMESCENE_HPP
#define GAMESCENE_HPP

#include "GraphicalEngine.hpp"
#include "IScene.hpp"

#include "GameEngine.hpp"
#include "GameOption.hpp"
#include "GamePause.hpp"

namespace indie
{
class GameScene : public IScene
{
public:
	explicit GameScene(GraphicalEngine &, indie::GameOption &);
	~GameScene() override = default;

	indie::IScene::sceneState draw() override;
private:
	indie::GraphicalEngine &_engine;
	indie::GameOption &gameOption;

	indie::GameEngine gameEngine;
	indie::GamePause pause;
	bool isPause = false;

	int gameState = 0;

	indie::Sound victory;
	indie::Sound defeat;

};
}

#endif //GAMESCENE_HPP
