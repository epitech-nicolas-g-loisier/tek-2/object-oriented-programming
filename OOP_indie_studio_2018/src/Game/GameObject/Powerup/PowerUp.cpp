/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** PowerUp
*/
#include "PowerUp.hpp"

indie::PowerUp::PowerUp(bool _isBonus, indie::GraphicalEngine &engine, const std::string &type, indie::AudioEngine &_audioEngine)
	:	isBonus(_isBonus), weakPtr(), node(engine, type), audioEngine(_audioEngine)
{
}

bool indie::PowerUp::interact(indie::Blast *)
{
	isAlive = false;
	return (false);
}

bool indie::PowerUp::interact(indie::Bomb *)
{
	isAlive = false;
	return (false);
}

bool indie::PowerUp::interact(indie::PowerUp *)
{
	isAlive = false;
	return false;
}

void indie::PowerUp::holdSmartPointer(std::shared_ptr<indie::PowerUp> &sharedPtr)
{
	if (sharedPtr.get() == this)
		weakPtr = sharedPtr;
}

bool indie::PowerUp::update(indie::MapData &)
{
	node.setRotation(node.getRotation() + 1);
	return isAlive;
}

void indie::PowerUp::display(std::pair<float, float> pos)
{
	node.setPosition(pos);
	node.setDisplay(true);
}

void indie::PowerUp::display()
{
	node.setDisplay(true);
}
