/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** Skull
*/

#include "Skull.hpp"
#include <iostream>

indie::Skull::Skull(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "Skull", _audioEngine)
{

	node.setPosition(pos);
}

bool indie::Skull::interact(indie::Player *player) {
	if (player) {
		player->hp = player->hp ? 1 : 0;
		player->speedLevel = 1;
		player->bombLevel = 1;
		player->fireLevel = 1;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerDownSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}