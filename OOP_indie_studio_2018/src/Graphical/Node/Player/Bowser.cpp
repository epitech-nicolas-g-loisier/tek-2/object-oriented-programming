/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Bowser
*/

#include <iostream>
#include "Bowser.hpp"
#include "GraphicalException.hpp"

indie::Bowser::Bowser(GraphicalEngine &engine)
{
	moveMesh = engine.scenemgr->getMesh(engine.findModelPath("Bowser/Walking.x").data());
	idleMesh = engine.scenemgr->getMesh(engine.findModelPath("Bowser/Idle.x").data());
	danceMesh = engine.scenemgr->getMesh(engine.findModelPath("Bowser/Dance.x").data());
	if (!moveMesh || !danceMesh || !idleMesh)
		throw ModelLoadingError("Bowser");

	texture[0] = engine.driver->getTexture(engine.findModelPath("Bowser/koopabody_alb.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("Bowser/koopaeye_alb.0.png").data());
	for (int i = 0; i < 2; ++i) {
		if (!texture[i])
			std::cerr << "Cannot find texture number: " << i << " for Bowser" << std::endl;
	}

	node = engine.scenemgr->addAnimatedMeshSceneNode(idleMesh);
	if (!node)
		throw NodeCreationError("Bowser");

	node->setRotation({0, -90, 0});
	node->setPosition({(float)(3), -0.5, (float)(3)});
	setTexture();
}

indie::Bowser::~Bowser()
{
	node->remove();
}

void indie::Bowser::setTexture()
{
	for (int i = 0; i < 2; ++i) {
		if (!texture[i])
			return;
	}
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->getMaterial(0).setTexture(0, texture[0]);
	node->getMaterial(1).setTexture(0, texture[1]);
}

void indie::Bowser::idle()
{
	if (state == "idle")
		return;

	state = "idle";
	node->setMesh(idleMesh);
	node->setCurrentFrame(std::rand() % 100 + 20);
	setTexture();
}

void indie::Bowser::move()
{
	if (state == "move")
		return;

	state = "move";
	node->setMesh(moveMesh);
	setTexture();
}

bool indie::Bowser::dance()
{
	if (danceTime == 0) {
		node->setMesh(danceMesh);
		setTexture();
	}
	danceTime++;
	if (danceTime > 150) {
		danceTime = 0;
		return false;
	}
	return true;
}

std::pair<float, float> indie::Bowser::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::Bowser::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::Bowser::getRotation()
{
	return node->getRotation().Y;
}

void indie::Bowser::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::Bowser::swapTexture(char)
{

}

void indie::Bowser::setPositionY(float y)
{
	node->setPosition({node->getPosition().X, y, node->getPosition().X});
}
