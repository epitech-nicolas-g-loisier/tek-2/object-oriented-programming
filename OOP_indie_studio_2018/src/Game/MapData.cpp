/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** MapData
*/

#include "MapData.hpp"
#include "GameException.hpp"
#include "PlayerNodeFactory.hpp"
#include "Ia.hpp"
#include "MapGenerator.hpp"

indie::MapData::MapData(indie::GraphicalEngine &_engine, AudioEngine &_audioEngine)
	: players(), entities(), entityMap(), engine(_engine), audioEngine(_audioEngine)
{}

void indie::MapData::generate(indie::GameOption &options)
{
	players.clear();
	entities.clear();
	entityMap.clear();
	std::pair<int, int> startPos[4] {{0,0},{14,8},{0,8},{14,0}};
	for (int i = 0; i < 4; i++) {
		std::shared_ptr<indie::Player> player;
		if (i < options.player)
			player = std::make_shared<indie::Player>(startPos[i], engine, options.character[i], audioEngine, options.playerInput[i]);
		else
			player = std::make_shared<indie::Ia>(startPos[i], engine, options.character[i], audioEngine);
		players.push_back(player);
		entities.push_back(player);
	}
	MapGenerator gen;
	if (options.mapChoice == "grid")
		gen.placeHardBlocksPattern();
	else
		gen.placeHardBlocks();
	gen.placeSoftBlocks();
	entityMap = convertMap(gen.getMap(), gen.getWidth(), gen.getHeight());
}

std::map<std::pair<unsigned char, unsigned char>, indie::EntityUnion> indie::MapData::convertMap(char *const*map, size_t width,
																								 size_t height)
{
	std::map<std::pair<unsigned char, unsigned char>, indie::EntityUnion> newMap;
	size_t indexCol = 1;
	size_t indexLine = 1;
	BlockFactory block(engine, audioEngine);

	while (indexCol < (height - 1)) {
		while (indexLine < (width - 1)) {
			if (map[indexCol][indexLine] != ' ')
				newMap.insert({{indexLine - 1, indexCol - 1},
							   block.createBlock(map[indexCol][indexLine], {indexLine -1, indexCol -1})});
			indexLine += 1;
		}
		indexCol += 1;
		indexLine = 1;
	}
	return newMap;
}