/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** SpeedDown
*/

#include "SpeedDown.hpp"
#include <iostream>

indie::SpeedDown::SpeedDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(false, engine, "SpeedDown", _audioEngine)
{

	node.setPosition(pos);
}

bool indie::SpeedDown::interact(indie::Player *player) {
	if (player) {
		if (player->speedLevel > 1)
			player->speedLevel--;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerDownSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}