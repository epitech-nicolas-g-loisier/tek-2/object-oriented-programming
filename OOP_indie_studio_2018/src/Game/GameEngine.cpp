/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameEngine
*/
#include "GameEngine.hpp"
#include "MapVisitors.hpp"
#include "BlockFactory.hpp"
#include <cmath>
#include <sstream>
#include <iomanip>

indie::GameEngine::GameEngine(GraphicalEngine &engine, GameOption _options)
		: audioEngine(), map(engine, audioEngine), options(std::move(_options)), graphicalEngine(engine),\
		gameTime(std::chrono::steady_clock::now() + std::chrono::minutes(2)),\
		sdIndex(135)
{
	for (int i = -1; i < 16; ++i) {
		blocks.push_back(std::make_shared<indie::Block>(false, std::pair<float, float>(i, -1), engine, audioEngine));
		blocks.push_back(std::make_shared<indie::Block>(false, std::pair<float, float>(i, 9), engine, audioEngine));
	}
	for (int i = 0; i < 9; ++i) {
		blocks.push_back(std::make_shared<indie::Block>(false, std::pair<float, float>(-1, i), engine, audioEngine));
		blocks.push_back(std::make_shared<indie::Block>(false, std::pair<float, float>(15, i), engine, audioEngine));
	}
}

void indie::GameEngine::generateMap()
{
	map.generate(options);
}

std::chrono::duration<long, std::ratio<1, 1000000000>> indie::GameEngine::displayTime() const
{
	auto time = gameTime - std::chrono::steady_clock::now();
	std::stringstream timeLeft;
	if (time > std::chrono::seconds(0)) {
		auto minutes = std::chrono::duration_cast<std::chrono::minutes>(time);
		auto seconds = std::chrono::duration_cast<std::chrono::seconds>(time - minutes);
		timeLeft << minutes.count() << ":" << std::setw(2) << std::setfill('0') << seconds.count();
	} else {
		timeLeft << "0:00";
		time = std::chrono::seconds(0);
	}
	graphicalEngine.drawText(timeLeft.str(), "medium", {255,255,255, 255}, {0,0,1920,100});
	return time;
}

void indie::GameEngine::spawnSuddenDeathBlock(std::chrono::duration<long, std::ratio<1, 1000000000>> timeLeft)
{
	auto ms = (std::chrono::duration_cast<std::chrono::milliseconds>(timeLeft)).count();
	std::shared_ptr<Blast> hitbox;

	while (sdIndex > ms * 0.003) {
		std::pair<int, int> pos((135 - sdIndex) % 15, (135 - sdIndex) / 15);
		if (hitbox == nullptr)
			hitbox = std::make_shared<Blast>(pos, graphicalEngine, false);
		if (map.entityMap.count(pos)) {
			std::weak_ptr<IGameObject> weakPtr;
			std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[pos]);
			weakPtr.lock()->interact(hitbox.get());
			map.entityMap.erase(pos);
		}
		map.entityMap[pos] = std::make_shared<Block>(false, pos, graphicalEngine, audioEngine);
		for (auto &&player : map.players) {
			auto _pos = player->getPosition();
			if (pos.first == lround(_pos.first) && pos.second == lround(_pos.second))
				hitbox->interact(player.get());
		}
		sdIndex--;
	}
}

int indie::GameEngine::mainLoop()
{
	auto timeLeft = displayTime();
	spawnSuddenDeathBlock(timeLeft);
	for (auto iterator = map.entities.begin(); iterator != map.entities.end();) {
		if (!iterator->get()->update(map)) {
			iterator = map.entities.erase(iterator);
		} else {
			iterator++;
		}
	}
	if (map.players.size() == 1) {
		auto result = std::find(options.character.begin(), options.character.end(), map.players.begin()->get()->name);
		return std::distance(options.character.begin(), result) + 1;
	} else if (map.players.empty())
		return -1;
	return 0;
}
