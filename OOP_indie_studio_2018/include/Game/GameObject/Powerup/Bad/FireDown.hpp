/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** FireDown
*/

#ifndef BOMBERMAN_FIREDOWN_HPP
#define BOMBERMAN_FIREDOWN_HPP

#include "PowerUp.hpp"

namespace indie {
	class FireDown : virtual public PowerUp {
	public:
		FireDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //BOMBERMAN_FIREDOWN_HPP
