/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameException
*/

#include "GameException.hpp"

indie::GameException::GameException(std::string message, std::string className)
	: _message(std::move(message)), _className(std::move(className))
{}

const char* indie::GameException::what() const noexcept
{
	return _message.data();
}