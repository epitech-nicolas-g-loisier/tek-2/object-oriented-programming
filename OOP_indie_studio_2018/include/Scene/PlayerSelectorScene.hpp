/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PlayerSelectorScene
*/

#ifndef PLAYERSELECTORSCENE_HPP
#define PLAYERSELECTORSCENE_HPP

#include <vector>
#include <map>
#include <chrono>
#include <array>
#include "IScene.hpp"
#include "PlayerNodeFactory.hpp"
#include "GraphicalEngine.hpp"
#include "GameOption.hpp"

namespace indie
{
class PlayerSelectorScene : public IScene
{
public:
	explicit PlayerSelectorScene(indie::GraphicalEngine &, indie::GameOption &);
	~PlayerSelectorScene() override = default;

	indie::IScene::sceneState draw() override;

private:
	indie::GraphicalEngine &_engine;
	std::vector<std::string> character;
	std::map<std::string, std::string> selectedCharacter;

	int nbrCharacter;
	std::pair<int, int> cursorPos[4] = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};
	std::chrono::steady_clock::time_point keyTime;
	bool lock[4] = {false, false, false, false};
	color_t color[4] = {{0, 0, 255, 80}, {255, 0, 0, 80}, {0, 255, 0, 80}, {255, 255, 0, 80}};

	void getInput();
	indie::GameOption &gameOption;
	std::vector<std::string> characterName;

	void drawSelected();
	const std::string &findRandomCharacter();
};
}

#endif //PLAYERSELECTORSCENE_HPP
