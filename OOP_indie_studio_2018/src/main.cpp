/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Main function
*/

#include <memory>
#include <string>
#include <iostream>
#include "Music.hpp"
#include "SceneFactory.hpp"
#include "GameOption.hpp"
#include "GraphicalException.hpp"
#include "GameException.hpp"

int main()
{
	indie::SceneFactory factory;
	indie::GraphicalEngine graphicalEngine;
	indie::GameOption gameOption;
	indie::IScene::sceneState chosenScene = indie::IScene::SPLASHSCREEN;
	indie::IScene::sceneState oldChosenScene = indie::IScene::EXIT;
	std::unique_ptr<indie::IScene> sceneManager;
	indie::Music music("music/Original.ogg");

	music.stop();
	gameOption.playerInput[0] = {"Z", "S", "Q", "D", "A", "E"};
	gameOption.playerInput[1] = {"up", "down", "left", "right", "rControl", "rShift"};
	gameOption.playerInput[2] = {"T", "G", "F", "H", "R", "Y"};
	gameOption.playerInput[3] = {"I", "K", "J", "L", "U", "O"};

	try {
		while (graphicalEngine.device->run() && graphicalEngine.driver) {
			if (chosenScene == indie::IScene::EXIT || graphicalEngine.receiver.keyPressed("delete")) {
				graphicalEngine.device->closeDevice();
				break;
			}
			if (chosenScene == indie::IScene::OPTION)
				music.switchMusic(gameOption.music);
			if (oldChosenScene != chosenScene) {
				sceneManager = factory.createScene(chosenScene, graphicalEngine, gameOption);
			}
			if (oldChosenScene == indie::IScene::SPLASHSCREEN && oldChosenScene != chosenScene)
				music.play();
			oldChosenScene = chosenScene;
			chosenScene = sceneManager->draw();
		}
	} catch (const indie::GameException &gameException) {
		std::cerr << "Game exception: " << gameException.what() << std::endl;
		return (84);
	} catch (const indie::GraphicalException &graphicalException) {
		std::cerr << "Graphical exception: " << graphicalException.what() << std::endl;
		return (84);
	}
	return (0);
}