/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** tests for 4011 component
*/

#include <criterion/criterion.h>
#include "../include/Component/C4011.hpp"
#include "../include/Component/Input.hpp"
#include "../include/Component/Output.hpp"
#include "../include/Exception.hpp"

Test(C4011, test_nand_component_true_true)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::FALSE, out.compute());
}

Test(C4011, test_nand_component_false_true)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::FALSE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4011, test_nand_component_true_false)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::FALSE, out.compute());
}

Test(C4011, test_nand_component_false_false)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::FALSE);
	nts::Input i2("i2", nts::FALSE);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4011, test_nand_component_true_undefined)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::UNDEFINED);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::UNDEFINED, out.compute());
}

Test(C4011, test_nand_component_undefined_false)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::UNDEFINED);
	nts::Input i2("i2", nts::FALSE);
	nts::Output out("out");

	nand.setLink(1, i1, 1);
	nand.setLink(2, i2, 1);
	nand.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4011, create_component_bad_pin)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		nand.setLink(0, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}

Test(C4011, create_component_relink_pin)
{
	nts::C4011 nand("nand");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		nand.setLink(1, i1, 1);
		nand.setLink(1, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Pin already linked.", error.what()) == 0);
	}
}

Test(C4011, compute_component_bad_pin)
{
	nts::C4011 nand("nand");

	try
	{
		nand.compute(0);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}