/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** INode
*/

#ifndef INODE_HPP
#define INODE_HPP

#include "AssetPath.hpp"

namespace indie {

class INode {
public:
	virtual ~INode() = default;
	virtual std::pair<float, float> getPosition() = 0;
	virtual void setPosition(const std::pair<float, float> &) = 0;
	virtual float getRotation() = 0;
	virtual void setRotation(float) = 0;
	virtual void swapTexture(char) = 0;

protected:
	virtual void setTexture() = 0;

};
}

#endif //INODE_HPP
