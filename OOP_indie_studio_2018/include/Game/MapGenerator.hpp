/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** MapGenerator
*/

#ifndef MAPGENERATOR_HPP_
#define MAPGENERATOR_HPP_

#include <iostream>
#include <cstring>
#include <cstdlib>

class MapGenerator {
	public:
		MapGenerator();
		~MapGenerator();

		void placeHardBlocks();
		void placeHardBlocksPattern();
		void placeSoftBlocks();
		void printMap() const;
		char *const*getMap() const;
		size_t getHeight() const;
		size_t getWidth() const;

	private:
		void placeLine(std::size_t, std::size_t);
		void fillEmptySpace();
		bool findSquareSize(std::size_t, std::size_t, std::size_t) const;
		bool isMapBorder(std::size_t, std::size_t) const;
		bool isMapCorner(std::size_t, std::size_t) const;

		char **_map;
		std::size_t _width;
		std::size_t _height;
};

#endif /* !MAPGENERATOR_HPP_ */
