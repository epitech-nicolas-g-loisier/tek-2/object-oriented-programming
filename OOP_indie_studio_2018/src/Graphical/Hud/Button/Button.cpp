
/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Button
*/

#include <utility>
#include <iostream>
#include "Button.hpp"

indie::Button::Button(std::pair<int, int> pos, std::string text, indie::GraphicalEngine &engine)
	: _text(std::move(text)), _pos(std::move(pos)), _engine(engine)
{
	texture["normal"] = "Button/Button.png";
	texture["hover"] = "Button/HoverButton.png";
	texture["clicked"] = "Button/ClickedButton.png";

	posMax = {_pos.first + 400, _pos.second + 100};
}

void indie::Button::draw()
{
	std::string a = checkState();
	_engine.drawTexture(texture[a], _pos);
	_engine.drawText(_text, "medium", {255, 255, 255, 255}, {_pos.first, _pos.second, posMax.first, posMax.second});
}

std::string indie::Button::checkState()
{
	auto mousePos = _engine.receiver.mousePos();

	click = false;
	if (mousePos.first > _pos.first && mousePos.first < posMax.first && mousePos.second > _pos.second && mousePos.second < posMax.second) {
		if (_engine.receiver.leftMouseReleased() && clicked) {
			click = true;
			clicked = false;
			return "clicked";
		} else if (_engine.receiver.leftMousePressed()) {
			clicked = true;
			return "clicked";
		} else
			return "hover";
	} else
		return "normal";
}

bool indie::Button::isClicked()
{
	bool tmp = click;
	click = false;
	return tmp;
}
