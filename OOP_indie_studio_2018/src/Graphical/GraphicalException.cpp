/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** GraphicalException
*/

#include <utility>
#include <iostream>
#include "GraphicalException.hpp"

indie::GraphicalException::GraphicalException(const std::string& element, const std::string& className)
	: _message("Unable to create: " + element + ", in " + className)
{}

const char *indie::GraphicalException::what() const noexcept
{
	return _message.data();
}

indie::TextureLoadingError::TextureLoadingError(const std::string& className, const std::string& texture)
	: GraphicalException("Texture: " + texture, className)
{}

indie::ModelLoadingError::ModelLoadingError(const std::string& className)
	: GraphicalException("Model", className)
{}

indie::FontLoadingError::FontLoadingError(const std::string& className, const std::string& fontType)
	: GraphicalException("Font " + fontType, className)
{}

indie::SoundLoadingError::SoundLoadingError(const std::string& className)
	: GraphicalException("Sound", className)
{}

indie::NodeCreationError::NodeCreationError(const std::string &className)
	: GraphicalException("Node", className)
{}
