/*
** EPITECH PROJECT, 2019
** OOP_nanotekspice
** File description:
** Factory
*/
#include "Factory.hpp"
#include "Exception.hpp"
#include "Input.hpp"
#include "Output.hpp"
#include "True.hpp"
#include "False.hpp"
#include "Clock.hpp"
#include "C4001.hpp"
#include "C4011.hpp"
#include "C4013.hpp"
#include "C4030.hpp"
#include "C4069.hpp"
#include "C4071.hpp"
#include "C4081.hpp"

#include <iostream>

nts::Factory::Factory()
{
	_ctors["input"] = &nts::Factory::createInput;
	_ctors["output"] = &nts::Factory::createOutput;
	_ctors["true"] = &nts::Factory::createTrue;
	_ctors["false"] = &nts::Factory::createFalse;
	_ctors["clock"] = &nts::Factory::createClock;
	_ctors["4001"] = &nts::Factory::create4001;
	_ctors["4011"] = &nts::Factory::create4011;
	_ctors["4013"] = &nts::Factory::create4013;
	_ctors["4030"] = &nts::Factory::create4030;
	_ctors["4069"] = &nts::Factory::create4069;
	_ctors["4071"] = &nts::Factory::create4071;
	_ctors["4081"] = &nts::Factory::create4081;
}

std::unique_ptr<nts::IComponent> nts::Factory::createComponent(const std::string &type, const std::string &value)
{
	if (_ctors.count(type) != 0)
		return ((this->*_ctors[type])(value));
	throw NanoException("Unknown type");
}

std::unique_ptr<nts::IComponent> nts::Factory::createInput(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new Input(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::createOutput(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new Output(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::createTrue(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new True(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::createFalse(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new False(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::createClock(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new Clock(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4001(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4001(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4011(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4011(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4013(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4013(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4030(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4030(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4069(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4069(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4071(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4071(value)));
};

std::unique_ptr<nts::IComponent> nts::Factory::create4081(const std::string &value) const noexcept
{
	return (std::unique_ptr<nts::IComponent>(new C4081(value)));
};
