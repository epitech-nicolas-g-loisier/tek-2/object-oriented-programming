/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GamePause
*/

#ifndef GAMEPAUSE_HPP
#define GAMEPAUSE_HPP

#include <chrono>
#include <vector>
#include <array>
#include <memory>
#include "GraphicalEngine.hpp"
#include "Button.hpp"

namespace indie
{
class GamePause
{
public:
	explicit GamePause(indie::GraphicalEngine &engine);
	~GamePause() = default;

	enum pauseState{
		PAUSE,
		RESUME,
		SAVE,
		EXIT
	};

	pauseState display();
	void initPause();
	std::chrono::steady_clock::time_point stopPause();


private:
	indie::GraphicalEngine &_engine;
	std::vector<indie::Button> button;
	std::array<std::string, 4> choices;

	indie::rect_t choice = {0, 0, 720, 760};
	std::chrono::steady_clock::time_point beginTime;
};
}

#endif //GAMEPAUSE_HPP
