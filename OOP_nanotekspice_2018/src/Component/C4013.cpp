/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component 4013 Method
*/

#include "C4013.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>
#include <array>
#include <algorithm>

nts::C4013::C4013(const std::string name)
	: _name(name), _vpin(UNDEFINED), _pcl(UNDEFINED)
{
	_output.emplace(1, std::vector<size_t>{3, 4, 5, 6});
	_output.emplace(2, std::vector<size_t>{3, 4, 5, 6});
	_output.emplace(12, std::vector<size_t>{8, 9, 10, 11});
	_output.emplace(13, std::vector<size_t>{8, 9, 10, 11});
}

nts::C4013::~C4013()
{
	for(auto o : _input)
	{
		delete o.second;
	}
}

nts::Tristate nts::C4013::compute(std::size_t pin)
{
	if (_input.count(pin) != 0)
		return _input[pin]->prev.compute(_input[pin]->pinNumber);
	else if (_output.count(pin) != 0) {
		std::vector<Tristate> values;
		for(auto&&x : _output[pin])
			values.push_back(compute(x));
		if (pin % 11 == 2)
			std::reverse(values.begin(), values.end());
		if (values[1] == TRUE && values[3] == TRUE)
			_vpin = UNDEFINED;
		else if (values[3] == TRUE)
			_vpin = TRUE;
		else if (values[1] == TRUE)
			_vpin = FALSE;
		else if (_pcl == TRUE && values[1] != UNDEFINED)
			_vpin = values[1];
		_pcl = values[0];
		if (pin % 11 == 2) {
			if (_vpin == UNDEFINED)
				return UNDEFINED;
			return (_vpin == TRUE ? FALSE : TRUE);
		}
		return _vpin;
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4013::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	std::array<size_t, 8> input = {3, 4, 5, 6, 8, 9, 10, 11};
	std::array<size_t, 4> output = {1, 2, 12, 13};

	if (std::find(input.begin(), input.end(), pin) != input.end()) {
		if (_input.count(pin) == 0) {
			_input.emplace(pin, new InputPin(other, otherPin));
		} else {
			throw NanoException("Pin already linked.");
		}
	} else if (std::find(output.begin(), output.end(), pin) != output.end()) {
		other.setLink(otherPin, *this, pin);
	} else {
		throw NanoException("Requested pin does not exist.");
	}
}

void nts::C4013::dump() const
{
	std::cout << _name << std::endl;
}

const std::string &nts::C4013::getName() const
{
	return (_name);
}