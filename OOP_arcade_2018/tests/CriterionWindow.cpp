/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** CriterionWindow implementation
*/
#include <criterion/criterion.h>
#include "CriterionWindow.hpp"

void CriterionWindow::drawLine(const Arcade::Line &line)
{
	cr_assert(line.pos.first >= 0 && line.pos.first < 80);
	cr_assert(line.pos.second >= 0 && line.pos.second < 40);
	std::pair<float, float> dest = line.pos;
	if (line.dir > 1)
		dest.first = line.dir == 0 ? dest.first - line.length : dest.first + line.length;
	else
		dest.second = line.dir == 2 ? dest.second - line.length : dest.second + line.length;
	cr_assert(dest.first >= 0 && dest.first < 80);
	cr_assert(dest.second >= 0 && dest.second < 40);
};

void CriterionWindow::drawRect(const Arcade::Rect &rect)
{
	cr_assert(rect.pos.first >= 0 && rect.pos.first < 80);
	cr_assert(rect.pos.second >= 0 && rect.pos.second < 40);
	std::pair<float, float> end = rect.pos;
	end.first += rect.size.first;
	end.second += rect.size.second;
	cr_assert(end.first >= 0 && end.first < 80);
	cr_assert(end.second >= 0 && end.second < 40);
};

void CriterionWindow::drawPair(const Arcade::Pair &pair)
{
	cr_assert(pair.pos.first >= 0 && pair.pos.first < 80 - pair.value.first.size());
	cr_assert(pair.pos.second >= 0 && pair.pos.second < 39);
};

void CriterionWindow::update()
{
};

std::vector<std::string> CriterionWindow::getInput()
{
	std::vector<std::string> input;
	return (input);
};