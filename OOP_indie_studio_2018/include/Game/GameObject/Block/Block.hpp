/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Block
*/
#ifndef BOMBERMAN_BLOCK_HPP
#define BOMBERMAN_BLOCK_HPP

#include "AudioEngine.hpp"
#include "IGameObject.hpp"
#include "PowerUpFactory.hpp"
#include "BlockNode.hpp"

namespace indie {

	class PowerUpFactory;

	class Block : public IGameObject {
	public:
		Block(bool, std::pair<float, float>, GraphicalEngine&, indie::AudioEngine&);
		Block(indie::PowerUpFactory &, bool, std::pair<float, float>, GraphicalEngine&, indie::AudioEngine&);
		~Block();
		bool interact(indie::Player*) override;
		bool interact(indie::Bomb*) override;
		bool interact(indie::Blast*) override;
		bool interact(indie::PowerUp*) override;

		const bool isDestroyable;

	private:
		std::shared_ptr<indie::PowerUp> powerUp;
		BlockNode _node;
		indie::AudioEngine &audioEngine;
	};
}

#endif //BOMBERMAN_BLOCK_HPP