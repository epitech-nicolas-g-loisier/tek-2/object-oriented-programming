/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Entity interface definition
*/
#ifndef ENTITY_HPP_
#define ENTITY_HPP_

#include "IGameObject.hpp"

namespace indie {

	class Player;
	class Bomb;
	struct MapData;

	class IEntity : public IGameObject {
	public:
		virtual bool update(indie::MapData &) = 0;
		virtual bool interact(indie::Player*) = 0;
		virtual bool interact(indie::Bomb*) = 0;
		virtual bool interact(indie::Blast*) = 0;
	};
}

#include "Player.hpp"
#include "Bomb.hpp"
#include "Blast.hpp"
#include "MapData.hpp"

#endif // ENTITY_HPP_