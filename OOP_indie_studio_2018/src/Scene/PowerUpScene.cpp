/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpScene
*/

#include "PowerUpScene.hpp"

indie::PowerUpScene::PowerUpScene(indie::GraphicalEngine &engine)
	: _engine(engine)
{}

indie::IScene::sceneState indie::PowerUpScene::draw()
{
	_engine.beginScene({120, 102, 136, 255});
	_engine.drawTexture("Screen/PowerUpScreen.png", {0, 0});
	_engine.drawText("press space", "medium", {255, 255, 255, 255}, {1500, 1030, 1920, 1080});

	_engine.endScene();
	if (_engine.receiver.keyDown("space"))
		return indie::IScene::GAME;
	return indie::IScene::POWERUP;
}