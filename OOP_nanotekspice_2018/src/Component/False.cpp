/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** False Method
*/

#include "False.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>

nts::False::False(const std::string name)
	: _name(name)
{
}

nts::Tristate nts::False::compute(std::size_t pin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	return _value;
}

void nts::False::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	other.setLink(otherPin, *this, pin);
}

void nts::False::dump() const
{
	std::cout << _name << "=" << _value << std::endl;
}

const std::string &nts::False::getName() const
{
	return (_name);
}

void nts::False::setValue(nts::Tristate)
{
	throw NanoException("Cannot set value of 'false' component");
}