/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Blast
*/
#include "Blast.hpp"

indie::Blast::Blast(const std::pair<unsigned char, unsigned char>&_pos, GraphicalEngine &engine, bool piercing)
	: pos(_pos), deathTime(std::chrono::steady_clock::now() + std::chrono::milliseconds(500)), powerUp(), _node(engine, piercing)
{
	_node.setPosition(_pos);
}

bool indie::Blast::update(indie::MapData &map)
{
	bool isAlive = std::chrono::steady_clock::now() < deathTime;

	if (!isAlive) {
		if (powerUp != nullptr) {
			powerUp->display();
			map.entityMap[pos] = powerUp;
			map.entities.push_back(powerUp);

		} else {
			map.entityMap.erase(pos);
		}
	}
	_node.update();
	return (isAlive);
}

void indie::Blast::burnOut()
{
	deathTime -= std::chrono::seconds(1);
}

void indie::Blast::holdPowerUp(const std::shared_ptr<indie::PowerUp> &powerup)
{
	powerUp = powerup;
}

bool indie::Blast::interact(indie::Player *player)
{
	if (player)
		player->takeDamage();
	return (true);
}

bool indie::Blast::interact(indie::Blast *blast)
{
	if (blast) {
		blast->holdPowerUp(powerUp);
		return (false);
	}
	return (true);
}

bool indie::Blast::interact(indie::Bomb *bomb)
{
	if (bomb)
		bomb->detonate();
	return (true);
}

unsigned char indie::Blast::getTimeToLive() const
{
	long timer = (deathTime - std::chrono::steady_clock::now()).count();
	return (timer > 255 ? 255 : timer);
}

bool indie::Blast::interact(indie::PowerUp *)
{
	return false;
}
