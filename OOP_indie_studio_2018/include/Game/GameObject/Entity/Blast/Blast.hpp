/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Blast
*/
#ifndef BOMBERMAN_BLAST_HPP
#define BOMBERMAN_BLAST_HPP

#include "IEntity.hpp"
#include "PowerUp.hpp"
#include "BlastNode.hpp"
#include <chrono>
#include <memory>

namespace indie {
	class Blast : public IEntity {
	public:
		Blast(const std::pair<unsigned char, unsigned char>&, GraphicalEngine &engine, bool);
		bool update(indie::MapData &) override;
		bool interact(indie::Player*) override;
		bool interact(indie::Bomb*) override;
		bool interact(indie::Blast*) override;
		bool interact(indie::PowerUp*) override;
		void holdPowerUp(const std::shared_ptr<indie::PowerUp>&);
		void burnOut();
		unsigned char getTimeToLive() const;

	private:
		std::pair<unsigned char, unsigned char> pos;
		std::chrono::steady_clock::time_point deathTime;
		std::shared_ptr<indie::PowerUp> powerUp;

		BlastNode _node;
	};
}

#endif //BOMBERMAN_BLAST_HPP