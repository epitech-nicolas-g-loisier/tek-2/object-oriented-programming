/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** FireDown
*/
#include "FireDown.hpp"
#include <iostream>

indie::FireDown::FireDown(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(false, engine, "FireDown", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::FireDown::interact(indie::Player *player) {
	if (player) {
		if (player->fireLevel > 1)
			player->fireLevel--;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerDownSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}