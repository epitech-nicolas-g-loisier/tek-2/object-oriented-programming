/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** MapData
*/
#include <variant>
#include <memory>
#include <map>
#include <vector>
#include <list>

#ifndef INDIE_STUDIO_MAPDATA_HPP
#define INDIE_STUDIO_MAPDATA_HPP

#include "IEntity.hpp"
#include "Block.hpp"
#include "PowerUp.hpp"
#include "GameOption.hpp"

namespace indie {

	class Block;
	class PowerUp;
	class Bomb;
	class Blast;
	class Player;
	class IEntity;

	typedef std::variant<std::shared_ptr<indie::PowerUp>, std::shared_ptr<indie::Block>, std::shared_ptr<indie::Blast>, std::shared_ptr<indie::Bomb>> EntityUnion;

	class MapData {
	public:
		MapData(indie::GraphicalEngine &engine, AudioEngine &audioEngine);
		void generate(GameOption &options);

		std::list <std::shared_ptr<indie::Player>> players;
		std::list <std::shared_ptr<indie::IEntity>> entities;
		std::map <std::pair<unsigned char, unsigned char>, indie::EntityUnion> entityMap;

	private:
		std::map<std::pair<unsigned char, unsigned char>, indie::EntityUnion> convertMap(char *const*map,
			size_t width, size_t height);
		GraphicalEngine &engine;
		AudioEngine &audioEngine;
	};
}

#endif //INDIE_STUDIO_MAPDATA_HPP
