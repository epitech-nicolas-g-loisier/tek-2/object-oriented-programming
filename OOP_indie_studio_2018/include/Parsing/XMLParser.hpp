/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** XMLParser
*/

#ifndef XMLPARSER_HPP_
#define XMLPARSER_HPP_

#include "XMLParser/Node.hpp"
#include <istream>

namespace XML
{

class XMLParser {
	public:
		XMLParser(std::shared_ptr<std::istream> is);
		~XMLParser() = default;

		Node begin() const;
		Node end() const;

		Node find(const std::string &str) const;
		std::vector<Node> findAll(const std::string &str) const;
		SimpleType get(const std::string &str) const;
		std::vector<SimpleType> getAll(const std::string &str) const;

	private:
		std::shared_ptr<std::istream> is;
		Node _begin;
};

}

#endif /* !XMLPARSER_HPP_ */
