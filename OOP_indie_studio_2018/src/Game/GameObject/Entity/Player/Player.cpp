/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Player class definition
*/

#include "Player.hpp"
#include "PlayerNodeFactory.hpp"
#include "MapVisitors.hpp"
#include <random>
#include <cmath>
#include <iostream>

indie::Player::Player(std::pair<unsigned char, unsigned char> _pos, GraphicalEngine& _engine, const std::string &character, indie::AudioEngine &_audioEngine, std::array<std::string, 6> _binding)
		:   hp(1), fireLevel(1), bombLevel(1), speedLevel(3), hasKick(false), hasPunch(false), hasLineBomb(false), moving(false),
	bombType(NORMAL), pos(_pos), stunTime(std::chrono::seconds(0)), iFrames(std::chrono::seconds(0)),
	powerUpTaken(), receiver(_engine.receiver), bombHandler(_engine, _audioEngine), binding(std::move(_binding)),
	previousFrameTime(std::chrono::seconds(0))
{
	node = _engine.playerNodeFactory.createPlayerNode(character, _engine);
	node->setPosition(_pos);
	name = character;
}

bool indie::Player::update(indie::MapData &map)
{
	auto currTime = std::chrono::steady_clock::now();
	if (previousFrameTime.time_since_epoch() < std::chrono::seconds(1))
		previousFrameTime = currTime;
	if (hp > 0) {
		auto timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(currTime - previousFrameTime).count();
		std::pair<float, float> velocity({0,0});
		if (receiver.keyPressed(binding[3])) {
			velocity.first += 0.6875 * speedLevel * timeDiff / 1000;
			node->move();
			node->setRotation(270);
		} else if (receiver.keyPressed(binding[2])) {
			velocity.first -= 0.6875 * speedLevel * timeDiff / 1000;
			node->move();
			node->setRotation(90);
		} else if (receiver.keyPressed(binding[0])) {
			velocity.second += 0.6875 * speedLevel * timeDiff / 1000;
			node->move();
			node->setRotation(180);
		} else if (receiver.keyPressed(binding[1])) {
			velocity.second -= 0.6875 * speedLevel * timeDiff / 1000;
			node->move();
			node->setRotation(0);
		} else
			node->idle();
		if (map.entityMap.count(pos)) {
			std::weak_ptr<IGameObject> weakPtr;
			std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[pos]);
			if (!weakPtr.lock()->interact(this))
				map.entityMap.erase(pos);
		}
		if (velocity != std::pair<float,float>({0, 0}))
			move(velocity, map);
		node->setPosition(pos);
		if (receiver.keyPressed(binding[5]))
			doLineBomb(node->getRotation(), map);
		if (receiver.keyPressed(binding[4]))
			putBomb(map);
	}
	if (hp <= 0) {
		std::knuth_b rand = std::knuth_b(std::random_device()());
		for (auto &powerup : powerUpTaken) {
			if (map.entityMap.size() >= 100)
				break;
			std::pair<unsigned char, unsigned char> _pos(rand(), rand());
			while (map.entityMap.count(_pos) > 0)
				_pos = {rand(), rand()};
			powerup->display(_pos);
			map.entityMap[_pos] = powerup;
		}
		for (auto it = map.players.begin(); it != map.players.end(); it++)
			if (it->get() == this) {
				map.players.erase(it);
				break;
			}
	}
	previousFrameTime = currTime;
	return (hp > 0);
}

void indie::Player::move(std::pair<float, float> velocity, MapData &map)
{
	std::pair<float, float> dest = {velocity.first + pos.first, velocity.second + pos.second};
	if (dest.first < 0) {
		pos.first = 0;
		return;
	}
	if (dest.second < 0) {
		pos.second = 0;
		return;
	}
	if (dest.first > 14) {
		pos.first = 14;
		return;
	}
	if (dest.second > 8) {
		pos.second = 8;
		return;
	}
	moving = true;
	std::pair<int, int> nearestDest = {lround(dest.first), lround(dest.second)};
	std::cout << nearestDest.first << "/" << nearestDest.second << std::endl;
	if (nearestDest.first != lround(pos.first) || nearestDest.second != lround(pos.second)) {
		if (map.entityMap.count(nearestDest)) {
			std::cout << "Colision" << std::endl;
			std::weak_ptr<IGameObject> weakPtr;
			std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[nearestDest]);
			if (!weakPtr.lock()->interact(this))
				map.entityMap.erase(nearestDest);
		}
	}
	if (moving) {
		pos = dest;
		moving = false;
	}
}

void indie::Player::doLineBomb(int rotation, indie::MapData &map)
{
	std::pair<int, int> roundedPos = {std::rint(pos.first), std::rint(pos.second)};
	std::pair<int, int> posIterator = {(rotation % 180 != 0) * (rotation == 90 ? -1 : 1), (rotation % 180 == 0) * (rotation == 0 ? -1 : 1)};

	for (int bombLeft = bombLevel; bombLeft; bombLeft--) {
		if (!bombHandler.putBomb(this, roundedPos, map))
			break;
		roundedPos.first += posIterator.first;
		roundedPos.second += posIterator.second;
		if (roundedPos.first < 0 || roundedPos.first > 13 || roundedPos.second < 0 || roundedPos.second > 8)
			break;
	}
}

void indie::Player::takeDamage()
{
	std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
	if (hp > 0 && iFrames < now) {
		hp--;
		iFrames = now + std::chrono::milliseconds(750);
	}
}

void indie::Player::stun()
{
	if (!hasPunch)
		stunTime = std::chrono::steady_clock::now() + std::chrono::milliseconds(500);
}

void indie::Player::pickPowerUp(const std::shared_ptr<indie::PowerUp> &powerup)
{
	powerUpTaken.push_back(powerup);
}

bool indie::Player::interact(indie::Player *)
{
	return (true);
}

bool indie::Player::interact(indie::Blast *blast)
{
	if (blast)
		takeDamage();
	return (true);
}

bool indie::Player::interact(indie::Bomb *bomb)
{
	if (bomb)
		stun();
	return (true);
}

bool indie::Player::interact(indie::PowerUp *)
{
	return false;
}

const std::pair<float, float> &indie::Player::getPosition()
{
	return pos;
}

bool indie::Player::putBomb(MapData &map)
{
	return bombHandler.putBomb(this, pos, map);
}

indie::BombHandler::BombHandler(indie::GraphicalEngine &graphicalEngine, indie::AudioEngine &_audioEngine)
	: engine(graphicalEngine), bombFactory(graphicalEngine, _audioEngine), audioEngine(_audioEngine)
{}

bool indie::BombHandler::putBomb(indie::Player *player, std::pair<float, float> pos, MapData &map)
{
	pos = {lround(pos.first), lround(pos.second)};
	auto bomb = bombFactory.spawnBomb(player->bombType, player->fireLevel, pos);

	for (auto it = droppedBomb.begin(); it != droppedBomb.end();) {
		if (it->expired())
			it = droppedBomb.erase(it);
		else
			it++;
	}
	if (bomb != nullptr && droppedBomb.size() < player->bombLevel) {
		if (map.entityMap.count(pos) != 0) {
			std::weak_ptr<IGameObject> weakPtr;
			std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[pos]);
			if (weakPtr.lock()->interact(bomb.get()))
				return (false);
		}
		droppedBomb.push_back(bomb);
		map.entityMap[pos] = bomb;
		map.entities.push_back(bomb);
	}
	return (bomb != nullptr);
}