/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Circuit Class
*/

#ifndef CIRCUIT_HPP_
	#define CIRCUIT_HPP_

#include <vector>
#include <utility>
#include <memory>
#include "IComponent.hpp"
#include "FileParser.hpp"
#include "Factory.hpp"

namespace nts
{
	class Circuit {
	public:
		Circuit(const std::string &);
		~Circuit();
		void createInput();
		void createOutput();
		void createComponent();
		void setInputValue(char **);
		void execute();
		void linkComponents();

	private:
		FileParser _parser;
		Factory _factory;

		std::vector<std::unique_ptr<IComponent>> _input;
		std::vector<std::unique_ptr<IComponent>> _output;
		std::vector<std::unique_ptr<IComponent>> _component;

		std::pair<std::string, std::string> getComponentFromString(const std::string &);
		std::pair<std::string, nts::Tristate> getValueFromString(const std::string &);
		void getPairFromLinkStr(const std::string &string, std::pair<std::string, size_t> &link);
		IComponent *getComponentByName(std::string &);
	};
}

#endif /* !CIRCUIT_HPP_ */
