/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** PowerUp class
*/

#include <memory>

#ifndef POWERUP_HPP_
#define POWERUP_HPP_

#include "IEntity.hpp"
#include "PowerUpNode.hpp"
#include "AudioEngine.hpp"

namespace indie {
	class PowerUp : public IEntity {
	public:
		PowerUp(bool, indie::GraphicalEngine &engine, const std::string &type, indie::AudioEngine&);
		bool update(indie::MapData &);
		bool interact(indie::Bomb*) final;
		bool interact(indie::Blast*) final;
		bool interact(indie::PowerUp*) final;
		void holdSmartPointer(std::shared_ptr<PowerUp>&);
		void display(std::pair<float, float>);
		void display();

		const bool isBonus;

	protected:
		std::weak_ptr<indie::PowerUp> weakPtr;
		indie::PowerUpNode node;
		bool isAlive = true;
		indie::AudioEngine &audioEngine;
	};
}

#endif // POWERUP_HPP_