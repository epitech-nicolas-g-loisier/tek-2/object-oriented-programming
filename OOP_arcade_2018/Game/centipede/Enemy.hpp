/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Centipede
*/
#include <utility>
#include <memory>
#include <vector>
#include <chrono>

#ifndef CENTIPEDE_ENEMY_
	#define CENTIPEDE_ENEMY_

namespace Centipede
{
	struct Obstacle
	{
		std::pair<int, int> pos;
		int hp;
	};

	class Enemy
	{
	public:
		Enemy(int memberLeft = 8);
		Enemy(const Enemy &);
		~Enemy(){};
		virtual int move(std::vector<std::unique_ptr<Centipede::Enemy>> &, std::vector<Centipede::Obstacle> &, std::pair<int, int> &);
		const std::vector<std::pair<int, int>> &getBody() const;
		bool getDir() const;
		virtual bool toDelete();
	protected:
		std::vector<std::pair<int, int>> _body;
		int _memberLeft;
		bool _dir;
		std::chrono::nanoseconds start;

		bool checkColision(std::vector<std::unique_ptr<Centipede::Enemy>> &entityList, std::vector<Centipede::Obstacle> &obstacleList, int xOffset);
	};

	class QuickEnemy : public Enemy
	{
	public:
		int move(std::vector<std::unique_ptr<Centipede::Enemy>> &, std::vector<Centipede::Obstacle> &, std::pair<int, int> &) override;
		bool toDelete() override;
	};
}

#endif // CENTIPEDE_ENEMY_