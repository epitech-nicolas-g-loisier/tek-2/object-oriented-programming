/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Player class
*/
#include <chrono>
#include <vector>
#include <memory>

#ifndef BOMBERMAN_PLAYER_HPP
#define BOMBERMAN_PLAYER_HPP

#include "IEntity.hpp"
#include "PowerUp.hpp"
#include "IPlayerNode.hpp"
#include "GraphicalEngine.hpp"
#include "BombType.hpp"
#include "BombFactory.hpp"
#include "AudioEngine.hpp"

namespace indie {

	class PowerUp;

	class BombHandler {
	public:
		explicit BombHandler(GraphicalEngine &, indie::AudioEngine &_audioEngine);
		bool putBomb(indie::Player *player, std::pair<float, float> pos, MapData &map);

	private:
		GraphicalEngine &engine;
		BombFactory bombFactory;
		std::vector<std::weak_ptr<Bomb>> droppedBomb;
		indie::AudioEngine &audioEngine;
	};

	class Player : public IEntity {
	public:
		Player(std::pair<unsigned char, unsigned char>, GraphicalEngine&, const std::string&, indie::AudioEngine &_audioEngine, std::array<std::string, 6> = {"null","null","null","null","null","null"});
		~Player() = default;

		bool update(indie::MapData &) override;
		bool interact(indie::Player*) override final;
		bool interact(indie::Bomb*) override final;
		bool interact(indie::Blast*) override final;
		bool interact(indie::PowerUp*) override final;
		void takeDamage();
		void stun();
		void pickPowerUp(const std::shared_ptr<indie::PowerUp>&);
		const std::pair<float, float> &getPosition();

		char hp;
		unsigned char fireLevel;
		unsigned char bombLevel;
		unsigned char speedLevel;
		bool hasKick;
		bool hasPunch;
		bool hasLineBomb;
		bool moving;
		BombType bombType;
		std::string name;

	protected:
		bool putBomb(indie::MapData &);
		void move(std::pair<float, float>, MapData &);
		void doLineBomb(int rotation, MapData &);

		std::pair<float, float> pos;
		std::chrono::steady_clock::time_point stunTime;
		std::chrono::steady_clock::time_point iFrames;
		std::vector<std::shared_ptr<indie::PowerUp>> powerUpTaken;
		std::unique_ptr<IPlayerNode> node;

	private:
		EventReceiver &receiver;
		BombHandler bombHandler;
		std::array<std::string, 6> binding;
		std::chrono::steady_clock::time_point previousFrameTime;
	};
}

#endif //BOMBERMAN_PLAYER_HPP