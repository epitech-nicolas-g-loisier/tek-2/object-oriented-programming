/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Punch
*/

#include "Punch.hpp"
#include <iostream>

indie::Punch::Punch(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "Punch", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::Punch::interact(indie::Player *player) {
	if (player) {
		player->hasPunch = true;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}