/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PierceBombPowerup
*/

#include "PierceBombPowerup.hpp"

indie::PierceBombPowerup::PierceBombPowerup(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "PierceBomb", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::PierceBombPowerup::interact(indie::Player *player)
{
	if (player) {
		player->bombType = PIERCE;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}