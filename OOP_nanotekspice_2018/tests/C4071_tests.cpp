/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** tests for 4071 component
*/

#include <criterion/criterion.h>
#include "C4071.hpp"
#include "Input.hpp"
#include "Output.hpp"
#include "Exception.hpp"

Test(C4071, test_c_or_component_true_true)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4071, test_c_or_component_false_true)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::FALSE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4071, test_c_or_component_true_false)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::TRUE);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4071, test_c_or_component_false_false)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::FALSE);
	nts::Input i2("i2", nts::FALSE);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::FALSE, out.compute());
}

Test(C4071, test_c_or_component_true_undefined)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::TRUE);
	nts::Input i2("i2", nts::UNDEFINED);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::TRUE, out.compute());
}

Test(C4071, test_c_or_component_undefined_false)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::UNDEFINED);
	nts::Input i2("i2", nts::FALSE);
	nts::Output out("out");

	c_or.setLink(1, i1, 1);
	c_or.setLink(2, i2, 1);
	c_or.setLink(3, out, 1);

	cr_assert_eq(nts::UNDEFINED, out.compute());
}

Test(C4071, create_component_bad_pin)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		c_or.setLink(0, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}

Test(C4071, create_component_relink_pin)
{
	nts::C4071 c_or("c_or");
	nts::Input i1("i1", nts::UNDEFINED);

	try
	{
		c_or.setLink(1, i1, 1);
		c_or.setLink(1, i1, 1);
		cr_assert(false);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Pin already linked.", error.what()) == 0);
	}
}

Test(C4071, compute_component_bad_pin)
{
	nts::C4071 c_or("c_or");

	try
	{
		c_or.compute(0);
	}
	catch (NanoException const &error)
	{
		cr_assert(strcmp("Requested pin does not exist.", error.what()) == 0);
	}
}