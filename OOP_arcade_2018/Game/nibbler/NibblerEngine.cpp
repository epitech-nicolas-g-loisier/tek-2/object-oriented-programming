/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Nibbler Engine
*/
#include <memory>
#include "Nibbler.hpp"

__attribute__ ((constructor))
void wrapper_ctor()
{
}

__attribute__ ((destructor))
void wrapper_dtor()
{
}

extern "C" std::unique_ptr<IEngine> getIEngine()
{
	return (std::make_unique<nibbler::Nibbler>());
}