/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** main
*/
#include "Input.hpp"
#include "Exception.hpp"
#include "Factory.hpp"
#include "Prompt.hpp"
#include "Circuit.hpp"
#include <iostream>
#include <vector>

int main(int argc, char **argv)
{
	if (argc == 1) {
		return (84);
	} else {
		nts::Circuit circuit(argv[1]);
		circuit.createInput();
		circuit.createOutput();
		circuit.createComponent();
		circuit.linkComponents();
		circuit.setInputValue(&argv[2]);
		circuit.execute();
	}
}