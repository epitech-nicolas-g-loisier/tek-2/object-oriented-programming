/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Component True
*/

#ifndef TRUE_HPP_
	#define TRUE_HPP_

#include "IInput.hpp"
#include "Pin.hpp"

namespace nts {

	class True : public IInput
	{
	public:
		True(const std::string name);
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;
		void setValue(nts::Tristate);
		void update() {};

	private:
		std::string _name;
		Tristate _value = TRUE;
	};
}

#endif /* !TRUE_HPP_ */
