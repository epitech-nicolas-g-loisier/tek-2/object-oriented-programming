/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PierceBombPowerup
*/

#ifndef INDIE_STUDIO_PIERCEBOMBPOWERUP_HPP
#define INDIE_STUDIO_PIERCEBOMBPOWERUP_HPP

#include "PowerUp.hpp"

namespace indie {
	class PierceBombPowerup : virtual public PowerUp {
	public:
		PierceBombPowerup(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //INDIE_STUDIO_PIERCEBOMBPOWERUP_HPP
