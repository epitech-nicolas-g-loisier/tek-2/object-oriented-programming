/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** BlockNode
*/

#include <iostream>
#include "GraphicalException.hpp"
#include "BlockNode.hpp"

indie::BlockNode::BlockNode(bool destroyable, GraphicalEngine &engine)
	: _destroyable(destroyable)
{
	texture[0] = engine.driver->getTexture(engine.findModelPath("brickBlock/brick.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("StrongBlock/block.png").data());
	if (!texture[0] || !texture[1])
		std::cerr << "Cannot find texture for Block" << std::endl;

	if (destroyable) {
		node = engine.scenemgr->addMeshSceneNode(engine.scenemgr->getMesh(engine.findModelPath("brickBlock/Brick Block.3ds").data()));
		if (!node)
			throw ModelLoadingError("BlockNode");

		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		node->setScale({0.013, 0.013, 0.013});
		setTexture();
	} else {
		node = engine.scenemgr->addMeshSceneNode(engine.scenemgr->getMesh(engine.findModelPath("StrongBlock/block.3ds").data()));
		if (!node)
			throw ModelLoadingError("BlockNode");

		node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
		setTexture();
	}
}

indie::BlockNode::~BlockNode()
{
	node->setVisible(false);
}

std::pair<float, float> indie::BlockNode::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::BlockNode::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, -1, pos.second * 2 + 1});
}

float indie::BlockNode::getRotation()
{
	return node->getRotation().Y;
}

void indie::BlockNode::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::BlockNode::swapTexture(char)
{

}

void indie::BlockNode::setTexture()
{
	if (!texture[0] || !texture[1])
		return;
	if (_destroyable) {
		node->setMaterialTexture(0, texture[0]);
	} else {
		node->setMaterialTexture(0, texture[1]);
	}
}
