/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** FireUp powerup class
*/

#ifndef FIREUP_HPP_
#define FIREUP_HPP_

#include "PowerUp.hpp"

namespace indie {
class FireUp : virtual public PowerUp {
public:
	FireUp(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
	virtual bool interact(indie::Player*);
};
}

#endif // FIREUP_HPP_