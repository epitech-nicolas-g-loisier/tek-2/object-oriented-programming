/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PierceBomb
*/

#ifndef INDIE_STUDIO_PIERCEBOMB_HPP
#define INDIE_STUDIO_PIERCEBOMB_HPP

#include "Bomb.hpp"

namespace indie {

	class PierceBomb : public Bomb {
	public:
		PierceBomb(unsigned char, std::pair<unsigned char, unsigned char>&, GraphicalEngine &, indie::AudioEngine &);
		bool update(indie::MapData &) override;

	protected:
		bool addBlastToMap(indie::MapData &, const std::pair<unsigned char, unsigned char>&) const override;
	};
}

#endif //INDIE_STUDIO_PIERCEBOMB_HPP
