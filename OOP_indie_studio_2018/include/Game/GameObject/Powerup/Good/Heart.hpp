/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Heart powerup class
*/

#ifndef HEART_HPP_
	#define HEART_HPP_

#include "PowerUp.hpp"

namespace indie {
	class Heart : virtual public PowerUp {
	public:
		Heart(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif // HEART_HPP_