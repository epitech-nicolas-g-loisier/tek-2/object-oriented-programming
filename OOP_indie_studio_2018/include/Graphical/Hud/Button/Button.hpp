/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Button
*/

#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <utility>
#include <string>
#include <irrlicht.h>
#include <map>
#include "GraphicalEngine.hpp"

namespace indie
{
class Button
{
public:
	Button(std::pair<int, int> pos, std::string text, indie::GraphicalEngine &engine);
	~Button() = default;
	void draw();
	bool isClicked();

private:
	const std::string _text;
	const std::pair<int, int> _pos;
	bool click = false;
	bool clicked = false;
	std::pair<int, int> posMax;
	indie::GraphicalEngine &_engine;
	std::map<std::string, std::string> texture{};

	std::string checkState();
};
}

#endif //BUTTON_HPP
