/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** test
*/

#include <iostream>
#include <istream>
#include <fstream>
#include "XMLParser/SimpleType.hpp"
#include "XMLParser/Tag.hpp"
#include "XMLParser.hpp"

const std::string WINSTR("[OK] ");
const std::string FAILSTR("[KO] ");

const std::string &assert(bool v)
{
	return (v)? WINSTR : FAILSTR;
}

int main(int ac, char **av)
{
	if (ac != 2)
		return 84;
	std::string filename(av[1]);


	std::shared_ptr<std::istream> is = std::make_shared<std::fstream>(filename);
	try {
		XML::XMLParser parser(is);

		std::cout << parser.find("GameEntities").find("Player").getAll("pos")[1].get<float>() << std::endl;

	} catch(std::exception &e) {
		std::cout << e.what() << std::endl;
	}
}
