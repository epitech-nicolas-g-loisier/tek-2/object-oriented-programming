/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** Game object interface definition
*/
#ifndef GAMEOBJECT_HPP_
#define GAMEOBJECT_HPP_

#include <utility>

namespace indie {

	class Player;
	class Bomb;
	class Blast;
	class PowerUp;

	class IGameObject {
	public:
		virtual ~IGameObject() = default;
		virtual bool interact(indie::Player*) = 0;
		virtual bool interact(indie::Bomb*) = 0;
		virtual bool interact(indie::Blast*) = 0;
		virtual bool interact(indie::PowerUp*) = 0;
	};
}

#include "IEntity.hpp"

#endif // GAMEOBJECT_HPP_