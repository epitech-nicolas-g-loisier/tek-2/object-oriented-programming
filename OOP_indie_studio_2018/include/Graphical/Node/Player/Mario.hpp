/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Player
*/


#include <irrlicht.h>
#include <array>
#include "GraphicalEngine.hpp"
#include "IPlayerNode.hpp"

#ifndef MARIO_HPP
#define MARIO_HPP

namespace indie
{
class Mario : public IPlayerNode
{

public:
	explicit Mario(GraphicalEngine&);
	~Mario() override;
	void move() override;
	void idle() override;
	bool dance() override;

	int danceTime = 0;

	std::pair<float, float> getPosition() override;
	void setPosition(const std::pair<float, float> &) override;
	void setPositionY(float) override;
	float getRotation() override;
	void setRotation(float) override;
	void swapTexture(char) override;

private:
	void setTexture() override;

	irr::scene::IAnimatedMesh *idleMesh{};
	irr::scene::IAnimatedMesh *moveMesh{};
	irr::scene::IAnimatedMesh *danceMesh{};
	irr::scene::IAnimatedMeshSceneNode *node{};
	std::array<irr::video::ITexture *, 7> texture{};
	std::string state = "idle";
};
}

#endif //MARIO_H
