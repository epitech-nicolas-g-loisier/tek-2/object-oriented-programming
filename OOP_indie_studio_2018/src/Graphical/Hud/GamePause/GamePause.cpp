/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GamePause
*/

#include "GamePause.hpp"
#include <memory>

indie::GamePause::GamePause(indie::GraphicalEngine &engine)
	: _engine(engine)
{
	button.push_back(Button({1100, 580}, "Resume", _engine));
	button.push_back(Button({1100, 730}, "Save", _engine));
	button.push_back(Button({1100, 880}, "Exit", _engine));
}

void indie::GamePause::initPause()
{
	std::pair<int, int> move = {720 * (std::rand() % 3), 760 * (std::rand() % 3)};

	choice.x += move.first;
	choice.xMax += move.first;
	choice.y += move.second;
	choice.yMax += move.second;
	beginTime = std::chrono::steady_clock::now();
}

std::chrono::steady_clock::time_point indie::GamePause::stopPause()
{
	return beginTime;
}

indie::GamePause::pauseState indie::GamePause::display()
{
	_engine.drawTexture("Pause/Pause.png", {0, 0});
	_engine.drawSprite("Pause/PausePerso.png", {0, 0}, choice);

	button[0].draw();
	button[1].draw();
	button[2].draw();

	_engine.drawCursor();

	if (button[0].isClicked())
		return RESUME;
	else if (button[1].isClicked())
		return PAUSE;
	else if (button[2].isClicked())
		return EXIT;
	else
		return PAUSE;
}
