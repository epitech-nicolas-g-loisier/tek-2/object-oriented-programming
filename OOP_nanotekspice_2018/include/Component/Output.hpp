/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** output
*/

#ifndef OUTPUT_HPP_
	#define OUTPUT_HPP_

#include "IComponent.hpp"
#include "Pin.hpp"

namespace nts {

	class Output : public IComponent
	{
	public:
		Output(const std::string name);
		~Output();
		nts::Tristate compute(std::size_t pin = 1);
		void setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin);
		void dump() const;
		const std::string &getName() const;
		const std::string getValue() const;

	private:
		std::string _name;
		Tristate _value;
		InputPin *_pin = nullptr;
	};
}

#endif /* !INPUT_HPP_ */
