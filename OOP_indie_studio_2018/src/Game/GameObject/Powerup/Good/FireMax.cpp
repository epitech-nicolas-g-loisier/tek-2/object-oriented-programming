/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** FireMax
*/
#include "FireMax.hpp"
#include <iostream>

indie::FireMax::FireMax(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "FireMax", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::FireMax::interact(indie::Player *player) {
	if (player) {
		player->fireLevel = 8;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}