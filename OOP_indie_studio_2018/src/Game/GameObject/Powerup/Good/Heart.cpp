/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** Heart
*/

#include "Heart.hpp"
#include <iostream>

indie::Heart::Heart(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine)
	: PowerUp(true, engine, "Heart", _audioEngine)
{
	node.setPosition(pos);
}

bool indie::Heart::interact(indie::Player *player) {
	if (player) {
		player->hp++;
		player->pickPowerUp(weakPtr.lock());
		audioEngine.playPowerUpSound();
		node.setDisplay(false);
		return (false);
	}
	return (true);
}