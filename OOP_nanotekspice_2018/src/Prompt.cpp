/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** prompt
*/
#include "IInput.hpp"
#include "Exception.hpp"
#include "Prompt.hpp"
#include <iostream>
#include <algorithm>
#include <signal.h>
#include <vector>
#include <unordered_map>
#include <memory>
#include <cstring>

static void loopSighandler(int s) {
	(void)s;
	std::vector<std::unique_ptr<nts::IComponent>> a;
	nts::Prompt::loop(a);
}

bool nts::Prompt::loop(std::vector<std::unique_ptr<nts::IComponent>> &outputs)
{
	static bool signalCaught = true;

	if (signalCaught == false || outputs.size() == 0) {
		signalCaught = true;
		return (true);
	}
	struct sigaction oldAction;
	sigaction(SIGINT, NULL, &oldAction);
	struct sigaction action = oldAction;
	action.sa_handler = &loopSighandler;
	sigaction(SIGINT, &action, NULL);
	signalCaught = false;
	while (!signalCaught) {
		for (auto&& i : outputs)
			i->compute();
	}
	sigaction(SIGINT, &oldAction, NULL);
	return (true);
}

bool nts::Prompt::compare(std::unique_ptr<nts::IComponent> &a, std::unique_ptr<nts::IComponent> &b)
{
	return (a->getName() < b->getName());
}

bool nts::Prompt::simulate(std::vector<std::unique_ptr<nts::IComponent>> &outputs)
{
	for (auto&& i : outputs)
		i->compute();
	return (true);
}

bool nts::Prompt::display(std::vector<std::unique_ptr<nts::IComponent>> &outputs)
{
	std::sort(outputs.begin(), outputs.end(), compare);
	for (auto&& i : outputs)
		std::cout << i->getName() << "=" << static_cast<nts::Output*>(i.get())->getValue() << std::endl;
	return (true);
}

void nts::Prompt::assignInputValue(std::vector<std::unique_ptr<nts::IComponent>> &inputs, std::string &str)
{
	int i = 0;
	std::string name = str.substr(0, str.find("="));
	std::string valueStr = str.substr(str.find("=") + 1);
	nts::Tristate value = nts::UNDEFINED;

	if (valueStr == "0" || valueStr == "1")
		value = (valueStr == "0" ? nts::FALSE : nts::TRUE);
	for (i = 0; inputs.begin() + i != inputs.end(); i++) {
		if (inputs[i]->getName() == name)
			break;
	}
	if (inputs.begin() + i != inputs.end()) {
		if (value == nts::UNDEFINED)
			throw NanoException("Invalid value");
		static_cast<IInput *>(inputs[i].get())->setValue(value);
	} else
		std::cerr << "Unknown command" << std::endl;
}

bool nts::Prompt::exit(std::vector<std::unique_ptr<nts::IComponent>> &)
{
	return (false);
}

void nts::Prompt::cleanInput(std::string &str)
{
	if (str.find_first_not_of(" \t") == str.npos) {
		str = "";
	} else {
		size_t begPos = str.find_first_not_of(" \t");
		size_t endPos = str.substr(begPos != str.npos ? begPos : 0).find_last_not_of(" \t");
		if (begPos != str.npos && endPos != str.npos)
			str = str.substr(begPos, endPos + 1);
	}
}

void nts::Prompt::updateClocks(std::vector<std::unique_ptr<nts::IComponent>> &inputs)
{
	for (auto&& i : inputs)
		static_cast<IInput *>(i.get())->update();
}

void nts::Prompt::start(std::vector<std::unique_ptr<nts::IComponent>> &inputs, std::vector<std::unique_ptr<nts::IComponent>> &outputs)
{
	std::unordered_map<std::string, bool (*)(std::vector<std::unique_ptr<nts::IComponent>> &)> commands;
	std::string str;
	bool status = true;

	commands["display"] = &display;
	commands["simulate"] = &simulate;
	commands["loop"] = &loop;
	commands["exit"] = &this->exit;
	simulate(outputs);
	display(outputs);
	while (!std::cin.eof() && status) {
		std::cout << "> ";
		getline(std::cin, str);
		cleanInput(str);
		if (str.compare("simulate") == 0)
			updateClocks(inputs);
		if (commands.count(str) == 1)
			status = commands[str](outputs);
		else if (str != "")
			assignInputValue(inputs, str);
	}
}