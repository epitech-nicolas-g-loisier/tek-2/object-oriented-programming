/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpNode
*/

#include <irrlicht.h>

#ifndef POWERUPNODE_HPP
#define POWERUPNODE_HPP

#include "GraphicalEngine.hpp"

namespace indie
{
class PowerUpNode
{
public:
	PowerUpNode(indie::GraphicalEngine &engine, const std::string &type);
	~PowerUpNode();
	std::pair<float, float> getPosition();
	void setPosition(const std::pair<float, float> &);
	float getRotation();
	void setRotation(float);
	void setDisplay(bool);

private:
	irr::scene::IMeshSceneNode *node;

};
}


#endif //POWERUPNODE_HPP
