/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** GameScene
*/

#include "GameScene.hpp"
#include <iostream>
#include <GraphicalException.hpp>

indie::GameScene::GameScene(indie::GraphicalEngine &engine, indie::GameOption &_gameOption)
	: _engine(engine), gameOption(_gameOption), gameEngine(engine, _gameOption), pause(engine), victory("effect/Victory.ogg"), defeat("effect/Defeat.ogg")
{
	gameEngine.generateMap();
	_engine.initGame();
}

indie::IScene::sceneState indie::GameScene::draw()
{
	indie::IScene::sceneState state = indie::IScene::GAME;

	_engine.beginScene({120, 102, 136, 255});

	_engine.scenemgr->drawAll();
	if (_engine.receiver.keyDown("escape") && !isPause) {
		isPause = true;
		pause.initPause();
	}
	if (isPause) {
		indie::GamePause::pauseState choice = pause.display();
		if (choice == indie::GamePause::pauseState::RESUME)
			isPause = false;
		else if (choice == indie::GamePause::pauseState::EXIT)
			state = indie::IScene::BEGIN;
	} else {
		gameState = gameEngine.mainLoop();
	}
	_engine.driver->endScene();
	if (gameState != 0) {
		gameOption.winner = gameState;
		return indie::IScene::GAMEEND;
	}
	return state;
}
