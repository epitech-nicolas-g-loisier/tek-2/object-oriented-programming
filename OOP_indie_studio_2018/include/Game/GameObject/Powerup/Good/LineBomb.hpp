/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** LineBomb
*/

#ifndef BOMBERMAN_LINEBOMB_HPP
#define BOMBERMAN_LINEBOMB_HPP

#include "PowerUp.hpp"

namespace indie {
	class LineBomb : virtual public PowerUp {
	public:
		LineBomb(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
		virtual bool interact(indie::Player *);
	};
}

#endif //BOMBERMAN_LINEBOMB_HPP
