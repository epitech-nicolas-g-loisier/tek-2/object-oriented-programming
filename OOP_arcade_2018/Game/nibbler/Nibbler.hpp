/*
** EPITECH PROJECT, 2019
** OOP arcade
** File description:
** Nibbler class
*/

#ifndef NIBBLER_HPP_
#define NIBBLER_HPP_

#include <vector>
#include <utility>
#include <string>
#include <memory>
#include <chrono>
#include <ctime>


#include "Snake.hpp"
#include "IEngine.hpp"

namespace nibbler {

class Nibbler : public IEngine {
	public:
		Nibbler();
		~Nibbler();
		std::pair<bool, int> computeInput(const std::vector<std::string> &);
		void display(IGraphical &display);

	private:
		std::unique_ptr<Snake> _snake = std::make_unique<Snake>();
		std::pair<int, int> _food;
		std::pair<int, int> _bonusFood;
		std::pair<int, int> _mapSize;
		int _score;
		std::chrono::nanoseconds start;
		int res;
		Arcade::Rect _snakeHead = {{0, 0}, {1, 1}, "", Arcade::Color::YELLOW, '@', false};
		Arcade::Rect _snakeBody = {{0, 0}, {1, 1}, "nibbler/SnakeBody.png", Arcade::Color::GREEN, '#', false};
		Arcade::Rect _foodRect = {{0, 0}, {1, 1}, "nibbler/Apple.png", Arcade::Color::RED, 'A', false};
		Arcade::Rect _bonusFoodRect = {{0, 0}, {1, 1}, "nibbler/Rabbit.png", Arcade::Color::WHITE, 'R', false};
		Arcade::Rect _mapRect = {{5, 5}, {_mapSize.first + 1, _mapSize.second + 1}, "", Arcade::Color::BLACK, ' ', true};

		bool checkColision() const;
		void createFood();
		void createBonusFood();
		void getHeadSprite(const std::pair<float, float> &, const std::pair<float, float> &);
};

}

#endif /* !NIBBLER_HPP_ */
