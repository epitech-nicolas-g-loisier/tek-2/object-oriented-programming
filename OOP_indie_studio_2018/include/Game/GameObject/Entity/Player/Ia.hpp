/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** Ia
*/

#ifndef IA_HPP_
#define IA_HPP_

#include "MapVisitors.hpp"
#include "BlockFactory.hpp"

#define ROW 15
#define COL 9

namespace indie {

	class Ia : virtual public Player {
	public:
		Ia(std::pair<float, float> src, GraphicalEngine &engine, const std::string &character, indie::AudioEngine &_audioEngine);
		~Ia() = default;

		bool update(indie::MapData &) final;

	private:

		void calculatePathToPlayer(indie::MapData &mapData);
		void canMoveWhenDecreasing(indie::MapData &mapData);
		void canMoveWhenIncreasing(indie::MapData &mapData);

		bool handling(indie::MapData &mapData);
		void setPosition(std::pair<float, float>);

		void isInRangeOfBlast(indie::MapData &mapData, unsigned int index);
		void hideFromRow(indie::MapData &mapData);
		void hideFromCol(indie::MapData &mapData);
		bool hide(indie::MapData &mapData);

		std::pair<float,float> chooseEnemy(indie::MapData &map);
		bool isDestination(int, int);
		bool isUnBlocked(std::map<std::pair<unsigned char, unsigned char>, indie::EntityUnion> const &grid, std::pair<float, float> mapIndex);
		static bool cellIsValid(int, int);
		bool mapIsValid();

		bool moving(indie::MapData &mapData);
		void movePlayerRight(indie::MapData &mapData, long timeDiff);
		void movePlayerLeft(indie::MapData &mapData, long timeDiff);
		void movePlayerDown(indie::MapData &mapData, long timeDiff);
		void movePlayerUp(indie::MapData &mapData, long timeDiff);

		void launchBomb(indie::MapData &mapData);

		bool isNearTarget(MapData &mapData);

		void interactWithEntity(indie::MapData &mapData);

		std::pair<float, float> _dest;
		std::pair<float, float> _prevDest;
		std::pair<float, float> _indexPos;
		std::pair<unsigned char, unsigned char> _bombPos;

		GraphicalEngine &_graphicalEngine;
		std::vector<std::pair<std::pair<float, float>, bool> > _path;
		bool _isMoving;
		std::chrono::steady_clock::time_point _prevTime;
		std::chrono::steady_clock::time_point _currTime;
	};
}

#endif /* !IA_HPP_ */
