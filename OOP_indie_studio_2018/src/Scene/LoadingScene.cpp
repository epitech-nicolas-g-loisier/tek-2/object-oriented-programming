/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** LoadingScene
*/

#include "LoadingScene.hpp"

indie::LoadingScene::LoadingScene(indie::GraphicalEngine &engine)
	: _engine(engine)
{}

indie::IScene::sceneState indie::LoadingScene::draw()
{
	_engine.beginScene({120, 102, 136, 255});
	_engine.drawBackground("Screen/LoadingScreen.png");
	_engine.drawText("Loading", "large", {255, 255, 255, 255}, {960, 0, 1920, 1080});

	_engine.endScene();
	return indie::IScene::POWERUP;
}
