/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** DangerousBombPowerup
*/

#ifndef INDIE_STUDIO_DANGEROUSBOMBPOWERUP_HPP
#define INDIE_STUDIO_DANGEROUSBOMBPOWERUP_HPP

#include "PowerUp.hpp"

namespace indie {
class DangerousBombPowerup : virtual public PowerUp {
public:
	DangerousBombPowerup(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
	virtual bool interact(indie::Player *);

};
}

#endif //INDIE_STUDIO_DANGEROUSBOMBPOWERUP_HPP
