/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** TitleScreenScene
*/

#include "TitleScreenScene.hpp"

indie::TitleScreenScene::TitleScreenScene(indie::GraphicalEngine &engine)
        : _engine(engine)
{
        choices = {indie::IScene::PLAYERSELECTOR, indie::IScene::LOADSAVE, indie::IScene::OPTION, indie::IScene::EXIT};

        button.push_back(Button({760, 215}, "New Game", engine));
	button.push_back(Button({760, 365}, "Load Game", engine));
	button.push_back(Button({760, 515}, "Options", engine));
	button.push_back(Button({760, 665}, "Exit", engine));
	engine.device->getCursorControl()->setPosition(960, 100);
}

indie::IScene::sceneState indie::TitleScreenScene::draw()
{
	_engine.beginScene({120, 102, 136, 255});
	indie::IScene::sceneState choice = indie::IScene::TITLESCREEN;
	_engine.drawBackground("Screen/TitleScreen.jpg");
        for (unsigned int idx = 0; idx < button.size(); idx++) {
        	button[idx].draw();
        	if (button[idx].isClicked()) {
			choice = choices[idx];
		}
        }

        _engine.drawCursor();
        _engine.endScene();
        return choice;
}
