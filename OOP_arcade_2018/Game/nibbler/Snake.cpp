/*
** EPITECH PROJECT, 2019
** OOP arcade
** File description:
** Snake Method
*/

#include "Snake.hpp"
#include <algorithm>

nibbler::Snake::Snake()
{
	_body.push_back({10, 16});
	_body.push_back({10, 15});
	_body.push_back({10, 14});
	_body.push_back({10, 13});
	_body.push_back({10, 12});
	_body.push_back({10, 11});
	_body.push_back({10, 10});
	_body.push_back({10, 9});
	_body.push_back({10, 8});

	_dir = SOUTH;
}

nibbler::Snake::~Snake()
{
}

void nibbler::Snake::changeDir(const std::string &input)
{
	if (input == "Z" && _dir != SOUTH && _body[0].first != _body[1].first)
		_dir = NORTH;
	else if (input == "S" && _dir != NORTH && _body[0].first != _body[1].first)
		_dir = SOUTH;
	else if (input == "Q" && _dir != EAST && _body[0].second != _body[1].second)
		_dir = WEST;
	else if (input == "D" && _dir != WEST && _body[0].second != _body[1].second)
		_dir = EAST;
}

void nibbler::Snake::move()
{
	std::pair<float, float> new_pos = _body[0];

	if (_dir == NORTH)
		new_pos.second -= 1;
	else if (_dir == SOUTH)
		new_pos.second += 1;
	else if (_dir == EAST)
		new_pos.first += 1;
	else if (_dir == WEST)
		new_pos.first -= 1;
	_body.insert(_body.begin(), new_pos);
	_body.pop_back();
}

#include <iostream>
void nibbler::Snake::grow(int size)
{
	std::vector<std::pair<float, float>> new_pos;

	for (int i = 1; i <= size; i++) {
		new_pos.push_back(_body[_body.size() - i]);
	}

	std::reverse(new_pos.begin(), new_pos.end());
	_body.insert(_body.end(), new_pos.begin(), new_pos.end());
}

std::vector<std::pair<float, float>> nibbler::Snake::getBody() const
{
	return _body;
}