/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PowerUpNode
*/

#include "PowerUpNode.hpp"

indie::PowerUpNode::PowerUpNode(indie::GraphicalEngine &engine, const std::string &type)
{
	node = engine.powerUpNodeFactory.createNode(engine, type);
	if (node)
		node->setVisible(false);
}

indie::PowerUpNode::~PowerUpNode()
{
	if (node)
		node->remove();
}

std::pair<float, float> indie::PowerUpNode::getPosition()
{
	if (!node)
		return {0, 0};
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::PowerUpNode::setPosition(const std::pair<float, float>& pos)
{
	if (!node)
		return;
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::PowerUpNode::getRotation()
{
	if (!node)
		return 0;
	return node->getRotation().Y;
}

void indie::PowerUpNode::setRotation(float rotation)
{
	if (!node)
		return;
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::PowerUpNode::setDisplay(bool visibility)
{
	if (!node)
		return;
	node->setVisible(visibility);
}