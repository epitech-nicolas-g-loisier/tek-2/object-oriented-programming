/*
** EPITECH PROJECT, 2019
** bomberman
** File description:
** test2D
*/

#include <irrlicht.h>
#include "GraphicalEngine.hpp"
#include "driverChoice.h"
#include "BeginScene.hpp"

indie::BeginScene::BeginScene(indie::GraphicalEngine &engine)
        : _engine(engine)
{
        title = "Title.png";
        background = "Screen/BeginScreen.jpg";
}

indie::IScene::sceneState indie::BeginScene::draw()
{
        int time = _engine.device->getTimer()->getTime();

        _engine.beginScene({120, 102, 136, 255});
        _engine.drawBackground(background);
        _engine.drawTexture(title, {1200, 40});

        _engine.drawText("Press SPACE to continue", "large", {time % 610 / 2, time % 610 / 2, 255, 255}, {0, 800, 1920, 1000});

        _engine.driver->endScene();
        if (_engine.receiver.keyPressed("space"))
                return indie::IScene::TITLESCREEN;
        return indie::IScene::BEGIN;
}
