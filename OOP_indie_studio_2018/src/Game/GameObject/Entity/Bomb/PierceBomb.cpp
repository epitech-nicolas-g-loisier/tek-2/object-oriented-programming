/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** PierceBomb
*/
#include "PierceBomb.hpp"
#include "MapVisitors.hpp"

indie::PierceBomb::PierceBomb(unsigned char fLevel, std::pair<unsigned char, unsigned char> &pos, GraphicalEngine &graphicalEngine, indie::AudioEngine &_audioEngine)
	:	Bomb(fLevel, pos, graphicalEngine, _audioEngine)
{}

bool indie::PierceBomb::update(indie::MapData &map)
{
	bool isAlive = std::chrono::steady_clock::now() < deathTime;
	if (isAlive)
		return isAlive;
	audioEngine.playBombSound();
	for (std::pair<unsigned char, unsigned char>i = {_pos.first - 1, _pos.second}; i.first >= _pos.first - level && i.first < 15; i.first--)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first + 1, _pos.second}; i.first <= _pos.first + level && i.first < 15 ; i.first++)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first, _pos.second - 1}; i.second >= _pos.second - level && i.second < 9; i.second--)
		if (!addBlastToMap(map, i))
			break;
	for (std::pair<unsigned char, unsigned char>i = {_pos.first, _pos.second + 1}; i.second <= _pos.second + level && i.second < 9; i.second++)
		if (!addBlastToMap(map, i))
			break;
	auto blast = std::make_shared<indie::Blast>(_pos, engine, true);
	map.entityMap[_pos] = blast;
	map.entities.push_back(std::move(blast));
	return (isAlive);
}

bool indie::PierceBomb::addBlastToMap(indie::MapData &map, const std::pair<unsigned char, unsigned char> &i) const
{
	auto blast = std::make_shared<indie::Blast>(i, engine, true);
	if (map.entityMap.count(i)) {
		std::weak_ptr<IGameObject> weakPtr;
		std::visit(ENTITY_VISITOR(weakPtr), map.entityMap[i]);
		if (weakPtr.lock()->interact(blast.get()))
			return (false);
	}
	map.entityMap[i] = blast;
	map.entities.push_back(std::move(blast));
	return (true);
}