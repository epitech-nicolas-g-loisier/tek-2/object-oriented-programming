/*
** EPITECH PROJECT, 2019
** Tektris
** File description:
** Luigi
*/

#include <iostream>
#include "Luigi.hpp"
#include "GraphicalException.hpp"

indie::Luigi::Luigi(GraphicalEngine &engine)
{
	moveMesh = engine.scenemgr->getMesh(engine.findModelPath("Luigi/Walking.x").data());
	idleMesh = engine.scenemgr->getMesh(engine.findModelPath("Luigi/Idle.x").data());
	danceMesh = engine.scenemgr->getMesh(engine.findModelPath("Luigi/Dance.x").data());
	if (!moveMesh || !danceMesh || !idleMesh)
		throw ModelLoadingError("Luigi");

	texture[0] = engine.driver->getTexture(engine.findModelPath("Luigi/body.png").data());
	texture[1] = engine.driver->getTexture(engine.findModelPath("Luigi/pupil.png").data());
	texture[2] = engine.driver->getTexture(engine.findModelPath("Luigi/face.png").data());
	texture[3] = engine.driver->getTexture(engine.findModelPath("Luigi/glove.png").data());
	texture[4] = engine.driver->getTexture(engine.findModelPath("Luigi/hair.png").data());
	texture[5] = engine.driver->getTexture(engine.findModelPath("Luigi/hat.png").data());
	for (int i = 0; i < 6; ++i) {
		if (!texture[i])
			std::cerr << "Cannot find texture number: " << i << " for Luigi" << std::endl;
	}

	node = engine.scenemgr->addAnimatedMeshSceneNode(idleMesh);
	if (!node)
		throw NodeCreationError("Luigi");

	node->setRotation({0, -90, 0});
	node->setPosition({(float)(3), -0.5, (float)(3)});
	setTexture();
}

indie::Luigi::~Luigi()
{
	node->remove();
}

void indie::Luigi::setTexture()
{
	for (int i = 0; i < 6; ++i) {
		if (!texture[i])
			return;
	}
	node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	node->getMaterial(0).setTexture(0, texture[0]);
	node->getMaterial(1).setTexture(0, texture[1]);
	node->getMaterial(2).setTexture(0, texture[2]);
	node->getMaterial(3).setTexture(0, texture[3]);
	node->getMaterial(4).setTexture(0, texture[3]);
	node->getMaterial(5).setTexture(0, texture[4]);
	node->getMaterial(6).setTexture(0, texture[5]);
}

void indie::Luigi::idle()
{
	if (state == "idle")
		return;

	state = "idle";
	node->setMesh(idleMesh);
	node->setCurrentFrame(std::rand() % 100 + 20);
	setTexture();
}

void indie::Luigi::move()
{
	if (state == "move")
		return;

	state = "move";
	node->setMesh(moveMesh);
	setTexture();
}

bool indie::Luigi::dance()
{
	if (danceTime == 0) {
		node->setMesh(danceMesh);
		setTexture();
	}
	danceTime++;
	if (danceTime > 150) {
		danceTime = 0;
		return false;
	}
	return true;
}

std::pair<float, float> indie::Luigi::getPosition()
{
	auto pos = node->getPosition();

	return {pos.X / 2, pos.Z / 2};
}

void indie::Luigi::setPosition(const std::pair<float, float>& pos)
{
	node->setPosition({pos.first * 2 + 1, node->getPosition().Y, pos.second * 2 + 1});
}

float indie::Luigi::getRotation()
{
	return node->getRotation().Y;
}

void indie::Luigi::setRotation(float rotation)
{
	auto tmp = node->getRotation();
	node->setRotation({tmp.X, rotation, tmp.Z});
}

void indie::Luigi::swapTexture(char)
{

}

void indie::Luigi::setPositionY(float y)
{
	node->setPosition({node->getPosition().X, y, node->getPosition().X});
}
