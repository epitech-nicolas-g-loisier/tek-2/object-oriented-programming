/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Main function
*/

#include "Core.hpp"
#include <iostream>

int main(int argc, char **argv)
{
	if (argc != 2) {
		std::cerr << "Usage:" << std::endl << "./arcade [graphical_library_name]" << std::endl;
		std::cerr << "The graphical library must be in the directory ./lib" << std::endl;
		return (84);
	}
	Core core;
	core.swapGraphLib(std::string(argv[1]));
	core.openMenu();
	return (0);
}