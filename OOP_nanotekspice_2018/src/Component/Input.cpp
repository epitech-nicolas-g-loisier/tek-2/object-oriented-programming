/*
** EPITECH PROJECT, 2019
** OOP NTS
** File description:
** Input Method
*/
#include "Input.hpp"
#include "Exception.hpp"
#include "Pin.hpp"
#include <iostream>
#include <unordered_map>

nts::Input::Input(const std::string name, const Tristate value)
	: _name(name), _value(value)
{
}

nts::Tristate nts::Input::compute(std::size_t pin)
{
	if (pin != 1)
		throw NanoException("Requested pin does not exist.");
	return _value;
}

void nts::Input::setLink(std::size_t pin, nts::IComponent &other, std::size_t otherPin)
{
	other.setLink(otherPin, *this, pin);
}

void nts::Input::dump() const
{
	std::cout << _name << "=" << _value << std::endl;
}

const std::string &nts::Input::getName() const
{
	return (_name);
}

void nts::Input::setValue(nts::Tristate value)
{
	_value = value;
}