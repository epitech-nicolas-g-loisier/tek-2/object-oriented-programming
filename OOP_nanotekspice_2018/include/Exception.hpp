/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** exception class for nano
*/

#ifndef EXCEPTION_HPP_
#define EXCEPTION_HPP_

#include <exception>
#include <string>

class NanoException : public std::exception {
	public:
		NanoException(const std::string &message);
		const char *what() const noexcept override;

	private:
		std::string _message;
};

#endif /* !EXCEPTION_HPP_ */
