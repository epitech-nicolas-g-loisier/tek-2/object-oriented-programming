/*
** EPITECH PROJECT, 2019
** Indie studio
** File description:
** FireMax powerup class
*/

#ifndef FIREMAX_HPP_
#define FIREMAX_HPP_

#include "PowerUp.hpp"

namespace indie {
class FireMax : virtual public PowerUp {
public:
	FireMax(GraphicalEngine &engine, std::pair<int, int> pos, indie::AudioEngine &_audioEngine);
	virtual bool interact(indie::Player*);
};
}

#endif // FIREMAX_HPP_