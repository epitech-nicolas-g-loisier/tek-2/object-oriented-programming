/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** SplashScreenScene
*/

#include <iostream>
#include "SplashScreenScene.hpp"
#include "GraphicalException.hpp"

indie::SplashScreenScene::SplashScreenScene(indie::GraphicalEngine &engine)
	: _engine(engine)
{
	splash.emplace_back("EpitechLogo.png");
	splash.emplace_back("TekVisionLogo.png");
}

indie::IScene::sceneState indie::SplashScreenScene::draw()
{
	_engine.beginScene({0, 0, 0, 0});

	if (color >= 255) {
		diff = -4;
		color = 255;
		choice++;
	} else if (color <= 0) {
		diff = 2;
		color = 0;
	}

	if (choice >= (int)splash.size() || _engine.receiver.keyDown("return"))
		return indie::IScene::BEGIN;

	_engine.drawTexture(splash[choice], {460, 354});
	_engine.drawRectangle({0, 0, 1920, 1080}, {0, 0, 0, color});

	color += diff;

	_engine.drawText("Press Enter to skip", "small", {255, 255, 255, 200}, {1570, 1050, 1920, 1080});

	_engine.endScene();
	return  indie::IScene::SPLASHSCREEN;
}
