/*
** EPITECH PROJECT, 2019
** OOP nanotekspice
** File description:
** prompt
*/
#include "Input.hpp"
#include "Output.hpp"
#include <vector>
#include <memory>

#ifndef PROMPT_NTS_
	#define PROMPT_NTS_

namespace nts {
	class Prompt
	{
	public:
		Prompt() {};
		void start(std::vector<std::unique_ptr<nts::IComponent>> &inputs, std::vector<std::unique_ptr<nts::IComponent>> &outputs);
		static bool loop(std::vector<std::unique_ptr<nts::IComponent>> &outputs);
		static bool compare(std::unique_ptr<nts::IComponent> &a, std::unique_ptr<nts::IComponent> &b);

	private:
		static bool simulate(std::vector<std::unique_ptr<nts::IComponent>> &outputs);
		static bool display(std::vector<std::unique_ptr<nts::IComponent>> &outputs);
		static bool exit(std::vector<std::unique_ptr<nts::IComponent>> &outputs);
		void assignInputValue(std::vector<std::unique_ptr<nts::IComponent>> &inputs, std::string &str);
		void cleanInput(std::string &str);
		void updateClocks(std::vector<std::unique_ptr<nts::IComponent>>&);
	};
}

#endif // PROMPT_NTS_