/*
** EPITECH PROJECT, 2019
** arcade
** File description:
** Definition of the interface IGraphical
*/
#include <string>
#include <vector>
#include "Assets.hpp"

#ifndef IGRAPHICAL_
	#define IGRAPHICAL_

class IGraphical
{
public:
	virtual ~IGraphical() {};
	virtual void drawLine(const Arcade::Line &) = 0;
	virtual void drawRect(const Arcade::Rect &) = 0;
	virtual void drawPair(const Arcade::Pair &) = 0;
	virtual void update() = 0;
	virtual std::vector<std::string> getInput() = 0;
};

#endif // IGRAPHICAL_