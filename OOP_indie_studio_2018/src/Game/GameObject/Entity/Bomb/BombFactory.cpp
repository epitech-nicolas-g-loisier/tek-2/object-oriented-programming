/*
** EPITECH PROJECT, 2019
** Indie Studio
** File description:
** BombFactory
*/
#include "BombFactory.hpp"
#include "Bomb.hpp"
#include "PierceBomb.hpp"
#include "DangerousBomb.hpp"
#include "GameException.hpp"

indie::BombFactory::BombFactory(indie::GraphicalEngine &_graphEngine, indie::AudioEngine &_audioEngine)
	: graphicalEngine(_graphEngine), audioEngine(_audioEngine)
{
	ctors[indie::BombType::NORMAL] = &indie::BombFactory::spawnNormalBomb;
	ctors[indie::BombType::DANGEROUS] = &indie::BombFactory::spawnDangerousBomb;
	ctors[indie::BombType::PIERCE] = &indie::BombFactory::spawnPierceBomb;
}

std::shared_ptr<indie::Bomb> indie::BombFactory::spawnBomb(indie::BombType type, unsigned char fireLevel,
	std::pair<float, float> pos)
{
	if (!ctors.count(type))
		throw GameException("Unknown type of bomb");
	return (this->*ctors[type])(fireLevel, pos);
}

std::shared_ptr<indie::Bomb> indie::BombFactory::spawnNormalBomb(unsigned char fireLevel, std::pair<unsigned char, unsigned char> pos) const
{
	return std::make_shared<Bomb>(fireLevel, pos, graphicalEngine, audioEngine);
}

std::shared_ptr<indie::Bomb> indie::BombFactory::spawnDangerousBomb(unsigned char fireLevel, std::pair<unsigned char, unsigned char> pos) const
{
	return std::make_shared<DangerousBomb>(fireLevel, pos, graphicalEngine, audioEngine);
}

std::shared_ptr<indie::Bomb> indie::BombFactory::spawnPierceBomb(unsigned char fireLevel, std::pair<unsigned char, unsigned char> pos) const
{
	return std::make_shared<PierceBomb>(fireLevel, pos, graphicalEngine, audioEngine);
}